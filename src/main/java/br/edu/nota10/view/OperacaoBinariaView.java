/**
 * 
 */
package br.edu.nota10.view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

import br.edu.nota10.controller.OperacaoBinariaController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class OperacaoBinariaView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblTutorial;
	public JLabel lblAplicacao;
	public JLabel lblTesteSeusConhec;

	// Tutorial
	public JLabel lblSomaBinarioTutorial;
	public JLabel lblSubtracaoBinarioTutorial;

	// Aplica��o
	public JLabel lblSomaBinarioAplicacao;
	public JLabel lblSubtracaoBinarioAplicacao;

	// Teste Seus Conhecimentos
	public JLabel lblSomaBinarioTeste;
	public JLabel lblSubtracaoBinarioTeste;
	public JLabel lblTesteGeralBinarioTeste;

	// Voltar
	public JLabel lblVoltar;

	public OperacaoBinariaView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Operacoes Binaria");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		imgFundo = new JLabel(imagens.fundo);

		lblTutorial = new JLabel(imagens.tutorial);
		lblAplicacao = new JLabel(imagens.aplicacao);
		lblTesteSeusConhec = new JLabel(imagens.testeSeuConhecmento);

		lblSomaBinarioTutorial = new JLabel(imagens.soma);
		lblSubtracaoBinarioTutorial = new JLabel(imagens.subtracao);

		lblSomaBinarioAplicacao = new JLabel(imagens.soma);
		lblSubtracaoBinarioAplicacao = new JLabel(imagens.subtracao);

		lblSomaBinarioTeste = new JLabel(imagens.soma);
		lblSubtracaoBinarioTeste = new JLabel(imagens.subtracao);
		lblTesteGeralBinarioTeste = new JLabel(imagens.testeGeral);

		lblVoltar = new JLabel(imagens.voltar);

		imgFundo.setBounds(0, 0, 800, 600);

		lblTutorial.setBounds(20, 50, 73, 25);
		lblAplicacao.setBounds(20, 200, 92, 25);
		lblTesteSeusConhec.setBounds(20, 350, 237, 25);

		lblSomaBinarioTutorial.setBounds(40, 90, 64, 22);
		lblSubtracaoBinarioTutorial.setBounds(40, 110, 108, 22);

		lblSomaBinarioAplicacao.setBounds(40, 240, 64, 22);
		lblSubtracaoBinarioAplicacao.setBounds(40, 260, 108, 22);

		lblSomaBinarioTeste.setBounds(40, 390, 64, 22);
		lblSubtracaoBinarioTeste.setBounds(40, 410, 108, 22);
		lblTesteGeralBinarioTeste.setBounds(40, 430, 108, 22);

		lblVoltar.setBounds(20, 515, 98, 45);

		add(lblTutorial);
		add(lblAplicacao);
		add(lblTesteSeusConhec);

		add(lblSomaBinarioTutorial);
		add(lblSubtracaoBinarioTutorial);

		add(lblSomaBinarioAplicacao);
		add(lblSubtracaoBinarioAplicacao);

		add(lblSomaBinarioTeste);
		add(lblSubtracaoBinarioTeste);
		add(lblTesteGeralBinarioTeste);

		add(lblVoltar);

		add(imgFundo);
		add(new JLabel());

		final OperacaoBinariaController matrizController = new OperacaoBinariaController(this);
		matrizController.addObserver(this);

		lblSomaBinarioTutorial.addMouseListener(matrizController);
		lblSubtracaoBinarioTutorial.addMouseListener(matrizController);

		lblSomaBinarioAplicacao.addMouseListener(matrizController);
		lblSubtracaoBinarioAplicacao.addMouseListener(matrizController);

		lblSomaBinarioTeste.addMouseListener(matrizController);
		lblSubtracaoBinarioTeste.addMouseListener(matrizController);
		lblTesteGeralBinarioTeste.addMouseListener(matrizController);

		lblVoltar.addMouseListener(matrizController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }

	public static void main(String[] args) {
		new OperacaoBinariaView();
	}
}