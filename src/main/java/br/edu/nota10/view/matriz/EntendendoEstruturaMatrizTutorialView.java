/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.EntendendoEstruturaMatrizTutorialController;
import br.edu.nota10.model.Imagem;

/**
 * @author Jesse A. Done
 */
public class EntendendoEstruturaMatrizTutorialView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblEntendendoMatrizTutorialTexto;
	public JLabel lblVolta;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public EntendendoEstruturaMatrizTutorialView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Entendendo a Estrutura Matricial Tutorial");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800,850));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblEntendendoMatrizTutorialTexto = new JLabel(imagens.entendendoMatrizTextoTutorial);
		lblVolta = new JLabel(imagens.voltar);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblEntendendoMatrizTutorialTexto.setBounds(20, 50, 734, 795);
		lblVolta.setBounds(30, 810, 98, 45);

		// Adicionando item ao Painel
		paneil.add(lblEntendendoMatrizTutorialTexto);
		paneil.add(lblVolta);
		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final EntendendoEstruturaMatrizTutorialController entendendoMatrizController = new EntendendoEstruturaMatrizTutorialController(this);
		lblVolta.addMouseListener(entendendoMatrizController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}