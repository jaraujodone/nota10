/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.MultiplicacaoPorEscalarMatrizTutorialController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class MultiplicacaoPorEscalarMatrizTutorialView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblMultPorEscalarTutorialTexto;
	public JLabel lblConferirEx1;
	public JLabel lblConferirEx2;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat113, txtMat121, txtMat122, txtMat123, txtMat131, txtMat132, txtMat133;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212, txtMat221, txtMat222, txtMat231, txtMat232, txtMat241, txtMat242;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public MultiplicacaoPorEscalarMatrizTutorialView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Tutorial Multiplica��o por Escalar");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800,1650));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblMultPorEscalarTutorialTexto = new JLabel(imagens.multiplicacaoPorEscalarTextoTutorial);
		lblConferirEx1 = new JLabel(imagens.conferir);
		lblConferirEx2 = new JLabel(imagens.conferir);
		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3);
		txtMat112 = new JTextField("", 3);
		txtMat113 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat123 = new JTextField("", 3);
		txtMat131 = new JTextField("", 3);
		txtMat132 = new JTextField("", 3);
		txtMat133 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3);
		txtMat212 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);
		txtMat231 = new JTextField("", 3);
		txtMat232 = new JTextField("", 3);
		txtMat241 = new JTextField("", 3);
		txtMat242 = new JTextField("", 3);

		// Posicionando
		imgFundo.setBounds(0, 0, 800, 3500);
		lblMultPorEscalarTutorialTexto.setBounds(20, 50, 740, 1550);
		lblConferirEx1.setBounds(470, 1300, 151, 38);
		lblConferirEx2.setBounds(470, 1540, 151, 38);
		lblResultadoEx1.setBounds(600, 1210, 75, 65);
		lblResultadoEx2.setBounds(590, 1460, 75, 65);
		lblVolta.setBounds(30, 1600, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(435, 1170, 30, 25);
		txtMat112.setBounds(495, 1170, 30, 25);
		txtMat113.setBounds(555, 1170, 30, 25);
		txtMat121.setBounds(435, 1210, 30, 25);
		txtMat122.setBounds(495, 1210, 30, 25);
		txtMat123.setBounds(555, 1210, 30, 25);
		txtMat131.setBounds(435, 1250, 30, 25);
		txtMat132.setBounds(495, 1250, 30, 25);
		txtMat133.setBounds(555, 1250, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(460, 1380, 30, 25);
		txtMat212.setBounds(520, 1380, 30, 25);
		txtMat221.setBounds(460, 1420, 30, 25);
		txtMat222.setBounds(520, 1420, 30, 25);
		txtMat231.setBounds(460, 1460, 30, 25);
		txtMat232.setBounds(520, 1460, 30, 25);
		txtMat241.setBounds(460, 1500, 30, 25);
		txtMat242.setBounds(520, 1500, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblMultPorEscalarTutorialTexto);
		paneil.add(lblConferirEx1);
		paneil.add(lblConferirEx2);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat113);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat123);
		paneil.add(txtMat131);
		paneil.add(txtMat132);
		paneil.add(txtMat133);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat221);
		paneil.add(txtMat222);
		paneil.add(txtMat231);
		paneil.add(txtMat232);
		paneil.add(txtMat241);
		paneil.add(txtMat242);	

		// Imagem Fundo
		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final MultiplicacaoPorEscalarMatrizTutorialController multPorEscalarTutorialController = 
				new MultiplicacaoPorEscalarMatrizTutorialController(this);
		multPorEscalarTutorialController.addObserver(this);
		lblConferirEx1.addMouseListener(multPorEscalarTutorialController);
		lblConferirEx2.addMouseListener(multPorEscalarTutorialController);
		lblVolta.addMouseListener(multPorEscalarTutorialController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) { }
}