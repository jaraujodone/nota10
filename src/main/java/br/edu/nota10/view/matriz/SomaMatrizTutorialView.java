/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.SomaMatrizTutorialController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class SomaMatrizTutorialView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblSomaTutorialTexto;
	public JLabel lblConferirEx1;
	public JLabel lblConferirEx2;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat113, txtMat121, txtMat122, txtMat123;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212, txtMat213, txtMat221, txtMat222, txtMat223;
	public JTextField txtMat231, txtMat232, txtMat233, txtMat241, txtMat242, txtMat243;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public SomaMatrizTutorialView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Tutorial Adi��o de Matrizes");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800, 1710));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblSomaTutorialTexto = new JLabel(imagens.adicacaoTextoTutorial);
		lblConferirEx1 = new JLabel(imagens.conferir);
		lblConferirEx2 = new JLabel(imagens.conferir);
		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3);
		txtMat112 = new JTextField("", 3);
		txtMat113 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat123 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3);
		txtMat212 = new JTextField("", 3);
		txtMat213 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);
		txtMat223 = new JTextField("", 3);
		txtMat231 = new JTextField("", 3);
		txtMat232 = new JTextField("", 3);
		txtMat233 = new JTextField("", 3);
		txtMat241 = new JTextField("", 3);
		txtMat242 = new JTextField("", 3);
		txtMat243 = new JTextField("", 3);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblSomaTutorialTexto.setBounds(20, 50, 722, 1577);
		lblConferirEx1.setBounds(510, 1360, 151, 38);
		lblConferirEx2.setBounds(510, 1610, 151, 38);
		lblResultadoEx1.setBounds(650, 1280, 75, 65);
		lblResultadoEx2.setBounds(650, 1520, 75, 65);
		lblVolta.setBounds(30, 1670, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(480, 1260, 30, 25);
		txtMat112.setBounds(540, 1260, 30, 25);
		txtMat113.setBounds(600, 1260, 30, 25);
		txtMat121.setBounds(480, 1310, 30, 25);
		txtMat122.setBounds(540, 1310, 30, 25);
		txtMat123.setBounds(600, 1310, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(483, 1450, 30, 25);
		txtMat212.setBounds(533, 1450, 30, 25);
		txtMat213.setBounds(583, 1450, 30, 25);
		txtMat221.setBounds(483, 1490, 30, 25);
		txtMat222.setBounds(533, 1490, 30, 25);
		txtMat223.setBounds(583, 1490, 30, 25);
		txtMat231.setBounds(483, 1530, 30, 25);
		txtMat232.setBounds(533, 1530, 30, 25);
		txtMat233.setBounds(583, 1530, 30, 25);
		txtMat241.setBounds(483, 1570, 30, 25);
		txtMat242.setBounds(533, 1570, 30, 25);
		txtMat243.setBounds(583, 1570, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblSomaTutorialTexto);
		paneil.add(lblConferirEx1);
		paneil.add(lblConferirEx2);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat113);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat123);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat213);
		paneil.add(txtMat221);
		paneil.add(txtMat222);
		paneil.add(txtMat223);
		paneil.add(txtMat231);
		paneil.add(txtMat232);
		paneil.add(txtMat233);
		paneil.add(txtMat241);
		paneil.add(txtMat242);
		paneil.add(txtMat243);

		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final SomaMatrizTutorialController somaTutorialController = new SomaMatrizTutorialController(this);
		somaTutorialController.addObserver(this);
		lblConferirEx1.addMouseListener(somaTutorialController);
		lblConferirEx2.addMouseListener(somaTutorialController);
		lblVolta.addMouseListener(somaTutorialController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) { }
}