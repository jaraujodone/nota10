/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.MultiplicacaoDeMatrizQuestoesController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class MultiplicacaoDeMatrizQuestoesView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblSubtracaoQuestoesTexto;
	public JLabel lblConferirQuestoes;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblResultadoEx3;
	public JLabel lblResultadoEx4;
	public JLabel lblResultadoEx5;
	public JLabel lblResultadoEx6;
	public JLabel lblResultadoEx7;
	public JLabel lblResultadoEx8;
	public JLabel lblResultadoEx9;
	public JLabel lblResultadoEx10;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat113;
	public JTextField txtMat121, txtMat122, txtMat123;
	public JTextField txtMat131, txtMat132, txtMat133;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212;
	public JTextField txtMat221, txtMat222;

	// Matriz 3 Exercicio
	public JTextField txtMat311, txtMat312, txtMat313;
	public JTextField txtMat321, txtMat322, txtMat323;

	// Matriz 4 Exercicio
	public JTextField txtMat411, txtMat412;
	public JTextField txtMat421, txtMat422;

	// Matriz 5 Exercicio
	public JTextField txtMat511, txtMat512, txtMat513, txtMat514, txtMat515;
	public JTextField txtMat521, txtMat522, txtMat523, txtMat524, txtMat525;
	public JTextField txtMat531, txtMat532, txtMat533, txtMat534, txtMat535;
	public JTextField txtMat541, txtMat542, txtMat543, txtMat544, txtMat545;
	public JTextField txtMat551, txtMat552, txtMat553, txtMat554, txtMat555;

	// Matriz 6 Exercicio
	public JTextField txtMat611, txtMat612, txtMat613;
	public JTextField txtMat621, txtMat622, txtMat623;
	public JTextField txtMat631, txtMat632, txtMat633;

	// Matriz 7 Exercicio
	public JTextField txtMat711, txtMat712;
	public JTextField txtMat721, txtMat722;
	public JTextField txtMat731, txtMat732;

	// Matriz 8 Exercicio
	public JTextField txtMat811, txtMat812, txtMat813;
	public JTextField txtMat821, txtMat822, txtMat823;
	public JTextField txtMat831, txtMat832, txtMat833;

	// Matriz 9 Exercicio
	public JTextField txtMat911, txtMat912, txtMat913, txtMat914;
	public JTextField txtMat921, txtMat922, txtMat923, txtMat924;

	// Matriz 10 Exercicio
	public JTextField txtMat1011, txtMat1012, txtMat1013;
	public JTextField txtMat1021, txtMat1022, txtMat1023;
	public JTextField txtMat1031, txtMat1032, txtMat1033;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public MultiplicacaoDeMatrizQuestoesView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Quest�es de Multiplica��o de Matrizes");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800,2400));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblSubtracaoQuestoesTexto = new JLabel(imagens.multDeMatrizesTextoQuestoes);
		lblConferirQuestoes = new JLabel(imagens.conferirQuestoes);
		lblResultadoEx1 = new JLabel(imagens.certo);
		lblResultadoEx2 = new JLabel(imagens.certo);
		lblResultadoEx3 = new JLabel(imagens.certo);
		lblResultadoEx4 = new JLabel(imagens.certo);
		lblResultadoEx5 = new JLabel(imagens.certo);
		lblResultadoEx6 = new JLabel(imagens.certo);
		lblResultadoEx7 = new JLabel(imagens.certo);
		lblResultadoEx8 = new JLabel(imagens.certo);
		lblResultadoEx9 = new JLabel(imagens.certo);
		lblResultadoEx10 = new JLabel(imagens.certo);
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3); 
		txtMat112 = new JTextField("", 3); 
		txtMat113 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat123 = new JTextField("", 3);
		txtMat131 = new JTextField("", 3);
		txtMat132 = new JTextField("", 3);
		txtMat133 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3); 
		txtMat212 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);

		// Matriz 3 Exercicio Instanciando
		txtMat311 = new JTextField("", 3); 
		txtMat312 = new JTextField("", 3); 
		txtMat313 = new JTextField("", 3);
		txtMat321 = new JTextField("", 3); 
		txtMat322 = new JTextField("", 3); 
		txtMat323 = new JTextField("", 3);

		// Matriz 4 Exercicio Instanciando
		txtMat411 = new JTextField("", 3); 
		txtMat412 = new JTextField("", 3);
		txtMat421 = new JTextField("", 3); 
		txtMat422 = new JTextField("", 3);

		// Matriz 5 Exercicio Instanciando
		txtMat511 = new JTextField("", 3); 
		txtMat512 = new JTextField("", 3); 
		txtMat513 = new JTextField("", 3); 
		txtMat514 = new JTextField("", 3); 
		txtMat515 = new JTextField("", 3);
		txtMat521 = new JTextField("", 3); 
		txtMat522 = new JTextField("", 3); 
		txtMat523 = new JTextField("", 3); 
		txtMat524 = new JTextField("", 3); 
		txtMat525 = new JTextField("", 3);
		txtMat531 = new JTextField("", 3); 
		txtMat532 = new JTextField("", 3); 
		txtMat533 = new JTextField("", 3); 
		txtMat534 = new JTextField("", 3); 
		txtMat535 = new JTextField("", 3);
		txtMat541 = new JTextField("", 3); 
		txtMat542 = new JTextField("", 3); 
		txtMat543 = new JTextField("", 3); 
		txtMat544 = new JTextField("", 3); 
		txtMat545 = new JTextField("", 3);
		txtMat551 = new JTextField("", 3); 
		txtMat552 = new JTextField("", 3); 
		txtMat553 = new JTextField("", 3); 
		txtMat554 = new JTextField("", 3); 
		txtMat555 = new JTextField("", 3);

		// Matriz 6 Exercicio Instanciando
		txtMat611 = new JTextField("", 3); 
		txtMat612 = new JTextField("", 3); 
		txtMat613 = new JTextField("", 3);
		txtMat621 = new JTextField("", 3); 
		txtMat622 = new JTextField("", 3); 
		txtMat623 = new JTextField("", 3);
		txtMat631 = new JTextField("", 3); 
		txtMat632 = new JTextField("", 3); 
		txtMat633 = new JTextField("", 3);

		// Matriz 7 Exercicio Instanciando
		txtMat711 = new JTextField("", 3); 
		txtMat712 = new JTextField("", 3);
		txtMat721 = new JTextField("", 3); 
		txtMat722 = new JTextField("", 3);
		txtMat731 = new JTextField("", 3); 
		txtMat732 = new JTextField("", 3);

		// Matriz 8 Exercicio Instanciando
		txtMat811 = new JTextField("", 3); 
		txtMat812 = new JTextField("", 3); 
		txtMat813 = new JTextField("", 3);
		txtMat821 = new JTextField("", 3); 
		txtMat822 = new JTextField("", 3); 
		txtMat823 = new JTextField("", 3);
		txtMat831 = new JTextField("", 3); 
		txtMat832 = new JTextField("", 3); 
		txtMat833 = new JTextField("", 3);

		// Matriz 9 Exercicio Instanciando
		txtMat911 = new JTextField("", 3); 
		txtMat912 = new JTextField("", 3); 
		txtMat913 = new JTextField("", 3); 
		txtMat914 = new JTextField("", 3);
		txtMat921 = new JTextField("", 3); 
		txtMat922 = new JTextField("", 3); 
		txtMat923 = new JTextField("", 3); 
		txtMat924 = new JTextField("", 3);

		// Matriz 10 Exercicio Instanciando
		txtMat1011 = new JTextField("", 3); 
		txtMat1012 = new JTextField("", 3); 
		txtMat1013 = new JTextField("", 3);
		txtMat1021 = new JTextField("", 3); 
		txtMat1022 = new JTextField("", 3); 
		txtMat1023 = new JTextField("", 3);
		txtMat1031 = new JTextField("", 3); 
		txtMat1032 = new JTextField("", 3); 
		txtMat1033 = new JTextField("", 3);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblSubtracaoQuestoesTexto.setBounds(0, 50, 734, 2232);
		lblConferirQuestoes.setBounds(300, 2310, 237, 38);
		lblResultadoEx1.setBounds(590, 220, 75, 65);
		lblResultadoEx2.setBounds(540, 375, 75, 65);
		lblResultadoEx3.setBounds(550, 580, 75, 65);
		lblResultadoEx4.setBounds(570, 745, 75, 65);
		lblResultadoEx5.setBounds(700, 1120, 75, 65);
		lblResultadoEx6.setBounds(610, 1340, 75, 65);
		lblResultadoEx7.setBounds(550, 1545, 75, 65);
		lblResultadoEx8.setBounds(630, 1760, 75, 65);
		lblResultadoEx9.setBounds(660, 1955, 75, 65);
		lblResultadoEx10.setBounds(570, 2185, 75, 65);
		lblVolta.setBounds(30, 2350, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(450, 180, 30, 25);
		txtMat112.setBounds(490, 180, 30, 25);
		txtMat113.setBounds(530, 180, 30, 25);
		txtMat121.setBounds(450, 220, 30, 25);
		txtMat122.setBounds(490, 220, 30, 25);
		txtMat123.setBounds(530, 220, 30, 25);
		txtMat131.setBounds(450, 260, 30, 25);
		txtMat132.setBounds(490, 260, 30, 25);
		txtMat133.setBounds(530, 260, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(433, 375, 30, 25);
		txtMat212.setBounds(473, 375, 30, 25);
		txtMat221.setBounds(433, 415, 30, 25);
		txtMat222.setBounds(473, 415, 30, 25);

		// Posicionando Matriz 3
		txtMat311.setBounds(420, 580, 30, 25);
		txtMat312.setBounds(460, 580, 30, 25);
		txtMat313.setBounds(500, 580, 30, 25);
		txtMat321.setBounds(420, 620, 30, 25);
		txtMat322.setBounds(460, 620, 30, 25);
		txtMat323.setBounds(500, 620, 30, 25);

		// Posicionando Matriz 4
		txtMat411.setBounds(460, 745, 30, 25);
		txtMat412.setBounds(500, 745, 30, 25);
		txtMat421.setBounds(460, 785, 30, 25);
		txtMat422.setBounds(500, 785, 30, 25);
		
		// Posicionando Matriz 5
		txtMat511.setBounds(490, 1000, 30, 25);
		txtMat512.setBounds(530, 1000, 30, 25);
		txtMat513.setBounds(570, 1000, 30, 25);
		txtMat514.setBounds(610, 1000, 30, 25);
		txtMat515.setBounds(650, 1000, 30, 25);
		txtMat521.setBounds(490, 1040, 30, 25);
		txtMat522.setBounds(530, 1040, 30, 25);
		txtMat523.setBounds(570, 1040, 30, 25);
		txtMat524.setBounds(610, 1040, 30, 25);
		txtMat525.setBounds(650, 1040, 30, 25);
		txtMat531.setBounds(490, 1080, 30, 25);
		txtMat532.setBounds(530, 1080, 30, 25);
		txtMat533.setBounds(570, 1080, 30, 25);
		txtMat534.setBounds(610, 1080, 30, 25);
		txtMat535.setBounds(650, 1080, 30, 25);
		txtMat541.setBounds(490, 1120, 30, 25);
		txtMat542.setBounds(530, 1120, 30, 25);
		txtMat543.setBounds(570, 1120, 30, 25);
		txtMat544.setBounds(610, 1120, 30, 25);
		txtMat545.setBounds(650, 1120, 30, 25);
		txtMat551.setBounds(490, 1160, 30, 25);
		txtMat552.setBounds(530, 1160, 30, 25);
		txtMat553.setBounds(570, 1160, 30, 25);
		txtMat554.setBounds(610, 1160, 30, 25);
		txtMat555.setBounds(650, 1160, 30, 25);

		// Posicionando Matriz 6
		txtMat611.setBounds(460, 1300, 30, 25);
		txtMat612.setBounds(500, 1300, 30, 25);
		txtMat613.setBounds(540, 1300, 30, 25);
		txtMat621.setBounds(460, 1340, 30, 25);
		txtMat622.setBounds(500, 1340, 30, 25);
		txtMat623.setBounds(540, 1340, 30, 25);
		txtMat631.setBounds(460, 1380, 30, 25);
		txtMat632.setBounds(500, 1380, 30, 25);
		txtMat633.setBounds(540, 1380, 30, 25);

		// Posicionando Matriz 7
		txtMat711.setBounds(440, 1505, 30, 25);
		txtMat712.setBounds(480, 1505, 30, 25);
		txtMat721.setBounds(440, 1545, 30, 25);
		txtMat722.setBounds(480, 1545, 30, 25);
		txtMat731.setBounds(440, 1585, 30, 25);
		txtMat732.setBounds(480, 1585, 30, 25);

		// Posicionando Matriz 8
		txtMat811.setBounds(470, 1720, 30, 25);
		txtMat812.setBounds(510, 1720, 30, 25);
		txtMat813.setBounds(550, 1720, 30, 25);
		txtMat821.setBounds(470, 1760, 30, 25);
		txtMat822.setBounds(510, 1760, 30, 25);
		txtMat823.setBounds(550, 1760, 30, 25);
		txtMat831.setBounds(470, 1800, 30, 25);
		txtMat832.setBounds(510, 1800, 30, 25);
		txtMat833.setBounds(550, 1800, 30, 25);

		// Posicionando Matriz 9
		txtMat911.setBounds(470, 1955, 30, 25);
		txtMat912.setBounds(510, 1955, 30, 25);
		txtMat913.setBounds(550, 1955, 30, 25);
		txtMat914.setBounds(590, 1955, 30, 25);
		txtMat921.setBounds(470, 1995, 30, 25);
		txtMat922.setBounds(510, 1995, 30, 25);
		txtMat923.setBounds(550, 1995, 30, 25);
		txtMat924.setBounds(590, 1995, 30, 25);

		// Posicionando Matriz 10
		txtMat1011.setBounds(420, 2145, 30, 25);
		txtMat1012.setBounds(460, 2145, 30, 25);
		txtMat1013.setBounds(500, 2145, 30, 25);
		txtMat1021.setBounds(420, 2185, 30, 25);
		txtMat1022.setBounds(460, 2185, 30, 25);
		txtMat1023.setBounds(500, 2185, 30, 25);
		txtMat1031.setBounds(420, 2225, 30, 25);
		txtMat1032.setBounds(460, 2225, 30, 25);
		txtMat1033.setBounds(500, 2225, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblSubtracaoQuestoesTexto);
		paneil.add(lblConferirQuestoes);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblResultadoEx3);
		paneil.add(lblResultadoEx4);
		paneil.add(lblResultadoEx5);
		paneil.add(lblResultadoEx6);
		paneil.add(lblResultadoEx7);
		paneil.add(lblResultadoEx8);
		paneil.add(lblResultadoEx9);
		paneil.add(lblResultadoEx10);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat113);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat123);
		paneil.add(txtMat131);
		paneil.add(txtMat132);
		paneil.add(txtMat133);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat221);
		paneil.add(txtMat222);

		// Adicionando a Matriz 3
		paneil.add(txtMat311);
		paneil.add(txtMat312);
		paneil.add(txtMat313);
		paneil.add(txtMat321);
		paneil.add(txtMat322);
		paneil.add(txtMat323);

		// Adicionando a Matriz 4
		paneil.add(txtMat411);
		paneil.add(txtMat412);
		paneil.add(txtMat421);
		paneil.add(txtMat422);

		// Adicionando a Matriz 5
		paneil.add(txtMat511);
		paneil.add(txtMat512);
		paneil.add(txtMat513);
		paneil.add(txtMat514);
		paneil.add(txtMat515);
		paneil.add(txtMat521);
		paneil.add(txtMat522);
		paneil.add(txtMat523);
		paneil.add(txtMat524);
		paneil.add(txtMat525);
		paneil.add(txtMat531);
		paneil.add(txtMat532);
		paneil.add(txtMat533);
		paneil.add(txtMat534);
		paneil.add(txtMat535);
		paneil.add(txtMat541);
		paneil.add(txtMat542);
		paneil.add(txtMat543);
		paneil.add(txtMat544);
		paneil.add(txtMat545);
		paneil.add(txtMat551);
		paneil.add(txtMat552);
		paneil.add(txtMat553);
		paneil.add(txtMat554);
		paneil.add(txtMat555);

		// Adicionando a Matriz 6
		paneil.add(txtMat611);
		paneil.add(txtMat612);
		paneil.add(txtMat613);
		paneil.add(txtMat621);
		paneil.add(txtMat622);
		paneil.add(txtMat623);
		paneil.add(txtMat631);
		paneil.add(txtMat632);
		paneil.add(txtMat633);

		// Adicionando a Matriz 7
		paneil.add(txtMat711);
		paneil.add(txtMat712);
		paneil.add(txtMat721);
		paneil.add(txtMat722);
		paneil.add(txtMat731);
		paneil.add(txtMat732);

		// Adicionando a Matriz 8
		paneil.add(txtMat811);
		paneil.add(txtMat812);
		paneil.add(txtMat813);
		paneil.add(txtMat821);
		paneil.add(txtMat822);
		paneil.add(txtMat823);
		paneil.add(txtMat831);
		paneil.add(txtMat832);
		paneil.add(txtMat833);

		// Adicionando a Matriz 9
		paneil.add(txtMat911);
		paneil.add(txtMat912);
		paneil.add(txtMat913);
		paneil.add(txtMat914);
		paneil.add(txtMat921);
		paneil.add(txtMat922);
		paneil.add(txtMat923);
		paneil.add(txtMat924);

		// Adicionando a Matriz 10
		paneil.add(txtMat1011);
		paneil.add(txtMat1012);
		paneil.add(txtMat1013);
		paneil.add(txtMat1021);
		paneil.add(txtMat1022);
		paneil.add(txtMat1023);
		paneil.add(txtMat1031);
		paneil.add(txtMat1032);
		paneil.add(txtMat1033);

		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final MultiplicacaoDeMatrizQuestoesController multQuestoesController = new MultiplicacaoDeMatrizQuestoesController(this); 
		multQuestoesController.addObserver(this);
		lblConferirQuestoes.addMouseListener(multQuestoesController);
		lblVolta.addMouseListener(multQuestoesController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}