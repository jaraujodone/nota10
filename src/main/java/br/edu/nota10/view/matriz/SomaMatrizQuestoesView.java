/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.SomaMatrizQuestoesController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class SomaMatrizQuestoesView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblSomaQuestoesTexto;
	public JLabel lblConferirQuestoes;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblResultadoEx3;
	public JLabel lblResultadoEx4;
	public JLabel lblResultadoEx5;
	public JLabel lblResultadoEx6;
	public JLabel lblResultadoEx7;
	public JLabel lblResultadoEx8;
	public JLabel lblResultadoEx9;
	public JLabel lblResultadoEx10;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat113, txtMat114, txtMat115;
	public JTextField txtMat121, txtMat122, txtMat123, txtMat124, txtMat125;
	public JTextField txtMat131, txtMat132, txtMat133, txtMat134, txtMat135;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212, txtMat213, txtMat214;
	public JTextField txtMat221, txtMat222, txtMat223, txtMat224;
	public JTextField txtMat231, txtMat232, txtMat233, txtMat234;
	public JTextField txtMat241, txtMat242, txtMat243, txtMat244;

	// Matriz 3 Exercicio
	public JTextField txtMat311, txtMat312, txtMat313;
	public JTextField txtMat321, txtMat322, txtMat323;
	public JTextField txtMat331, txtMat332, txtMat333;

	// Matriz 4 Exercicio
	public JTextField txtMat411, txtMat412, txtMat413, txtMat414, txtMat415;
	public JTextField txtMat421, txtMat422, txtMat423, txtMat424, txtMat425;
	public JTextField txtMat431, txtMat432, txtMat433, txtMat434, txtMat435;
	public JTextField txtMat441, txtMat442, txtMat443, txtMat444, txtMat445;
	public JTextField txtMat451, txtMat452, txtMat453, txtMat454, txtMat455;

	// Matriz 5 Exercicio
	public JTextField txtMat511, txtMat512, txtMat513, txtMat514;
	public JTextField txtMat521, txtMat522, txtMat523, txtMat524;
	public JTextField txtMat531, txtMat532, txtMat533, txtMat534;

	// Matriz 6 Exercicio
	public JTextField txtMat611, txtMat612, txtMat613;
	public JTextField txtMat621, txtMat622, txtMat623;
	public JTextField txtMat631, txtMat632, txtMat633;

	// Matriz 7 Exercicio
	public JTextField txtMat711, txtMat712, txtMat713, txtMat714;
	public JTextField txtMat721, txtMat722, txtMat723, txtMat724;
	public JTextField txtMat731, txtMat732, txtMat733, txtMat734;
	public JTextField txtMat741, txtMat742, txtMat743, txtMat744;

	// Matriz 8 Exercicio
	public JTextField txtMat811, txtMat812, txtMat813, txtMat814;
	public JTextField txtMat821, txtMat822, txtMat823, txtMat824;
	public JTextField txtMat831, txtMat832, txtMat833, txtMat834;

	// Matriz 9 Exercicio
	public JTextField txtMat911, txtMat912, txtMat913, txtMat914;
	public JTextField txtMat921, txtMat922, txtMat923, txtMat924;
	public JTextField txtMat931, txtMat932, txtMat933, txtMat934;

	// Matriz 10 Exercicio
	public JTextField txtMat1011, txtMat1012, txtMat1013;
	public JTextField txtMat1021, txtMat1022, txtMat1023;
	public JTextField txtMat1031, txtMat1032, txtMat1033;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public SomaMatrizQuestoesView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Quest�es de Adi��o de Matrizes");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800,2600));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblSomaQuestoesTexto = new JLabel(imagens.adicacaoTextoQuestoes);
		lblConferirQuestoes = new JLabel(imagens.conferirQuestoes);
		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblResultadoEx3 = new JLabel();
		lblResultadoEx4 = new JLabel();
		lblResultadoEx5 = new JLabel();
		lblResultadoEx6 = new JLabel();
		lblResultadoEx7 = new JLabel();
		lblResultadoEx8 = new JLabel();
		lblResultadoEx9 = new JLabel();
		lblResultadoEx10 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3);
		txtMat112 = new JTextField("", 3);
		txtMat113 = new JTextField("", 3);
		txtMat114 = new JTextField("", 3);
		txtMat115 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat123 = new JTextField("", 3);
		txtMat124 = new JTextField("", 3);
		txtMat125 = new JTextField("", 3);
		txtMat131 = new JTextField("", 3);
		txtMat132 = new JTextField("", 3);
		txtMat133 = new JTextField("", 3);
		txtMat134 = new JTextField("", 3);
		txtMat135 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3);
		txtMat212 = new JTextField("", 3);
		txtMat213 = new JTextField("", 3);
		txtMat214 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);
		txtMat223 = new JTextField("", 3);
		txtMat224 = new JTextField("", 3);
		txtMat231 = new JTextField("", 3);
		txtMat232 = new JTextField("", 3);
		txtMat233 = new JTextField("", 3);
		txtMat234 = new JTextField("", 3);
		txtMat241 = new JTextField("", 3);
		txtMat242 = new JTextField("", 3);
		txtMat243 = new JTextField("", 3);
		txtMat244 = new JTextField("", 3);

		// Matriz 3 Exercicio Instanciando
		txtMat311 = new JTextField("", 3);
		txtMat312 = new JTextField("", 3);
		txtMat313 = new JTextField("", 3);
		txtMat321 = new JTextField("", 3);
		txtMat322 = new JTextField("", 3);
		txtMat323 = new JTextField("", 3);
		txtMat331 = new JTextField("", 3);
		txtMat332 = new JTextField("", 3);
		txtMat333 = new JTextField("", 3);

		// Matriz 4 Exercicio Instanciando
		txtMat411 = new JTextField("", 3);
		txtMat412 = new JTextField("", 3);
		txtMat413 = new JTextField("", 3);
		txtMat414 = new JTextField("", 3);
		txtMat415 = new JTextField("", 3);
		txtMat421 = new JTextField("", 3);
		txtMat422 = new JTextField("", 3);
		txtMat423 = new JTextField("", 3);
		txtMat424 = new JTextField("", 3);
		txtMat425 = new JTextField("", 3);
		txtMat431 = new JTextField("", 3);
		txtMat432 = new JTextField("", 3);
		txtMat433 = new JTextField("", 3);
		txtMat434 = new JTextField("", 3);
		txtMat435 = new JTextField("", 3);
		txtMat441 = new JTextField("", 3);
		txtMat442 = new JTextField("", 3);
		txtMat443 = new JTextField("", 3);
		txtMat444 = new JTextField("", 3);
		txtMat445 = new JTextField("", 3);
		txtMat451 = new JTextField("", 3);
		txtMat452 = new JTextField("", 3);
		txtMat453 = new JTextField("", 3);
		txtMat454 = new JTextField("", 3);
		txtMat455 = new JTextField("", 3);

		// Matriz 5 Exercicio Instanciando
		txtMat511 = new JTextField("", 3);
		txtMat512 = new JTextField("", 3);
		txtMat513 = new JTextField("", 3);
		txtMat514 = new JTextField("", 3);
		txtMat521 = new JTextField("", 3);
		txtMat522 = new JTextField("", 3);
		txtMat523 = new JTextField("", 3);
		txtMat524 = new JTextField("", 3);
		txtMat531 = new JTextField("", 3);
		txtMat532 = new JTextField("", 3);
		txtMat533 = new JTextField("", 3);
		txtMat534 = new JTextField("", 3);

		// Matriz 6 Exercicio Instanciando
		txtMat611 = new JTextField("", 3);
		txtMat612 = new JTextField("", 3);
		txtMat613 = new JTextField("", 3);
		txtMat621 = new JTextField("", 3);
		txtMat622 = new JTextField("", 3);
		txtMat623 = new JTextField("", 3);
		txtMat631 = new JTextField("", 3);
		txtMat632 = new JTextField("", 3);
		txtMat633 = new JTextField("", 3);

		// Matriz 7 Exercicio Instanciando
		txtMat711 = new JTextField("", 3);
		txtMat712 = new JTextField("", 3);
		txtMat713 = new JTextField("", 3);
		txtMat714 = new JTextField("", 3);
		txtMat721 = new JTextField("", 3);
		txtMat722 = new JTextField("", 3);
		txtMat723 = new JTextField("", 3);
		txtMat724 = new JTextField("", 3);
		txtMat731 = new JTextField("", 3);
		txtMat732 = new JTextField("", 3);
		txtMat733 = new JTextField("", 3);
		txtMat734 = new JTextField("", 3);
		txtMat741 = new JTextField("", 3);
		txtMat742 = new JTextField("", 3);
		txtMat743 = new JTextField("", 3);
		txtMat744 = new JTextField("", 3);

		// Matriz 8 Exercicio Instanciando
		txtMat811 = new JTextField("", 3);
		txtMat812 = new JTextField("", 3);
		txtMat813 = new JTextField("", 3);
		txtMat814 = new JTextField("", 3);
		txtMat821 = new JTextField("", 3);
		txtMat822 = new JTextField("", 3);
		txtMat823 = new JTextField("", 3);
		txtMat824 = new JTextField("", 3);
		txtMat831 = new JTextField("", 3);
		txtMat832 = new JTextField("", 3);
		txtMat833 = new JTextField("", 3);
		txtMat834 = new JTextField("", 3);

		// Matriz 9 Exercicio Instanciando
		txtMat911 = new JTextField("", 3);
		txtMat912 = new JTextField("", 3);
		txtMat913 = new JTextField("", 3);
		txtMat914 = new JTextField("", 3);
		txtMat921 = new JTextField("", 3);
		txtMat922 = new JTextField("", 3);
		txtMat923 = new JTextField("", 3);
		txtMat924 = new JTextField("", 3);
		txtMat931 = new JTextField("", 3);
		txtMat932 = new JTextField("", 3);
		txtMat933 = new JTextField("", 3);
		txtMat934 = new JTextField("", 3);

		// Matriz 10 Exercicio Instanciando
		txtMat1011 = new JTextField("", 3);
		txtMat1012 = new JTextField("", 3);
		txtMat1013 = new JTextField("", 3);
		txtMat1021 = new JTextField("", 3);
		txtMat1022 = new JTextField("", 3);
		txtMat1023 = new JTextField("", 3);
		txtMat1031 = new JTextField("", 3);
		txtMat1032 = new JTextField("", 3);
		txtMat1033 = new JTextField("", 3);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblSomaQuestoesTexto.setBounds(0, 50, 741, 2449);
		lblConferirQuestoes.setBounds(300, 2530, 237, 38);
		lblResultadoEx1.setBounds(690, 230, 75, 65);
		lblResultadoEx2.setBounds(650, 475, 75, 65);
		lblResultadoEx3.setBounds(650, 690, 75, 65);
		lblResultadoEx4.setBounds(700, 1000, 75, 65);
		lblResultadoEx5.setBounds(690, 1260, 75, 65);
		lblResultadoEx6.setBounds(650, 1465, 75, 65);
		lblResultadoEx7.setBounds(680, 1725, 75, 65);
		lblResultadoEx8.setBounds(670, 1950, 75, 65);
		lblResultadoEx9.setBounds(680, 2195, 75, 65);
		lblResultadoEx10.setBounds(650, 2400, 75, 65);
		lblVolta.setBounds(30, 2560, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(480, 190, 30, 25);
		txtMat112.setBounds(520, 190, 30, 25);
		txtMat113.setBounds(560, 190, 30, 25);
		txtMat114.setBounds(600, 190, 30, 25);
		txtMat115.setBounds(640, 190, 30, 25);
		txtMat121.setBounds(480, 230, 30, 25);
		txtMat122.setBounds(520, 230, 30, 25);
		txtMat123.setBounds(560, 230, 30, 25);
		txtMat124.setBounds(600, 230, 30, 25);
		txtMat125.setBounds(640, 230, 30, 25);
		txtMat131.setBounds(480, 270, 30, 25);
		txtMat132.setBounds(520, 270, 30, 25);
		txtMat133.setBounds(560, 270, 30, 25);
		txtMat134.setBounds(600, 270, 30, 25);
		txtMat135.setBounds(640, 270, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(475, 395, 30, 25);
		txtMat212.setBounds(515, 395, 30, 25);
		txtMat213.setBounds(555, 395, 30, 25);
		txtMat214.setBounds(595, 395, 30, 25);
		txtMat221.setBounds(475, 435, 30, 25);
		txtMat222.setBounds(515, 435, 30, 25);
		txtMat223.setBounds(555, 435, 30, 25);
		txtMat224.setBounds(595, 435, 30, 25);
		txtMat231.setBounds(475, 475, 30, 25);
		txtMat232.setBounds(515, 475, 30, 25);
		txtMat233.setBounds(555, 475, 30, 25);
		txtMat234.setBounds(595, 475, 30, 25);
		txtMat241.setBounds(475, 515, 30, 25);
		txtMat242.setBounds(515, 515, 30, 25);
		txtMat243.setBounds(555, 515, 30, 25);
		txtMat244.setBounds(595, 515, 30, 25);

		// Posicionando Matriz 3
		txtMat311.setBounds(510, 665, 30, 25);
		txtMat312.setBounds(550, 665, 30, 25);
		txtMat313.setBounds(590, 665, 30, 25);
		txtMat321.setBounds(510, 705, 30, 25);
		txtMat322.setBounds(550, 705, 30, 25);
		txtMat323.setBounds(590, 705, 30, 25);
		txtMat331.setBounds(510, 745, 30, 25);
		txtMat332.setBounds(550, 745, 30, 25);
		txtMat333.setBounds(590, 745, 30, 25);

		// Posicionando Matriz 4
		txtMat411.setBounds(495, 895, 30, 25);
		txtMat412.setBounds(535, 895, 30, 25);
		txtMat413.setBounds(575, 895, 30, 25);
		txtMat414.setBounds(615, 895, 30, 25);
		txtMat415.setBounds(655, 895, 30, 25);
		txtMat421.setBounds(495, 935, 30, 25);
		txtMat422.setBounds(535, 935, 30, 25);
		txtMat423.setBounds(575, 935, 30, 25);
		txtMat424.setBounds(615, 935, 30, 25);
		txtMat425.setBounds(655, 935, 30, 25);
		txtMat431.setBounds(495, 975, 30, 25);
		txtMat432.setBounds(535, 975, 30, 25);
		txtMat433.setBounds(575, 975, 30, 25);
		txtMat434.setBounds(615, 975, 30, 25);
		txtMat435.setBounds(655, 975, 30, 25);
		txtMat441.setBounds(495, 1015, 30, 25);
		txtMat442.setBounds(535, 1015, 30, 25);
		txtMat443.setBounds(575, 1015, 30, 25);
		txtMat444.setBounds(615, 1015, 30, 25);
		txtMat445.setBounds(655, 1015, 30, 25);
		txtMat451.setBounds(495, 1055, 30, 25);
		txtMat452.setBounds(535, 1055, 30, 25);
		txtMat453.setBounds(575, 1055, 30, 25);
		txtMat454.setBounds(615, 1055, 30, 25);
		txtMat455.setBounds(655, 1055, 30, 25);

		// Posicionando Matriz 5
		txtMat511.setBounds(500, 1220, 30, 25);
		txtMat512.setBounds(540, 1220, 30, 25);
		txtMat513.setBounds(580, 1220, 30, 25);
		txtMat514.setBounds(620, 1220, 30, 25);
		txtMat521.setBounds(500, 1260, 30, 25);
		txtMat522.setBounds(540, 1260, 30, 25);
		txtMat523.setBounds(580, 1260, 30, 25);
		txtMat524.setBounds(620, 1260, 30, 25);
		txtMat531.setBounds(500, 1300, 30, 25);
		txtMat532.setBounds(540, 1300, 30, 25);
		txtMat533.setBounds(580, 1300, 30, 25);
		txtMat534.setBounds(620, 1300, 30, 25);

		// Posicionando Matriz 6
		txtMat611.setBounds(490, 1425, 30, 25);
		txtMat612.setBounds(530, 1425, 30, 25);
		txtMat613.setBounds(570, 1425, 30, 25);
		txtMat621.setBounds(490, 1465, 30, 25);
		txtMat622.setBounds(530, 1465, 30, 25);
		txtMat623.setBounds(570, 1465, 30, 25);
		txtMat631.setBounds(490, 1505, 30, 25);
		txtMat632.setBounds(530, 1505, 30, 25);
		txtMat633.setBounds(570, 1505, 30, 25);

		// Posicionando Matriz 7
		txtMat711.setBounds(495, 1645, 30, 25);
		txtMat712.setBounds(535, 1645, 30, 25);
		txtMat713.setBounds(575, 1645, 30, 25);
		txtMat714.setBounds(615, 1645, 30, 25);
		txtMat721.setBounds(495, 1685, 30, 25);
		txtMat722.setBounds(535, 1685, 30, 25);
		txtMat723.setBounds(575, 1685, 30, 25);
		txtMat724.setBounds(615, 1685, 30, 25);
		txtMat731.setBounds(495, 1725, 30, 25);
		txtMat732.setBounds(535, 1725, 30, 25);
		txtMat733.setBounds(575, 1725, 30, 25);
		txtMat734.setBounds(615, 1725, 30, 25);
		txtMat741.setBounds(495, 1765, 30, 25);
		txtMat742.setBounds(535, 1765, 30, 25);
		txtMat743.setBounds(575, 1765, 30, 25);
		txtMat744.setBounds(615, 1765, 30, 25);

		// Posicionando Matriz 8
		txtMat811.setBounds(495, 1910, 30, 25);
		txtMat812.setBounds(535, 1910, 30, 25);
		txtMat813.setBounds(575, 1910, 30, 25);
		txtMat814.setBounds(615, 1910, 30, 25);
		txtMat821.setBounds(495, 1950, 30, 25);
		txtMat822.setBounds(535, 1950, 30, 25);
		txtMat823.setBounds(575, 1950, 30, 25);
		txtMat824.setBounds(615, 1950, 30, 25);
		txtMat831.setBounds(495, 1990, 30, 25);
		txtMat832.setBounds(535, 1990, 30, 25);
		txtMat833.setBounds(575, 1990, 30, 25);
		txtMat834.setBounds(615, 1990, 30, 25);

		// Posicionando Matriz 9
		txtMat911.setBounds(500, 2155, 30, 25);
		txtMat912.setBounds(540, 2155, 30, 25);
		txtMat913.setBounds(580, 2155, 30, 25);
		txtMat914.setBounds(620, 2155, 30, 25);
		txtMat921.setBounds(500, 2195, 30, 25);
		txtMat922.setBounds(540, 2195, 30, 25);
		txtMat923.setBounds(580, 2195, 30, 25);
		txtMat924.setBounds(620, 2195, 30, 25);
		txtMat931.setBounds(500, 2235, 30, 25);
		txtMat932.setBounds(540, 2235, 30, 25);
		txtMat933.setBounds(580, 2235, 30, 25);
		txtMat934.setBounds(620, 2235, 30, 25);

		// Posicionando Matriz 10
		txtMat1011.setBounds(500, 2360, 30, 25);
		txtMat1012.setBounds(540, 2360, 30, 25);
		txtMat1013.setBounds(580, 2360, 30, 25);
		txtMat1021.setBounds(500, 2400, 30, 25);
		txtMat1022.setBounds(540, 2400, 30, 25);
		txtMat1023.setBounds(580, 2400, 30, 25);
		txtMat1031.setBounds(500, 2440, 30, 25);
		txtMat1032.setBounds(540, 2440, 30, 25);
		txtMat1033.setBounds(580, 2440, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblSomaQuestoesTexto);
		paneil.add(lblConferirQuestoes);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblResultadoEx3);
		paneil.add(lblResultadoEx4);
		paneil.add(lblResultadoEx5);
		paneil.add(lblResultadoEx6);
		paneil.add(lblResultadoEx7);
		paneil.add(lblResultadoEx8);
		paneil.add(lblResultadoEx9);
		paneil.add(lblResultadoEx10);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat113);
		paneil.add(txtMat114);
		paneil.add(txtMat115);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat123);
		paneil.add(txtMat124);
		paneil.add(txtMat125);
		paneil.add(txtMat131);
		paneil.add(txtMat132);
		paneil.add(txtMat133);
		paneil.add(txtMat134);
		paneil.add(txtMat135);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat213);
		paneil.add(txtMat214);
		paneil.add(txtMat221);
		paneil.add(txtMat222);
		paneil.add(txtMat223);
		paneil.add(txtMat224);
		paneil.add(txtMat231);
		paneil.add(txtMat232);
		paneil.add(txtMat233);
		paneil.add(txtMat234);
		paneil.add(txtMat241);
		paneil.add(txtMat242);
		paneil.add(txtMat243);
		paneil.add(txtMat244);

		// Adicionando a Matriz 3
		paneil.add(txtMat311);
		paneil.add(txtMat312);
		paneil.add(txtMat313);
		paneil.add(txtMat321);
		paneil.add(txtMat322);
		paneil.add(txtMat323);
		paneil.add(txtMat331);
		paneil.add(txtMat332);
		paneil.add(txtMat333);

		// Adicionando a Matriz 4
		paneil.add(txtMat411);
		paneil.add(txtMat412);
		paneil.add(txtMat413);
		paneil.add(txtMat414);
		paneil.add(txtMat415);
		paneil.add(txtMat421);
		paneil.add(txtMat422);
		paneil.add(txtMat423);
		paneil.add(txtMat424);
		paneil.add(txtMat425);
		paneil.add(txtMat431);
		paneil.add(txtMat432);
		paneil.add(txtMat433);
		paneil.add(txtMat434);
		paneil.add(txtMat435);
		paneil.add(txtMat441);
		paneil.add(txtMat442);
		paneil.add(txtMat443);
		paneil.add(txtMat444);
		paneil.add(txtMat445);
		paneil.add(txtMat451);
		paneil.add(txtMat452);
		paneil.add(txtMat453);
		paneil.add(txtMat454);
		paneil.add(txtMat455);

		// Adicionando a Matriz 5
		paneil.add(txtMat511);
		paneil.add(txtMat512);
		paneil.add(txtMat513);
		paneil.add(txtMat514);
		paneil.add(txtMat521);
		paneil.add(txtMat522);
		paneil.add(txtMat523);
		paneil.add(txtMat524);
		paneil.add(txtMat531);
		paneil.add(txtMat532);
		paneil.add(txtMat533);
		paneil.add(txtMat534);

		// Adicionando a Matriz 6
		paneil.add(txtMat611);
		paneil.add(txtMat612);
		paneil.add(txtMat613);
		paneil.add(txtMat621);
		paneil.add(txtMat622);
		paneil.add(txtMat623);
		paneil.add(txtMat631);
		paneil.add(txtMat632);
		paneil.add(txtMat633);

		// Adicionando a Matriz 7
		paneil.add(txtMat711);
		paneil.add(txtMat712);
		paneil.add(txtMat713);
		paneil.add(txtMat714);
		paneil.add(txtMat721);
		paneil.add(txtMat722);
		paneil.add(txtMat723);
		paneil.add(txtMat724);
		paneil.add(txtMat731);
		paneil.add(txtMat732);
		paneil.add(txtMat733);
		paneil.add(txtMat734);
		paneil.add(txtMat741);
		paneil.add(txtMat742);
		paneil.add(txtMat743);
		paneil.add(txtMat744);

		// Adicionando a Matriz 8
		paneil.add(txtMat811);
		paneil.add(txtMat812);
		paneil.add(txtMat813);
		paneil.add(txtMat814);
		paneil.add(txtMat821);
		paneil.add(txtMat822);
		paneil.add(txtMat823);
		paneil.add(txtMat824);
		paneil.add(txtMat831);
		paneil.add(txtMat832);
		paneil.add(txtMat833);
		paneil.add(txtMat834);

		// Adicionando a Matriz 9
		paneil.add(txtMat911);
		paneil.add(txtMat912);
		paneil.add(txtMat913);
		paneil.add(txtMat914);
		paneil.add(txtMat921);
		paneil.add(txtMat922);
		paneil.add(txtMat923);
		paneil.add(txtMat924);
		paneil.add(txtMat931);
		paneil.add(txtMat932);
		paneil.add(txtMat933);
		paneil.add(txtMat934);

		// Adicionando a Matriz 10
		paneil.add(txtMat1011);
		paneil.add(txtMat1012);
		paneil.add(txtMat1013);
		paneil.add(txtMat1021);
		paneil.add(txtMat1022);
		paneil.add(txtMat1023);
		paneil.add(txtMat1031);
		paneil.add(txtMat1032);
		paneil.add(txtMat1033);

		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final SomaMatrizQuestoesController somaQuestoesController = new SomaMatrizQuestoesController(this); 
		somaQuestoesController.addObserver(this);
		lblConferirQuestoes.addMouseListener(somaQuestoesController);
		lblVolta.addMouseListener(somaQuestoesController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}