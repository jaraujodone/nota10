/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.MultiplicacaoDeMatrizTutorialController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class MultiplicacaoDeMatrizTutorialView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblMultMatrizTutorialTexto;
	public JLabel lblConferirEx1;
	public JLabel lblConferirEx2;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat121, txtMat122, txtMat131, txtMat132;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212, txtMat213, txtMat221, txtMat222, txtMat223, txtMat231, txtMat232, txtMat233;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public MultiplicacaoDeMatrizTutorialView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Tutorial Adi��o de Matrizes");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800, 2100));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblMultMatrizTutorialTexto = new JLabel(imagens.multiplicacaoTextoTutorial);
		lblConferirEx1 = new JLabel(imagens.conferir);
		lblConferirEx2 = new JLabel(imagens.conferir);
		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3);
		txtMat112 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat131 = new JTextField("", 3);
		txtMat132 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3);
		txtMat212 = new JTextField("", 3);
		txtMat213 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);
		txtMat223 = new JTextField("", 3);
		txtMat231 = new JTextField("", 3);
		txtMat232 = new JTextField("", 3);
		txtMat233 = new JTextField("", 3);

		// Posicionando
		imgFundo.setBounds(0, 0, 800, 3500);
		lblMultMatrizTutorialTexto.setBounds(20, 50, 726, 1985);
		lblConferirEx1.setBounds(480, 1810, 151, 38);
		lblConferirEx2.setBounds(530, 2010, 151, 38);
		lblResultadoEx1.setBounds(570, 1725, 75, 65);
		lblResultadoEx2.setBounds(690, 1905, 75, 65);
		lblVolta.setBounds(30, 2050, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(455, 1690, 30, 25);
		txtMat112.setBounds(515, 1690, 30, 25);
		txtMat121.setBounds(455, 1730, 30, 25);
		txtMat122.setBounds(515, 1730, 30, 25);
		txtMat131.setBounds(455, 1770, 30, 25);
		txtMat132.setBounds(515, 1770, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(515, 1880, 30, 25);
		txtMat212.setBounds(575, 1880, 30, 25);
		txtMat213.setBounds(635, 1880, 30, 25);
		txtMat221.setBounds(515, 1920, 30, 25);
		txtMat222.setBounds(575, 1920, 30, 25);
		txtMat223.setBounds(635, 1920, 30, 25);
		txtMat231.setBounds(515, 1960, 30, 25);
		txtMat232.setBounds(575, 1960, 30, 25);
		txtMat233.setBounds(635, 1960, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblMultMatrizTutorialTexto);
		paneil.add(lblConferirEx1);
		paneil.add(lblConferirEx2);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat131);
		paneil.add(txtMat132);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat213);
		paneil.add(txtMat221);
		paneil.add(txtMat222);
		paneil.add(txtMat223);
		paneil.add(txtMat231);
		paneil.add(txtMat232);
		paneil.add(txtMat233);

		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final MultiplicacaoDeMatrizTutorialController multDeMatrizTutorialController = 
				new MultiplicacaoDeMatrizTutorialController(this);
		multDeMatrizTutorialController.addObserver(this);
		lblConferirEx1.addMouseListener(multDeMatrizTutorialController);
		lblConferirEx2.addMouseListener(multDeMatrizTutorialController);
		lblVolta.addMouseListener(multDeMatrizTutorialController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) { }
}