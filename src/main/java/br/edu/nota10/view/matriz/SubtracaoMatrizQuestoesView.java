/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.SubtracaoMatrizQuestoesController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class SubtracaoMatrizQuestoesView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblSubtracaoQuestoesTexto;
	public JLabel lblConferirQuestoes;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblResultadoEx3;
	public JLabel lblResultadoEx4;
	public JLabel lblResultadoEx5;
	public JLabel lblResultadoEx6;
	public JLabel lblResultadoEx7;
	public JLabel lblResultadoEx8;
	public JLabel lblResultadoEx9;
	public JLabel lblResultadoEx10;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat113;
	public JTextField txtMat121, txtMat122, txtMat123;
	public JTextField txtMat131, txtMat132, txtMat133;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212, txtMat213;
	public JTextField txtMat221, txtMat222, txtMat223;
	public JTextField txtMat231, txtMat232, txtMat233;

	// Matriz 3 Exercicio
	public JTextField txtMat311, txtMat312, txtMat313;
	public JTextField txtMat321, txtMat322, txtMat323;
	public JTextField txtMat331, txtMat332, txtMat333;
	public JTextField txtMat341, txtMat342, txtMat343;

	// Matriz 4 Exercicio
	public JTextField txtMat411, txtMat412, txtMat413, txtMat414;
	public JTextField txtMat421, txtMat422, txtMat423, txtMat424;
	public JTextField txtMat431, txtMat432, txtMat433, txtMat434;
	public JTextField txtMat441, txtMat442, txtMat443, txtMat444;

	// Matriz 5 Exercicio
	public JTextField txtMat511, txtMat512, txtMat513, txtMat514;
	public JTextField txtMat521, txtMat522, txtMat523, txtMat524;
	public JTextField txtMat531, txtMat532, txtMat533, txtMat534;
	public JTextField txtMat541, txtMat542, txtMat543, txtMat544;
	public JTextField txtMat551, txtMat552, txtMat553, txtMat554;

	// Matriz 6 Exercicio
	public JTextField txtMat611, txtMat612, txtMat613, txtMat614;
	public JTextField txtMat621, txtMat622, txtMat623, txtMat624;
	public JTextField txtMat631, txtMat632, txtMat633, txtMat634;

	// Matriz 7 Exercicio
	public JTextField txtMat711, txtMat712, txtMat713;
	public JTextField txtMat721, txtMat722, txtMat723;
	public JTextField txtMat731, txtMat732, txtMat733;

	// Matriz 8 Exercicio
	public JTextField txtMat811, txtMat812, txtMat813;
	public JTextField txtMat821, txtMat822, txtMat823;
	public JTextField txtMat831, txtMat832, txtMat833;

	// Matriz 9 Exercicio
	public JTextField txtMat911, txtMat912, txtMat913, txtMat914;
	public JTextField txtMat921, txtMat922, txtMat923, txtMat924;
	public JTextField txtMat931, txtMat932, txtMat933, txtMat934;
	public JTextField txtMat941, txtMat942, txtMat943, txtMat944;
	public JTextField txtMat951, txtMat952, txtMat953, txtMat954;

	// Matriz 10 Exercicio
	public JTextField txtMat1011, txtMat1012, txtMat1013;
	public JTextField txtMat1021, txtMat1022, txtMat1023;
	public JTextField txtMat1031, txtMat1032, txtMat1033;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public SubtracaoMatrizQuestoesView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Quest�es de Subtra��o de Matrizes");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800,2550));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblSubtracaoQuestoesTexto = new JLabel(imagens.subtracaoTextoQuestoes);
		lblConferirQuestoes = new JLabel(imagens.conferirQuestoes);
		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblResultadoEx3 = new JLabel();
		lblResultadoEx4 = new JLabel();
		lblResultadoEx5 = new JLabel();
		lblResultadoEx6 = new JLabel();
		lblResultadoEx7 = new JLabel();
		lblResultadoEx8 = new JLabel();
		lblResultadoEx9 = new JLabel();
		lblResultadoEx10 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3);
		txtMat112 = new JTextField("", 3);
		txtMat113 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat123 = new JTextField("", 3);
		txtMat131 = new JTextField("", 3);
		txtMat132 = new JTextField("", 3);
		txtMat133 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3);
		txtMat212 = new JTextField("", 3);
		txtMat213 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);
		txtMat223 = new JTextField("", 3);
		txtMat231 = new JTextField("", 3);
		txtMat232 = new JTextField("", 3);
		txtMat233 = new JTextField("", 3);

		// Matriz 3 Exercicio Instanciando
		txtMat311 = new JTextField("", 3);
		txtMat312 = new JTextField("", 3);
		txtMat313 = new JTextField("", 3);
		txtMat321 = new JTextField("", 3);
		txtMat322 = new JTextField("", 3);
		txtMat323 = new JTextField("", 3);
		txtMat331 = new JTextField("", 3);
		txtMat332 = new JTextField("", 3);
		txtMat333 = new JTextField("", 3);
		txtMat341 = new JTextField("", 3);
		txtMat342 = new JTextField("", 3);
		txtMat343 = new JTextField("", 3);

		// Matriz 4 Exercicio Instanciando
		txtMat411 = new JTextField("", 3);
		txtMat412 = new JTextField("", 3);
		txtMat413 = new JTextField("", 3);
		txtMat414 = new JTextField("", 3);
		txtMat421 = new JTextField("", 3);
		txtMat422 = new JTextField("", 3);
		txtMat423 = new JTextField("", 3);
		txtMat424 = new JTextField("", 3);
		txtMat431 = new JTextField("", 3);
		txtMat432 = new JTextField("", 3);
		txtMat433 = new JTextField("", 3);
		txtMat434 = new JTextField("", 3);
		txtMat441 = new JTextField("", 3);
		txtMat442 = new JTextField("", 3);
		txtMat443 = new JTextField("", 3);
		txtMat444 = new JTextField("", 3);

		// Matriz 5 Exercicio Instanciando
		txtMat511 = new JTextField("", 3);
		txtMat512 = new JTextField("", 3);
		txtMat513 = new JTextField("", 3);
		txtMat514 = new JTextField("", 3);
		txtMat521 = new JTextField("", 3);
		txtMat522 = new JTextField("", 3);
		txtMat523 = new JTextField("", 3);
		txtMat524 = new JTextField("", 3);
		txtMat531 = new JTextField("", 3);
		txtMat532 = new JTextField("", 3);
		txtMat533 = new JTextField("", 3);
		txtMat534 = new JTextField("", 3);
		txtMat541 = new JTextField("", 3);
		txtMat542 = new JTextField("", 3);
		txtMat543 = new JTextField("", 3);
		txtMat544 = new JTextField("", 3);
		txtMat551 = new JTextField("", 3);
		txtMat552 = new JTextField("", 3);
		txtMat553 = new JTextField("", 3);
		txtMat554 = new JTextField("", 3);

		// Matriz 6 Exercicio Instanciando
		txtMat611 = new JTextField("", 3);
		txtMat612 = new JTextField("", 3);
		txtMat613 = new JTextField("", 3);
		txtMat614 = new JTextField("", 3);
		txtMat621 = new JTextField("", 3);
		txtMat622 = new JTextField("", 3);
		txtMat623 = new JTextField("", 3);
		txtMat624 = new JTextField("", 3);
		txtMat631 = new JTextField("", 3);
		txtMat632 = new JTextField("", 3);
		txtMat633 = new JTextField("", 3);
		txtMat634 = new JTextField("", 3);

		// Matriz 7 Exercicio Instanciando
		txtMat711 = new JTextField("", 3);
		txtMat712 = new JTextField("", 3);
		txtMat713 = new JTextField("", 3);
		txtMat721 = new JTextField("", 3);
		txtMat722 = new JTextField("", 3);
		txtMat723 = new JTextField("", 3);
		txtMat731 = new JTextField("", 3);
		txtMat732 = new JTextField("", 3);
		txtMat733 = new JTextField("", 3);

		// Matriz 8 Exercicio Instanciando
		txtMat811 = new JTextField("", 3);
		txtMat812 = new JTextField("", 3);
		txtMat813 = new JTextField("", 3);
		txtMat821 = new JTextField("", 3);
		txtMat822 = new JTextField("", 3);
		txtMat823 = new JTextField("", 3);
		txtMat831 = new JTextField("", 3);
		txtMat832 = new JTextField("", 3);
		txtMat833 = new JTextField("", 3);

		// Matriz 9 Exercicio Instanciando
		txtMat911 = new JTextField("", 3);
		txtMat912 = new JTextField("", 3);
		txtMat913 = new JTextField("", 3);
		txtMat914 = new JTextField("", 3);
		txtMat921 = new JTextField("", 3);
		txtMat922 = new JTextField("", 3);
		txtMat923 = new JTextField("", 3);
		txtMat924 = new JTextField("", 3);
		txtMat931 = new JTextField("", 3);
		txtMat932 = new JTextField("", 3);
		txtMat933 = new JTextField("", 3);
		txtMat934 = new JTextField("", 3);
		txtMat941 = new JTextField("", 3);
		txtMat942 = new JTextField("", 3);
		txtMat943 = new JTextField("", 3);
		txtMat944 = new JTextField("", 3);
		txtMat951 = new JTextField("", 3);
		txtMat952 = new JTextField("", 3);
		txtMat953 = new JTextField("", 3);
		txtMat954 = new JTextField("", 3);

		// Matriz 10 Exercicio Instanciando
		txtMat1011 = new JTextField("", 3);
		txtMat1012 = new JTextField("", 3);
		txtMat1013 = new JTextField("", 3);
		txtMat1021 = new JTextField("", 3);
		txtMat1022 = new JTextField("", 3);
		txtMat1023 = new JTextField("", 3);
		txtMat1031 = new JTextField("", 3);
		txtMat1032 = new JTextField("", 3);
		txtMat1033 = new JTextField("", 3);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblSubtracaoQuestoesTexto.setBounds(0, 50, 747, 2390);
		lblConferirQuestoes.setBounds(300, 2470, 237, 38);
		lblResultadoEx1.setBounds(600, 230, 75, 65);
		lblResultadoEx2.setBounds(600, 430, 75, 65);
		lblResultadoEx3.setBounds(650, 680, 75, 65);
		lblResultadoEx4.setBounds(680, 930, 75, 65);
		lblResultadoEx5.setBounds(690, 1220, 75, 65);
		lblResultadoEx6.setBounds(695, 1430, 75, 65);
		lblResultadoEx7.setBounds(635, 1635, 75, 65);
		lblResultadoEx8.setBounds(655, 1840, 75, 65);
		lblResultadoEx9.setBounds(680, 2140, 75, 65);
		lblResultadoEx10.setBounds(650, 2350, 75, 65);
		lblVolta.setBounds(30, 2510, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(470, 190, 30, 25);
		txtMat112.setBounds(510, 190, 30, 25);
		txtMat113.setBounds(550, 190, 30, 25);
		txtMat121.setBounds(470, 230, 30, 25);
		txtMat122.setBounds(510, 230, 30, 25);
		txtMat123.setBounds(550, 230, 30, 25);
		txtMat131.setBounds(470, 270, 30, 25);
		txtMat132.setBounds(510, 270, 30, 25);
		txtMat133.setBounds(550, 270, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(460, 390, 30, 25);
		txtMat212.setBounds(500, 390, 30, 25);
		txtMat213.setBounds(540, 390, 30, 25);
		txtMat221.setBounds(460, 430, 30, 25);
		txtMat222.setBounds(500, 430, 30, 25);
		txtMat223.setBounds(540, 430, 30, 25);
		txtMat231.setBounds(460, 470, 30, 25);
		txtMat232.setBounds(500, 470, 30, 25);
		txtMat233.setBounds(540, 470, 30, 25);

		// Posicionando Matriz 3
		txtMat311.setBounds(490, 600, 30, 25);
		txtMat312.setBounds(530, 600, 30, 25);
		txtMat313.setBounds(570, 600, 30, 25);
		txtMat321.setBounds(490, 640, 30, 25);
		txtMat322.setBounds(530, 640, 30, 25);
		txtMat323.setBounds(570, 640, 30, 25);
		txtMat331.setBounds(490, 680, 30, 25);
		txtMat332.setBounds(530, 680, 30, 25);
		txtMat333.setBounds(570, 680, 30, 25);
		txtMat341.setBounds(490, 720, 30, 25);
		txtMat342.setBounds(530, 720, 30, 25);
		txtMat343.setBounds(570, 720, 30, 25);

		// Posicionando Matriz 4
		txtMat411.setBounds(510, 850, 30, 25);
		txtMat412.setBounds(550, 850, 30, 25);
		txtMat413.setBounds(590, 850, 30, 25);
		txtMat414.setBounds(630, 850, 30, 25);
		txtMat421.setBounds(510, 890, 30, 25);
		txtMat422.setBounds(550, 890, 30, 25);
		txtMat423.setBounds(590, 890, 30, 25);
		txtMat424.setBounds(630, 890, 30, 25);
		txtMat431.setBounds(510, 930, 30, 25);
		txtMat432.setBounds(550, 930, 30, 25);
		txtMat433.setBounds(590, 930, 30, 25);
		txtMat434.setBounds(630, 930, 30, 25);
		txtMat441.setBounds(510, 970, 30, 25);
		txtMat442.setBounds(550, 970, 30, 25);
		txtMat443.setBounds(590, 970, 30, 25);
		txtMat444.setBounds(630, 970, 30, 25);

		// Posicionando Matriz 5
		txtMat511.setBounds(510, 1100, 30, 25);
		txtMat512.setBounds(550, 1100, 30, 25);
		txtMat513.setBounds(590, 1100, 30, 25);
		txtMat514.setBounds(630, 1100, 30, 25);
		txtMat521.setBounds(510, 1140, 30, 25);
		txtMat522.setBounds(550, 1140, 30, 25);
		txtMat523.setBounds(590, 1140, 30, 25);
		txtMat524.setBounds(630, 1140, 30, 25);
		txtMat531.setBounds(510, 1180, 30, 25);
		txtMat532.setBounds(550, 1180, 30, 25);
		txtMat533.setBounds(590, 1180, 30, 25);
		txtMat534.setBounds(630, 1180, 30, 25);
		txtMat541.setBounds(510, 1220, 30, 25);
		txtMat542.setBounds(550, 1220, 30, 25);
		txtMat543.setBounds(590, 1220, 30, 25);
		txtMat544.setBounds(630, 1220, 30, 25);
		txtMat551.setBounds(510, 1260, 30, 25);
		txtMat552.setBounds(550, 1260, 30, 25);
		txtMat553.setBounds(590, 1260, 30, 25);
		txtMat554.setBounds(630, 1260, 30, 25);

		// Posicionando Matriz 6
		txtMat611.setBounds(530, 1390, 30, 25);
		txtMat612.setBounds(570, 1390, 30, 25);
		txtMat613.setBounds(610, 1390, 30, 25);
		txtMat614.setBounds(650, 1390, 30, 25);
		txtMat621.setBounds(530, 1430, 30, 25);
		txtMat622.setBounds(570, 1430, 30, 25);
		txtMat623.setBounds(610, 1430, 30, 25);
		txtMat624.setBounds(650, 1430, 30, 25);
		txtMat631.setBounds(530, 1470, 30, 25);
		txtMat632.setBounds(570, 1470, 30, 25);
		txtMat633.setBounds(610, 1470, 30, 25);
		txtMat634.setBounds(650, 1470, 30, 25);

		// Posicionando Matriz 7
		txtMat711.setBounds(495, 1595, 30, 25);
		txtMat712.setBounds(535, 1595, 30, 25);
		txtMat713.setBounds(575, 1595, 30, 25);
		txtMat721.setBounds(495, 1635, 30, 25);
		txtMat722.setBounds(535, 1635, 30, 25);
		txtMat723.setBounds(575, 1635, 30, 25);
		txtMat731.setBounds(495, 1675, 30, 25);
		txtMat732.setBounds(535, 1675, 30, 25);
		txtMat733.setBounds(575, 1675, 30, 25);

		// Posicionando Matriz 8
		txtMat811.setBounds(500, 1800, 30, 25);
		txtMat812.setBounds(540, 1800, 30, 25);
		txtMat813.setBounds(580, 1800, 30, 25);
		txtMat821.setBounds(500, 1840, 30, 25);
		txtMat822.setBounds(540, 1840, 30, 25);
		txtMat823.setBounds(580, 1840, 30, 25);
		txtMat831.setBounds(500, 1880, 30, 25);
		txtMat832.setBounds(540, 1880, 30, 25);
		txtMat833.setBounds(580, 1880, 30, 25);

		// Posicionando Matriz 9
		txtMat911.setBounds(500, 2020, 30, 25);
		txtMat912.setBounds(540, 2020, 30, 25);
		txtMat913.setBounds(580, 2020, 30, 25);
		txtMat914.setBounds(620, 2020, 30, 25);
		txtMat921.setBounds(500, 2060, 30, 25);
		txtMat922.setBounds(540, 2060, 30, 25);
		txtMat923.setBounds(580, 2060, 30, 25);
		txtMat924.setBounds(620, 2060, 30, 25);
		txtMat931.setBounds(500, 2100, 30, 25);
		txtMat932.setBounds(540, 2100, 30, 25);
		txtMat933.setBounds(580, 2100, 30, 25);
		txtMat934.setBounds(620, 2100, 30, 25);
		txtMat941.setBounds(500, 2140, 30, 25);
		txtMat942.setBounds(540, 2140, 30, 25);
		txtMat943.setBounds(580, 2140, 30, 25);
		txtMat944.setBounds(620, 2140, 30, 25);
		txtMat951.setBounds(500, 2180, 30, 25);
		txtMat952.setBounds(540, 2180, 30, 25);
		txtMat953.setBounds(580, 2180, 30, 25);
		txtMat954.setBounds(620, 2180, 30, 25);

		// Posicionando Matriz 10
		txtMat1011.setBounds(515, 2310, 30, 25);
		txtMat1012.setBounds(555, 2310, 30, 25);
		txtMat1013.setBounds(595, 2310, 30, 25);
		txtMat1021.setBounds(515, 2350, 30, 25);
		txtMat1022.setBounds(555, 2350, 30, 25);
		txtMat1023.setBounds(595, 2350, 30, 25);
		txtMat1031.setBounds(515, 2390, 30, 25);
		txtMat1032.setBounds(555, 2390, 30, 25);
		txtMat1033.setBounds(595, 2390, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblSubtracaoQuestoesTexto);
		paneil.add(lblConferirQuestoes);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblResultadoEx3);
		paneil.add(lblResultadoEx4);
		paneil.add(lblResultadoEx5);
		paneil.add(lblResultadoEx6);
		paneil.add(lblResultadoEx7);
		paneil.add(lblResultadoEx8);
		paneil.add(lblResultadoEx9);
		paneil.add(lblResultadoEx10);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat113);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat123);
		paneil.add(txtMat131);
		paneil.add(txtMat132);
		paneil.add(txtMat133);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat213);
		paneil.add(txtMat221);
		paneil.add(txtMat222);
		paneil.add(txtMat223);
		paneil.add(txtMat231);
		paneil.add(txtMat232);
		paneil.add(txtMat233);

		// Adicionando a Matriz 3
		paneil.add(txtMat311);
		paneil.add(txtMat312);
		paneil.add(txtMat313);
		paneil.add(txtMat321);
		paneil.add(txtMat322);
		paneil.add(txtMat323);
		paneil.add(txtMat331);
		paneil.add(txtMat332);
		paneil.add(txtMat333);
		paneil.add(txtMat341);
		paneil.add(txtMat342);
		paneil.add(txtMat343);

		// Adicionando a Matriz 4
		paneil.add(txtMat411);
		paneil.add(txtMat412);
		paneil.add(txtMat413);
		paneil.add(txtMat414);
		paneil.add(txtMat421);
		paneil.add(txtMat422);
		paneil.add(txtMat423);
		paneil.add(txtMat424);
		paneil.add(txtMat431);
		paneil.add(txtMat432);
		paneil.add(txtMat433);
		paneil.add(txtMat434);
		paneil.add(txtMat441);
		paneil.add(txtMat442);
		paneil.add(txtMat443);
		paneil.add(txtMat444);

		// Adicionando a Matriz 5
		paneil.add(txtMat511);
		paneil.add(txtMat512);
		paneil.add(txtMat513);
		paneil.add(txtMat514);
		paneil.add(txtMat521);
		paneil.add(txtMat522);
		paneil.add(txtMat523);
		paneil.add(txtMat524);
		paneil.add(txtMat531);
		paneil.add(txtMat532);
		paneil.add(txtMat533);
		paneil.add(txtMat534);
		paneil.add(txtMat541);
		paneil.add(txtMat542);
		paneil.add(txtMat543);
		paneil.add(txtMat544);
		paneil.add(txtMat551);
		paneil.add(txtMat552);
		paneil.add(txtMat553);
		paneil.add(txtMat554);

		// Adicionando a Matriz 6
		paneil.add(txtMat611);
		paneil.add(txtMat612);
		paneil.add(txtMat613);
		paneil.add(txtMat614);
		paneil.add(txtMat621);
		paneil.add(txtMat622);
		paneil.add(txtMat623);
		paneil.add(txtMat624);
		paneil.add(txtMat631);
		paneil.add(txtMat632);
		paneil.add(txtMat633); 
		paneil.add(txtMat634);

		// Adicionando a Matriz 7
		paneil.add(txtMat711);
		paneil.add(txtMat712);
		paneil.add(txtMat713);
		paneil.add(txtMat721);
		paneil.add(txtMat722);
		paneil.add(txtMat723);
		paneil.add(txtMat731);
		paneil.add(txtMat732);
		paneil.add(txtMat733);

		// Adicionando a Matriz 8
		paneil.add(txtMat811);
		paneil.add(txtMat812);
		paneil.add(txtMat813);
		paneil.add(txtMat821);
		paneil.add(txtMat822);
		paneil.add(txtMat823);
		paneil.add(txtMat831);
		paneil.add(txtMat832);
		paneil.add(txtMat833);

		// Adicionando a Matriz 9
		paneil.add(txtMat911);
		paneil.add(txtMat912);
		paneil.add(txtMat913);
		paneil.add(txtMat914);
		paneil.add(txtMat921);
		paneil.add(txtMat922);
		paneil.add(txtMat923);
		paneil.add(txtMat924);
		paneil.add(txtMat931);
		paneil.add(txtMat932);
		paneil.add(txtMat933);
		paneil.add(txtMat934);
		paneil.add(txtMat941);
		paneil.add(txtMat942);
		paneil.add(txtMat943);
		paneil.add(txtMat944);
		paneil.add(txtMat951);
		paneil.add(txtMat952);
		paneil.add(txtMat953);
		paneil.add(txtMat954);

		// Adicionando a Matriz 10
		paneil.add(txtMat1011);
		paneil.add(txtMat1012);
		paneil.add(txtMat1013);
		paneil.add(txtMat1021);
		paneil.add(txtMat1022);
		paneil.add(txtMat1023);
		paneil.add(txtMat1031);
		paneil.add(txtMat1032); 
		paneil.add(txtMat1033);

		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final SubtracaoMatrizQuestoesController subtracaoQuestoesController = new SubtracaoMatrizQuestoesController(this); 
		subtracaoQuestoesController.addObserver(this);
		lblConferirQuestoes.addMouseListener(subtracaoQuestoesController);
		lblVolta.addMouseListener(subtracaoQuestoesController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}