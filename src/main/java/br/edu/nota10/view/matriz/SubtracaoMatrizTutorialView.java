/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.SubtracaoMatrizTutorialController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class SubtracaoMatrizTutorialView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblSubtracaoTutorialTexto;
	public JLabel lblConferirEx1;
	public JLabel lblConferirEx2;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat113, txtMat121, txtMat122, txtMat123;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212, txtMat213, txtMat221, txtMat222, txtMat223;
	public JTextField txtMat231, txtMat232, txtMat233, txtMat241, txtMat242, txtMat243;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public SubtracaoMatrizTutorialView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Tutorial Subtra��o de Matrizes");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800, 1650));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblSubtracaoTutorialTexto = new JLabel(imagens.subtracaoTextoTutorial);
		lblConferirEx1 = new JLabel(imagens.conferir);
		lblConferirEx2 = new JLabel(imagens.conferir);
		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3);
		txtMat112 = new JTextField("", 3);
		txtMat113 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat123 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3);
		txtMat212 = new JTextField("", 3);
		txtMat213 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);
		txtMat223 = new JTextField("", 3);
		txtMat231 = new JTextField("", 3);
		txtMat232 = new JTextField("", 3);
		txtMat233 = new JTextField("", 3);
		txtMat241 = new JTextField("", 3);
		txtMat242 = new JTextField("", 3);
		txtMat243 = new JTextField("", 3);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblSubtracaoTutorialTexto.setBounds(20, 50, 722, 1547);
		lblConferirEx1.setBounds(500, 1325, 151, 38);
		lblConferirEx2.setBounds(500, 1570, 151, 38);
		lblResultadoEx1.setBounds(660, 1260, 75, 65);
		lblResultadoEx2.setBounds(650, 1470, 75, 65);
		lblVolta.setBounds(30, 1600, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(490, 1235, 30, 25);
		txtMat112.setBounds(550, 1235, 30, 25);
		txtMat113.setBounds(610, 1235, 30, 25);
		txtMat121.setBounds(490, 1275, 30, 25);
		txtMat122.setBounds(550, 1275, 30, 25);
		txtMat123.setBounds(610, 1275, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(493, 1400, 30, 25);
		txtMat212.setBounds(543, 1400, 30, 25);
		txtMat213.setBounds(593, 1400, 30, 25);
		txtMat221.setBounds(493, 1440, 30, 25);
		txtMat222.setBounds(543, 1440, 30, 25);
		txtMat223.setBounds(593, 1440, 30, 25);
		txtMat231.setBounds(493, 1480, 30, 25);
		txtMat232.setBounds(543, 1480, 30, 25);
		txtMat233.setBounds(593, 1480, 30, 25);
		txtMat241.setBounds(493, 1520, 30, 25);
		txtMat242.setBounds(543, 1520, 30, 25);
		txtMat243.setBounds(593, 1520, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblSubtracaoTutorialTexto);
		paneil.add(lblConferirEx1);
		paneil.add(lblConferirEx2);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat113);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat123);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat213);
		paneil.add(txtMat221);
		paneil.add(txtMat222);
		paneil.add(txtMat223);
		paneil.add(txtMat231);
		paneil.add(txtMat232);
		paneil.add(txtMat233);
		paneil.add(txtMat241);
		paneil.add(txtMat242);
		paneil.add(txtMat243);

		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final SubtracaoMatrizTutorialController subtracaoTutorialController = new SubtracaoMatrizTutorialController(this);
		subtracaoTutorialController.addObserver(this);
		lblConferirEx1.addMouseListener(subtracaoTutorialController);
		lblConferirEx2.addMouseListener(subtracaoTutorialController);
		lblVolta.addMouseListener(subtracaoTutorialController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}