/**
 * 
 */
package br.edu.nota10.view.matriz;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.matriz.MultiplicacaoPorEscalarMatrizQuestoesController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class MultiplicacaoPorEscalarMatrizQuestoesView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblMultPorEscalarQuestoesTexto;
	public JLabel lblConferirQuestoes;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblResultadoEx3;
	public JLabel lblResultadoEx4;
	public JLabel lblResultadoEx5;
	public JLabel lblResultadoEx6;
	public JLabel lblResultadoEx7;
	public JLabel lblResultadoEx8;
	public JLabel lblResultadoEx9;
	public JLabel lblResultadoEx10;
	public JLabel lblVolta;

	// Matriz 1 Exercicio
	public JTextField txtMat111, txtMat112, txtMat113;
	public JTextField txtMat121, txtMat122, txtMat123;
	public JTextField txtMat131, txtMat132, txtMat133;

	// Matriz 2 Exercicio
	public JTextField txtMat211, txtMat212, txtMat213;
	public JTextField txtMat221, txtMat222, txtMat223;
	public JTextField txtMat231, txtMat232, txtMat233;

	// Matriz 3 Exercicio
	public JTextField txtMat311, txtMat312, txtMat313;
	public JTextField txtMat321, txtMat322, txtMat323;
	public JTextField txtMat331, txtMat332, txtMat333;
	public JTextField txtMat341, txtMat342, txtMat343;

	// Matriz 4 Exercicio
	public JTextField txtMat411, txtMat412, txtMat413, txtMat414, txtMat415;
	public JTextField txtMat421, txtMat422, txtMat423, txtMat424, txtMat425;
	public JTextField txtMat431, txtMat432, txtMat433, txtMat434, txtMat435;
	public JTextField txtMat441, txtMat442, txtMat443, txtMat444, txtMat445;
	public JTextField txtMat451, txtMat452, txtMat453, txtMat454, txtMat455;

	// Matriz 5 Exercicio
	public JTextField txtMat511, txtMat512, txtMat513;
	public JTextField txtMat521, txtMat522, txtMat523;
	public JTextField txtMat531, txtMat532, txtMat533;
	public JTextField txtMat541, txtMat542, txtMat543;
	public JTextField txtMat551, txtMat552, txtMat553;

	// Matriz 6 Exercicio
	public JTextField txtMat611, txtMat612, txtMat613, txtMat614;
	public JTextField txtMat621, txtMat622, txtMat623, txtMat624;
	public JTextField txtMat631, txtMat632, txtMat633, txtMat634;

	// Matriz 7 Exercicio
	public JTextField txtMat711, txtMat712, txtMat713, txtMat714;
	public JTextField txtMat721, txtMat722, txtMat723, txtMat724;
	public JTextField txtMat731, txtMat732, txtMat733, txtMat734;

	// Matriz 8 Exercicio
	public JTextField txtMat811, txtMat812, txtMat813, txtMat814, txtMat815;
	public JTextField txtMat821, txtMat822, txtMat823, txtMat824, txtMat825;
	public JTextField txtMat831, txtMat832, txtMat833, txtMat834, txtMat835;
	public JTextField txtMat841, txtMat842, txtMat843, txtMat844, txtMat845;
	public JTextField txtMat851, txtMat852, txtMat853, txtMat854, txtMat855;
	// Matriz 9 Exercicio
	public JTextField txtMat911, txtMat912, txtMat913, txtMat914;
	public JTextField txtMat921, txtMat922, txtMat923, txtMat924;
	public JTextField txtMat931, txtMat932, txtMat933, txtMat934;

	// Matriz 10 Exercicio
	public JTextField txtMat1011, txtMat1012, txtMat1013;
	public JTextField txtMat1021, txtMat1022, txtMat1023;
	public JTextField txtMat1031, txtMat1032, txtMat1033;

	public JPanel paneil;
	public JScrollPane scrollPane;

	public MultiplicacaoPorEscalarMatrizQuestoesView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Quest�es de Multiplica��o por Escalar");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		paneil = new JPanel();
		paneil.setLayout(null);
		paneil.setPreferredSize(new Dimension(800,2400));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblMultPorEscalarQuestoesTexto = new JLabel(imagens.multPorEscalarTextoQuestoes);
		lblConferirQuestoes = new JLabel(imagens.conferirQuestoes);
		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblResultadoEx3 = new JLabel();
		lblResultadoEx4 = new JLabel();
		lblResultadoEx5 = new JLabel();
		lblResultadoEx6 = new JLabel();
		lblResultadoEx7 = new JLabel();
		lblResultadoEx8 = new JLabel();
		lblResultadoEx9 = new JLabel();
		lblResultadoEx10 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);

		// Matriz 1 Exercicio Instanciando
		txtMat111 = new JTextField("", 3);
		txtMat112 = new JTextField("", 3);
		txtMat113 = new JTextField("", 3);
		txtMat121 = new JTextField("", 3);
		txtMat122 = new JTextField("", 3);
		txtMat123 = new JTextField("", 3);
		txtMat131 = new JTextField("", 3);
		txtMat132 = new JTextField("", 3);
		txtMat133 = new JTextField("", 3);

		// Matriz 2 Exercicio Instanciando
		txtMat211 = new JTextField("", 3);
		txtMat212 = new JTextField("", 3);
		txtMat213 = new JTextField("", 3);
		txtMat221 = new JTextField("", 3);
		txtMat222 = new JTextField("", 3);
		txtMat223 = new JTextField("", 3);
		txtMat231 = new JTextField("", 3);
		txtMat232 = new JTextField("", 3);
		txtMat233 = new JTextField("", 3);

		// Matriz 3 Exercicio Instanciando
		txtMat311 = new JTextField("", 3);
		txtMat312 = new JTextField("", 3);
		txtMat313 = new JTextField("", 3);
		txtMat321 = new JTextField("", 3);
		txtMat322 = new JTextField("", 3);
		txtMat323 = new JTextField("", 3);
		txtMat331 = new JTextField("", 3);
		txtMat332 = new JTextField("", 3);
		txtMat333 = new JTextField("", 3);
		txtMat341 = new JTextField("", 3);
		txtMat342 = new JTextField("", 3);
		txtMat343 = new JTextField("", 3);

		// Matriz 4 Exercicio Instanciando
		txtMat411 = new JTextField("", 3);
		txtMat412 = new JTextField("", 3);
		txtMat413 = new JTextField("", 3);
		txtMat414 = new JTextField("", 3);
		txtMat415 = new JTextField("", 3);
		txtMat421 = new JTextField("", 3);
		txtMat422 = new JTextField("", 3);
		txtMat423 = new JTextField("", 3);
		txtMat424 = new JTextField("", 3);
		txtMat425 = new JTextField("", 3);
		txtMat431 = new JTextField("", 3);
		txtMat432 = new JTextField("", 3);
		txtMat433 = new JTextField("", 3);
		txtMat434 = new JTextField("", 3);
		txtMat435 = new JTextField("", 3);
		txtMat441 = new JTextField("", 3);
		txtMat442 = new JTextField("", 3);
		txtMat443 = new JTextField("", 3);
		txtMat444 = new JTextField("", 3);
		txtMat445 = new JTextField("", 3);
		txtMat441 = new JTextField("", 3);
		txtMat442 = new JTextField("", 3);
		txtMat443 = new JTextField("", 3);
		txtMat444 = new JTextField("", 3);
		txtMat445 = new JTextField("", 3);
		txtMat451 = new JTextField("", 3);
		txtMat452 = new JTextField("", 3);
		txtMat453 = new JTextField("", 3);
		txtMat454 = new JTextField("", 3);
		txtMat455 = new JTextField("", 3);

		// Matriz 5 Exercicio Instanciando
		txtMat511 = new JTextField("", 3);
		txtMat512 = new JTextField("", 3);
		txtMat513 = new JTextField("", 3);
		txtMat521 = new JTextField("", 3);
		txtMat522 = new JTextField("", 3);
		txtMat523 = new JTextField("", 3);
		txtMat531 = new JTextField("", 3);
		txtMat532 = new JTextField("", 3);
		txtMat533 = new JTextField("", 3);
		txtMat541 = new JTextField("", 3);
		txtMat542 = new JTextField("", 3);
		txtMat543 = new JTextField("", 3);
		txtMat551 = new JTextField("", 3);
		txtMat552 = new JTextField("", 3);
		txtMat553 = new JTextField("", 3);

		// Matriz 6 Exercicio Instanciando
		txtMat611 = new JTextField("", 3);
		txtMat612 = new JTextField("", 3);
		txtMat613 = new JTextField("", 3);
		txtMat614 = new JTextField("", 3);
		txtMat621 = new JTextField("", 3);
		txtMat622 = new JTextField("", 3);
		txtMat623 = new JTextField("", 3);
		txtMat624 = new JTextField("", 3);
		txtMat631 = new JTextField("", 3);
		txtMat632 = new JTextField("", 3);
		txtMat633 = new JTextField("", 3);
		txtMat634 = new JTextField("", 3);

		// Matriz 7 Exercicio Instanciando
		txtMat711 = new JTextField("", 3);
		txtMat712 = new JTextField("", 3);
		txtMat713 = new JTextField("", 3);
		txtMat714 = new JTextField("", 3);
		txtMat721 = new JTextField("", 3);
		txtMat722 = new JTextField("", 3);
		txtMat723 = new JTextField("", 3);
		txtMat724 = new JTextField("", 3);
		txtMat731 = new JTextField("", 3);
		txtMat732 = new JTextField("", 3);
		txtMat733 = new JTextField("", 3);
		txtMat734 = new JTextField("", 3);

		// Matriz 8 Exercicio Instanciando
		txtMat811 = new JTextField("", 3);
		txtMat812 = new JTextField("", 3);
		txtMat813 = new JTextField("", 3);
		txtMat814 = new JTextField("", 3);
		txtMat815 = new JTextField("", 3);
		txtMat821 = new JTextField("", 3);
		txtMat822 = new JTextField("", 3);
		txtMat823 = new JTextField("", 3);
		txtMat824 = new JTextField("", 3);
		txtMat825 = new JTextField("", 3);
		txtMat831 = new JTextField("", 3);
		txtMat832 = new JTextField("", 3);
		txtMat833 = new JTextField("", 3);
		txtMat834 = new JTextField("", 3);
		txtMat835 = new JTextField("", 3);
		txtMat841 = new JTextField("", 3);
		txtMat842 = new JTextField("", 3);
		txtMat843 = new JTextField("", 3);
		txtMat844 = new JTextField("", 3);
		txtMat845 = new JTextField("", 3);
		txtMat841 = new JTextField("", 3);
		txtMat842 = new JTextField("", 3);
		txtMat843 = new JTextField("", 3);
		txtMat844 = new JTextField("", 3);
		txtMat845 = new JTextField("", 3);
		txtMat851 = new JTextField("", 3);
		txtMat852 = new JTextField("", 3);
		txtMat853 = new JTextField("", 3);
		txtMat854 = new JTextField("", 3);
		txtMat855 = new JTextField("", 3);

		// Matriz 9 Exercicio Instanciando
		txtMat911 = new JTextField("", 3);
		txtMat912 = new JTextField("", 3);
		txtMat913 = new JTextField("", 3);
		txtMat914 = new JTextField("", 3);
		txtMat921 = new JTextField("", 3);
		txtMat922 = new JTextField("", 3);
		txtMat923 = new JTextField("", 3);
		txtMat924 = new JTextField("", 3);
		txtMat931 = new JTextField("", 3);
		txtMat932 = new JTextField("", 3);
		txtMat933 = new JTextField("", 3);
		txtMat934 = new JTextField("", 3);

		// Matriz 10 Exercicio Instanciando
		txtMat1011 = new JTextField("", 3);
		txtMat1012 = new JTextField("", 3);
		txtMat1013 = new JTextField("", 3);
		txtMat1021 = new JTextField("", 3);
		txtMat1022 = new JTextField("", 3);
		txtMat1023 = new JTextField("", 3);
		txtMat1031 = new JTextField("", 3);
		txtMat1032 = new JTextField("", 3);
		txtMat1033 = new JTextField("", 3);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblMultPorEscalarQuestoesTexto.setBounds(0, 50, 741, 2272);
		lblConferirQuestoes.setBounds(300, 2330, 237, 38);
		lblResultadoEx1.setBounds(540, 200, 75, 65);
		lblResultadoEx2.setBounds(540, 390, 75, 65);
		lblResultadoEx3.setBounds(530, 610, 75, 65);
		lblResultadoEx4.setBounds(630, 880, 75, 65);
		lblResultadoEx5.setBounds(535, 1160, 75, 65);
		lblResultadoEx6.setBounds(570, 1345, 75, 65);
		lblResultadoEx7.setBounds(580, 1560, 75, 65);
		lblResultadoEx8.setBounds(655, 1845, 75, 65);
		lblResultadoEx9.setBounds(600, 2025, 75, 65);
		lblResultadoEx10.setBounds(540, 2205, 75, 65);
		lblVolta.setBounds(30, 2360, 98, 45);

		// Posicionando Matriz 1
		txtMat111.setBounds(385, 160, 30, 25);
		txtMat112.setBounds(425, 160, 30, 25);
		txtMat113.setBounds(465, 160, 30, 25);
		txtMat121.setBounds(385, 200, 30, 25);
		txtMat122.setBounds(425, 200, 30, 25);
		txtMat123.setBounds(465, 200, 30, 25);
		txtMat131.setBounds(385, 240, 30, 25);
		txtMat132.setBounds(425, 240, 30, 25);
		txtMat133.setBounds(465, 240, 30, 25);

		// Posicionando Matriz 2
		txtMat211.setBounds(380, 350, 30, 25);
		txtMat212.setBounds(420, 350, 30, 25);
		txtMat213.setBounds(460, 350, 30, 25);
		txtMat221.setBounds(380, 390, 30, 25);
		txtMat222.setBounds(420, 390, 30, 25);
		txtMat223.setBounds(460, 390, 30, 25);
		txtMat231.setBounds(380, 430, 30, 25);
		txtMat232.setBounds(420, 430, 30, 25);
		txtMat233.setBounds(460, 430, 30, 25);

		// Posicionando Matriz 3
		txtMat311.setBounds(375, 530, 30, 25);
		txtMat312.setBounds(415, 530, 30, 25);
		txtMat313.setBounds(455, 530, 30, 25);
		txtMat321.setBounds(375, 570, 30, 25);
		txtMat322.setBounds(415, 570, 30, 25);
		txtMat323.setBounds(455, 570, 30, 25);
		txtMat331.setBounds(375, 610, 30, 25);
		txtMat332.setBounds(415, 610, 30, 25);
		txtMat333.setBounds(455, 610, 30, 25);
		txtMat341.setBounds(375, 650, 30, 25);
		txtMat342.setBounds(415, 650, 30, 25);
		txtMat343.setBounds(455, 650, 30, 25);

		// Posicionando Matriz 4
		txtMat411.setBounds(410, 760, 30, 25);
		txtMat412.setBounds(450, 760, 30, 25);
		txtMat413.setBounds(490, 760, 30, 25);
		txtMat414.setBounds(530, 760, 30, 25);
		txtMat415.setBounds(570, 760, 30, 25);
		txtMat421.setBounds(410, 800, 30, 25);
		txtMat422.setBounds(450, 800, 30, 25);
		txtMat423.setBounds(490, 800, 30, 25);
		txtMat424.setBounds(530, 800, 30, 25);
		txtMat425.setBounds(570, 800, 30, 25);
		txtMat431.setBounds(410, 840, 30, 25);
		txtMat432.setBounds(450, 840, 30, 25);
		txtMat433.setBounds(490, 840, 30, 25);
		txtMat434.setBounds(530, 840, 30, 25);
		txtMat435.setBounds(570, 840, 30, 25);
		txtMat441.setBounds(410, 880, 30, 25);
		txtMat442.setBounds(450, 880, 30, 25);
		txtMat443.setBounds(490, 880, 30, 25);
		txtMat444.setBounds(530, 880, 30, 25);
		txtMat445.setBounds(570, 880, 30, 25);
		txtMat451.setBounds(410, 920, 30, 25);
		txtMat452.setBounds(450, 920, 30, 25);
		txtMat453.setBounds(490, 920, 30, 25);
		txtMat454.setBounds(530, 920, 30, 25);
		txtMat455.setBounds(570, 920, 30, 25);

		// Posicionando Matriz 5
		txtMat511.setBounds(395, 1040, 30, 25);
		txtMat512.setBounds(435, 1040, 30, 25);
		txtMat513.setBounds(475, 1040, 30, 25);
		txtMat521.setBounds(395, 1080, 30, 25);
		txtMat522.setBounds(435, 1080, 30, 25);
		txtMat523.setBounds(475, 1080, 30, 25);
		txtMat531.setBounds(395, 1120, 30, 25);
		txtMat532.setBounds(435, 1120, 30, 25);
		txtMat533.setBounds(475, 1120, 30, 25);
		txtMat541.setBounds(395, 1160, 30, 25);
		txtMat542.setBounds(435, 1160, 30, 25);
		txtMat543.setBounds(475, 1160, 30, 25);
		txtMat551.setBounds(395, 1200, 30, 25);
		txtMat552.setBounds(435, 1200, 30, 25);
		txtMat553.setBounds(475, 1200, 30, 25);

		// Posicionando Matriz 6
		txtMat611.setBounds(405, 1305, 30, 25);
		txtMat612.setBounds(445, 1305, 30, 25);
		txtMat613.setBounds(485, 1305, 30, 25);
		txtMat614.setBounds(525, 1305, 30, 25);
		txtMat621.setBounds(405, 1345, 30, 25);
		txtMat622.setBounds(445, 1345, 30, 25);
		txtMat623.setBounds(485, 1345, 30, 25);
		txtMat624.setBounds(525, 1345, 30, 25);
		txtMat631.setBounds(405, 1385, 30, 25);
		txtMat632.setBounds(445, 1385, 30, 25);
		txtMat633.setBounds(485, 1385, 30, 25);
		txtMat634.setBounds(525, 1385, 30, 25);

		// Posicionando Matriz 7
		txtMat711.setBounds(395, 1520, 30, 25);
		txtMat712.setBounds(435, 1520, 30, 25);
		txtMat713.setBounds(475, 1520, 30, 25);
		txtMat714.setBounds(515, 1520, 30, 25);
		txtMat721.setBounds(395, 1560, 30, 25);
		txtMat722.setBounds(435, 1560, 30, 25);
		txtMat723.setBounds(475, 1560, 30, 25);
		txtMat724.setBounds(515, 1560, 30, 25);
		txtMat731.setBounds(395, 1600, 30, 25);
		txtMat732.setBounds(435, 1600, 30, 25);
		txtMat733.setBounds(475, 1600, 30, 25);
		txtMat734.setBounds(515, 1600, 30, 25);

		// Posicionando Matriz 8
		txtMat811.setBounds(415, 1725, 30, 25);
		txtMat812.setBounds(455, 1725, 30, 25);
		txtMat813.setBounds(495, 1725, 30, 25);
		txtMat814.setBounds(535, 1725, 30, 25);
		txtMat815.setBounds(575, 1725, 30, 25);
		txtMat821.setBounds(415, 1765, 30, 25);
		txtMat822.setBounds(455, 1765, 30, 25);
		txtMat823.setBounds(495, 1765, 30, 25);
		txtMat824.setBounds(535, 1765, 30, 25);
		txtMat825.setBounds(575, 1765, 30, 25);
		txtMat831.setBounds(415, 1805, 30, 25);
		txtMat832.setBounds(455, 1805, 30, 25);
		txtMat833.setBounds(495, 1805, 30, 25);
		txtMat834.setBounds(535, 1805, 30, 25);
		txtMat835.setBounds(575, 1805, 30, 25);
		txtMat841.setBounds(415, 1845, 30, 25);
		txtMat842.setBounds(455, 1845, 30, 25);
		txtMat843.setBounds(495, 1845, 30, 25);
		txtMat844.setBounds(535, 1845, 30, 25);
		txtMat845.setBounds(575, 1845, 30, 25);
		txtMat851.setBounds(415, 1885, 30, 25);
		txtMat852.setBounds(455, 1885, 30, 25);
		txtMat853.setBounds(495, 1885, 30, 25);
		txtMat854.setBounds(535, 1885, 30, 25);
		txtMat855.setBounds(575, 1885, 30, 25);

		// Posicionando Matriz 9
		txtMat911.setBounds(405, 1985, 30, 25);
		txtMat912.setBounds(445, 1985, 30, 25);
		txtMat913.setBounds(485, 1985, 30, 25);
		txtMat914.setBounds(525, 1985, 30, 25);
		txtMat921.setBounds(405, 2025, 30, 25);
		txtMat922.setBounds(445, 2025, 30, 25);
		txtMat923.setBounds(485, 2025, 30, 25);
		txtMat924.setBounds(525, 2025, 30, 25);
		txtMat931.setBounds(405, 2065, 30, 25);
		txtMat932.setBounds(445, 2065, 30, 25);
		txtMat933.setBounds(485, 2065, 30, 25);
		txtMat934.setBounds(525, 2065, 30, 25);

		// Posicionando Matriz 10
		txtMat1011.setBounds(370, 2165, 30, 25);
		txtMat1012.setBounds(410, 2165, 30, 25);
		txtMat1013.setBounds(450, 2165, 30, 25);
		txtMat1021.setBounds(370, 2205, 30, 25);
		txtMat1022.setBounds(410, 2205, 30, 25);
		txtMat1023.setBounds(450, 2205, 30, 25);
		txtMat1031.setBounds(370, 2245, 30, 25);
		txtMat1032.setBounds(410, 2245, 30, 25);
		txtMat1033.setBounds(450, 2245, 30, 25);

		// Adicionando item ao Painel
		paneil.add(lblMultPorEscalarQuestoesTexto);
		paneil.add(lblConferirQuestoes);
		paneil.add(lblResultadoEx1);
		paneil.add(lblResultadoEx2);
		paneil.add(lblResultadoEx3);
		paneil.add(lblResultadoEx4);
		paneil.add(lblResultadoEx5);
		paneil.add(lblResultadoEx6);
		paneil.add(lblResultadoEx7);
		paneil.add(lblResultadoEx8);
		paneil.add(lblResultadoEx9);
		paneil.add(lblResultadoEx10);
		paneil.add(lblVolta);

		// Adicionando a Matriz 1
		paneil.add(txtMat111);
		paneil.add(txtMat112);
		paneil.add(txtMat113);
		paneil.add(txtMat121);
		paneil.add(txtMat122);
		paneil.add(txtMat123);
		paneil.add(txtMat131);
		paneil.add(txtMat132);
		paneil.add(txtMat133);

		// Adicionando a Matriz 2
		paneil.add(txtMat211);
		paneil.add(txtMat212);
		paneil.add(txtMat213);
		paneil.add(txtMat221);
		paneil.add(txtMat222);
		paneil.add(txtMat223);
		paneil.add(txtMat231);
		paneil.add(txtMat232);
		paneil.add(txtMat233);

		// Adicionando a Matriz 3
		paneil.add(txtMat311);
		paneil.add(txtMat312);
		paneil.add(txtMat313);
		paneil.add(txtMat321);
		paneil.add(txtMat322);
		paneil.add(txtMat323);
		paneil.add(txtMat331);
		paneil.add(txtMat332);
		paneil.add(txtMat333);
		paneil.add(txtMat341);
		paneil.add(txtMat342);
		paneil.add(txtMat343);

		// Adicionando a Matriz 4
		paneil.add(txtMat411);
		paneil.add(txtMat412);
		paneil.add(txtMat413);
		paneil.add(txtMat414);
		paneil.add(txtMat415);
		paneil.add(txtMat421);
		paneil.add(txtMat422);
		paneil.add(txtMat423);
		paneil.add(txtMat424);
		paneil.add(txtMat425);
		paneil.add(txtMat431);
		paneil.add(txtMat432);
		paneil.add(txtMat433);
		paneil.add(txtMat434);
		paneil.add(txtMat435);
		paneil.add(txtMat441);
		paneil.add(txtMat442);
		paneil.add(txtMat443);
		paneil.add(txtMat444);
		paneil.add(txtMat445);
		paneil.add(txtMat451);
		paneil.add(txtMat452);
		paneil.add(txtMat453);
		paneil.add(txtMat454);
		paneil.add(txtMat455);

		// Adicionando a Matriz 5
		paneil.add(txtMat511);
		paneil.add(txtMat512);
		paneil.add(txtMat513);
		paneil.add(txtMat521);
		paneil.add(txtMat522);
		paneil.add(txtMat523);
		paneil.add(txtMat531);
		paneil.add(txtMat532);
		paneil.add(txtMat533);
		paneil.add(txtMat541);
		paneil.add(txtMat542);
		paneil.add(txtMat543);
		paneil.add(txtMat551);
		paneil.add(txtMat552);
		paneil.add(txtMat553);

		// Adicionando a Matriz 6
		paneil.add(txtMat611);
		paneil.add(txtMat612);
		paneil.add(txtMat613);
		paneil.add(txtMat614);
		paneil.add(txtMat621);
		paneil.add(txtMat622);
		paneil.add(txtMat623);
		paneil.add(txtMat624);
		paneil.add(txtMat631);
		paneil.add(txtMat632);
		paneil.add(txtMat633);
		paneil.add(txtMat634);

		// Adicionando a Matriz 7
		paneil.add(txtMat711);
		paneil.add(txtMat712);
		paneil.add(txtMat713);
		paneil.add(txtMat714);
		paneil.add(txtMat721);
		paneil.add(txtMat722);
		paneil.add(txtMat723);
		paneil.add(txtMat724);
		paneil.add(txtMat731);
		paneil.add(txtMat732);
		paneil.add(txtMat733);
		paneil.add(txtMat734);

		// Adicionando a Matriz 8
		paneil.add(txtMat811);
		paneil.add(txtMat812);
		paneil.add(txtMat813);
		paneil.add(txtMat814);
		paneil.add(txtMat815);
		paneil.add(txtMat821);
		paneil.add(txtMat822);
		paneil.add(txtMat823);
		paneil.add(txtMat824);
		paneil.add(txtMat825);
		paneil.add(txtMat831);
		paneil.add(txtMat832);
		paneil.add(txtMat833);
		paneil.add(txtMat834);
		paneil.add(txtMat835);
		paneil.add(txtMat841);
		paneil.add(txtMat842);
		paneil.add(txtMat843);
		paneil.add(txtMat844);
		paneil.add(txtMat845);
		paneil.add(txtMat851);
		paneil.add(txtMat852);
		paneil.add(txtMat853);
		paneil.add(txtMat854);
		paneil.add(txtMat855);

		// Adicionando a Matriz 9
		paneil.add(txtMat911);
		paneil.add(txtMat912);
		paneil.add(txtMat913);
		paneil.add(txtMat914);
		paneil.add(txtMat921);
		paneil.add(txtMat922);
		paneil.add(txtMat923);
		paneil.add(txtMat924);
		paneil.add(txtMat931);
		paneil.add(txtMat932);
		paneil.add(txtMat933);
		paneil.add(txtMat934);

		// Adicionando a Matriz 10
		paneil.add(txtMat1011);
		paneil.add(txtMat1012);
		paneil.add(txtMat1013);
		paneil.add(txtMat1021);
		paneil.add(txtMat1022);
		paneil.add(txtMat1023);
		paneil.add(txtMat1031);
		paneil.add(txtMat1032);
		paneil.add(txtMat1033);

		paneil.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(paneil, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final MultiplicacaoPorEscalarMatrizQuestoesController multQuestoesController = new MultiplicacaoPorEscalarMatrizQuestoesController(this); 
		multQuestoesController.addObserver(this);
		lblConferirQuestoes.addMouseListener(multQuestoesController);
		lblVolta.addMouseListener(multQuestoesController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}