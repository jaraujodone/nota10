/**
 * 
 */
package br.edu.nota10.view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

import br.edu.nota10.controller.OperacaoMatrizController;
import br.edu.nota10.model.Imagem;


/**
 * @author Jesse A. Done
 */
public class OperacaoMatrizView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblTutorial;
	public JLabel lblTesteSeusConhec;

	// Tutorial
	public JLabel lblEntendendoMatrizTutorial;
	public JLabel lblSomaTutorial;
	public JLabel lblSubtracaoTutorial;
	public JLabel lblMultEscalarTutorial;
	public JLabel lblMultMatrizTutorial;

	// Teste Seus Conhecimentos
	public JLabel lblSomaTeste;
	public JLabel lblSubtracaoTeste;
	public JLabel lblMultEscalarTeste;
	public JLabel lblMultMatrizTeste;
	public JLabel lblTesteGeralTeste;

	// Voltar
	public JLabel lblVoltar;	

	public OperacaoMatrizView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Operacoes Matricial");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		imgFundo = new JLabel(imagens.fundo);

		lblEntendendoMatrizTutorial = new JLabel(imagens.entendendoMatriz);
		lblTutorial = new JLabel(imagens.tutorial);
		lblTesteSeusConhec = new JLabel(imagens.testeSeuConhecmento);

		lblSomaTutorial = new JLabel(imagens.soma);
		lblSubtracaoTutorial = new JLabel(imagens.subtracao);
		lblMultEscalarTutorial = new JLabel(imagens.multEscalar);
		lblMultMatrizTutorial = new JLabel(imagens.multMatriz);

		lblSomaTeste = new JLabel(imagens.soma);
		lblSubtracaoTeste = new JLabel(imagens.subtracao);
		lblMultEscalarTeste = new JLabel(imagens.multEscalar);
		lblMultMatrizTeste = new JLabel(imagens.multMatriz);
		lblTesteGeralTeste = new JLabel(imagens.testeGeral);

		lblVoltar = new JLabel(imagens.voltar);

		imgFundo.setBounds(0, 0, 800, 600);

		lblTutorial.setBounds(20, 50, 73, 25);
		lblTesteSeusConhec.setBounds(20, 350, 237, 25);


		lblEntendendoMatrizTutorial.setBounds(40, 90, 271, 22);
		lblSomaTutorial.setBounds(40, 110, 64, 22);
		lblSubtracaoTutorial.setBounds(40, 130, 108, 22);
		lblMultEscalarTutorial.setBounds(40, 150, 230, 22);
		lblMultMatrizTutorial.setBounds(40, 170, 247, 22);

		lblSomaTeste.setBounds(40, 390, 64, 22);
		lblSubtracaoTeste.setBounds(40, 410, 108, 22);
		lblMultEscalarTeste.setBounds(40, 430, 230, 22);
		lblMultMatrizTeste.setBounds(40, 450, 247, 22);
		lblTesteGeralTeste.setBounds(40, 470, 108, 22);

		lblVoltar.setBounds(20, 515, 98, 45);

		add(lblTutorial);
		add(lblTesteSeusConhec);

		add(lblEntendendoMatrizTutorial);
		add(lblSomaTutorial);
		add(lblSubtracaoTutorial);
		add(lblMultEscalarTutorial);
		add(lblMultMatrizTutorial);

		add(lblSomaTeste);
		add(lblSubtracaoTeste);
		add(lblMultEscalarTeste);
		add(lblMultMatrizTeste);
		add(lblTesteGeralTeste);

		add(lblVoltar);

		add(imgFundo);
		add(new JLabel());

		final OperacaoMatrizController matrizController = new OperacaoMatrizController(this);
		matrizController.addObserver(this);

		lblEntendendoMatrizTutorial.addMouseListener(matrizController);
		lblSomaTutorial.addMouseListener(matrizController);
		lblSubtracaoTutorial.addMouseListener(matrizController);
		lblMultEscalarTutorial.addMouseListener(matrizController);
		lblMultMatrizTutorial.addMouseListener(matrizController);

		lblSomaTeste.addMouseListener(matrizController);
		lblSubtracaoTeste.addMouseListener(matrizController);
		lblMultEscalarTeste.addMouseListener(matrizController);
		lblMultMatrizTeste.addMouseListener(matrizController);
		lblTesteGeralTeste.addMouseListener(matrizController);

		lblVoltar.addMouseListener(matrizController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}