/**
 * 
 */
package br.edu.nota10.view.binario;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import br.edu.nota10.controller.binario.SubtracaoBinarioAplicacaoController;
import br.edu.nota10.model.Imagem;

/**
 * @author Jesse A. Done
 */
public class SubtracaoBinarioAplicacaoView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblVolta;
	public JLabel lblSinalSubtracao;
	public JLabel lblLinhaAplicacao;
	public JLabel lblSubtrairBinario;
	public JLabel lblResposta;
	public JLabel lblBtnSubtrair;

	public JTextField txtValor1;
	public JTextField txtValor2;

	public SubtracaoBinarioAplicacaoView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Subtrair de Bin�rio Aplica��o");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		imgFundo = new JLabel(imagens.fundo);
		lblVolta = new JLabel(imagens.voltar);
		lblSinalSubtracao = new JLabel(imagens.sinalSubtracao);
		lblLinhaAplicacao = new JLabel(imagens.linhaAplicacao);
		lblSubtrairBinario = new JLabel(imagens.subtracaoBinario);
		lblResposta = new JLabel();
		lblBtnSubtrair = new JLabel(imagens.subtrairBinario);

		txtValor1 = new JTextField("", 300);
		txtValor2 = new JTextField("", 300);		

		imgFundo.setBounds(0, 0, 800, 600);
		txtValor1.setBounds(350, 200, 100, 30);
		txtValor2.setBounds(350, 240, 100, 30);
		lblResposta.setBounds(340, 290, 100, 30);
		lblBtnSubtrair.setBounds(455, 290, 110, 30);
		lblSubtrairBinario.setBounds(190, 50, 413, 42);
		lblSinalSubtracao.setBounds(300, 230, 28, 28);
		lblLinhaAplicacao.setBounds(325, 280, 127, 4);
		lblVolta.setBounds(20, 515, 98, 45);

		add(txtValor1);
		add(txtValor2);
		add(lblResposta);
		add(lblBtnSubtrair);
		add(lblSubtrairBinario);
		add(lblSinalSubtracao);
		add(lblLinhaAplicacao);
		add(lblVolta);
		add(imgFundo);

		final SubtracaoBinarioAplicacaoController subtracaoBinarioAplicacaoController = new SubtracaoBinarioAplicacaoController(this);
		lblVolta.addMouseListener(subtracaoBinarioAplicacaoController);
		lblBtnSubtrair.addMouseListener(subtracaoBinarioAplicacaoController);
		subtracaoBinarioAplicacaoController.addObserver(this);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}