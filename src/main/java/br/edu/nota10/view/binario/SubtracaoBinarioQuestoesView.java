/**
 * 
 */
package br.edu.nota10.view.binario;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.binario.SubtracaoBinarioQuestoesController;
import br.edu.nota10.model.Imagem;

/**
 * @author Jesse A. Done
 */
public class SubtracaoBinarioQuestoesView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblSubtracaoBinarioQuestoes;
	public JLabel lblConferirQuestoes;
	public JLabel lblResultadoEx1;
	public JLabel lblResultadoEx2;
	public JLabel lblResultadoEx3;
	public JLabel lblResultadoEx4;
	public JLabel lblResultadoEx5;
	public JLabel lblResultadoEx6;
	public JLabel lblResultadoEx7;
	public JLabel lblResultadoEx8;
	public JLabel lblResultadoEx9;
	public JLabel lblResultadoEx10;
	public JLabel lblVolta;

	public JTextField txtResposta1;
	public JTextField txtResposta2;
	public JTextField txtResposta3;
	public JTextField txtResposta4;
	public JTextField txtResposta5;
	public JTextField txtResposta6;
	public JTextField txtResposta7;
	public JTextField txtResposta8;
	public JTextField txtResposta9;
	public JTextField txtResposta10;

	public JPanel painel;
	public JScrollPane scrollPane;

	public SubtracaoBinarioQuestoesView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Quest�es de SUbtra��o de Bin�rio");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		painel = new JPanel();
		painel.setLayout(null);
		painel.setPreferredSize(new Dimension(800, 2000));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblSubtracaoBinarioQuestoes = new JLabel(imagens.subtracaoBinarioTextoQuestoes);
		lblConferirQuestoes = new JLabel(imagens.conferirQuestoes);

		lblResultadoEx1 = new JLabel();
		lblResultadoEx2 = new JLabel();
		lblResultadoEx3 = new JLabel();
		lblResultadoEx4 = new JLabel();
		lblResultadoEx5 = new JLabel();
		lblResultadoEx6 = new JLabel();
		lblResultadoEx7 = new JLabel();
		lblResultadoEx8 = new JLabel();
		lblResultadoEx9 = new JLabel();
		lblResultadoEx10 = new JLabel();

		txtResposta1 = new JTextField("", 10);
		txtResposta2 = new JTextField("", 10);
		txtResposta3 = new JTextField("", 10);
		txtResposta4 = new JTextField("", 10);
		txtResposta5 = new JTextField("", 10);
		txtResposta6 = new JTextField("", 10);
		txtResposta7 = new JTextField("", 10);
		txtResposta8 = new JTextField("", 10);
		txtResposta9 = new JTextField("", 10);
		txtResposta10 = new JTextField("", 10);

		lblVolta = new JLabel(imagens.voltar);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblSubtracaoBinarioQuestoes.setBounds(20, 50, 740, 1885);
		lblConferirQuestoes.setBounds(300, 1920, 237, 38);

		lblResultadoEx1.setBounds(140, 140, 75, 65);
		lblResultadoEx2.setBounds(130, 330, 75, 65);
		lblResultadoEx3.setBounds(130, 510, 75, 65);
		lblResultadoEx4.setBounds(130, 695, 75, 65);
		lblResultadoEx5.setBounds(150, 880, 75, 65);
		lblResultadoEx6.setBounds(150, 1060, 75, 65);
		lblResultadoEx7.setBounds(150, 1250, 75, 65);
		lblResultadoEx8.setBounds(130, 1430, 75, 65);
		lblResultadoEx9.setBounds(130, 1610, 75, 65);
		lblResultadoEx10.setBounds(130, 1790, 75, 65);		

		txtResposta1.setBounds(30, 205, 100, 25);
		txtResposta2.setBounds(22, 390, 80, 25);
		txtResposta3.setBounds(18, 570, 100, 25);
		txtResposta4.setBounds(20, 755, 100, 25);
		txtResposta5.setBounds(25, 940, 100, 25);
		txtResposta6.setBounds(40, 1120, 100, 25);
		txtResposta7.setBounds(40, 1310, 100, 25);
		txtResposta8.setBounds(25, 1490, 100, 25);
		txtResposta9.setBounds(25, 1670, 100, 25);
		txtResposta10.setBounds(25, 1850, 80, 25);

		lblVolta.setBounds(30, 1950, 98, 45);

		// Adicionando item ao Painel
		painel.add(lblSubtracaoBinarioQuestoes);
		painel.add(lblConferirQuestoes);

		painel.add(lblResultadoEx1);
		painel.add(lblResultadoEx2);
		painel.add(lblResultadoEx3);
		painel.add(lblResultadoEx4);
		painel.add(lblResultadoEx5);
		painel.add(lblResultadoEx6);
		painel.add(lblResultadoEx7);
		painel.add(lblResultadoEx8);
		painel.add(lblResultadoEx9);
		painel.add(lblResultadoEx10);

		painel.add(txtResposta1);
		painel.add(txtResposta2);
		painel.add(txtResposta3);
		painel.add(txtResposta4);
		painel.add(txtResposta5);
		painel.add(txtResposta6);
		painel.add(txtResposta7);
		painel.add(txtResposta8);
		painel.add(txtResposta9);
		painel.add(txtResposta10);

		painel.add(lblVolta);

		painel.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(painel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final SubtracaoBinarioQuestoesController subtracaoBinarioQuestoesController = new SubtracaoBinarioQuestoesController(this);
		lblConferirQuestoes.addMouseListener(subtracaoBinarioQuestoesController);
		lblVolta.addMouseListener(subtracaoBinarioQuestoesController);
		subtracaoBinarioQuestoesController.addObserver(this);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}