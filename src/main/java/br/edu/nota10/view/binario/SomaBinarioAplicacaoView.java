/**
 * 
 */
package br.edu.nota10.view.binario;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import br.edu.nota10.controller.binario.SomaBinarioAplicacaoController;
import br.edu.nota10.model.Imagem;

/**
 * @author Jesse A. Done
 */
public class SomaBinarioAplicacaoView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblVolta;
	public JLabel lblSinalSoma;
	public JLabel lblLinhaAplicacao;
	public JLabel lblSomaBinario;
	public JLabel lblResposta;
	public JLabel lblBtnSomar;
	
	public JTextField txtValor1;
	public JTextField txtValor2;

	public SomaBinarioAplicacaoView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Soma de Bin�rio Aplica��o");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		imgFundo = new JLabel(imagens.fundo);
		lblVolta = new JLabel(imagens.voltar);
		lblSinalSoma = new JLabel(imagens.sinalAdicao);
		lblLinhaAplicacao = new JLabel(imagens.linhaAplicacao);
		lblSomaBinario = new JLabel(imagens.somaBinario);
		lblResposta = new JLabel();
		lblBtnSomar = new JLabel(imagens.somarBinario);
		
		txtValor1 = new JTextField("", 300);
		txtValor2 = new JTextField("", 300);		
		
		imgFundo.setBounds(0, 0, 800, 600);
		txtValor1.setBounds(350, 200, 100, 30);
		txtValor2.setBounds(350, 240, 100, 30);
		lblResposta.setBounds(340, 290, 100, 30);
		lblBtnSomar.setBounds(455, 290, 102, 30);
		lblSomaBinario.setBounds(230, 50, 328, 42);
		lblSinalSoma.setBounds(300, 230, 28, 28);
		lblLinhaAplicacao.setBounds(325, 280, 127, 4);
		lblVolta.setBounds(20, 515, 98, 45);

		add(txtValor1);
		add(txtValor2);
		add(lblResposta);
		add(lblBtnSomar);
		add(lblSomaBinario);
		add(lblSinalSoma);
		add(lblLinhaAplicacao);
		add(lblVolta);
		add(imgFundo);
		
		final SomaBinarioAplicacaoController somaBinarioAplicacaoController = new SomaBinarioAplicacaoController(this);
		lblVolta.addMouseListener(somaBinarioAplicacaoController);
		lblBtnSomar.addMouseListener(somaBinarioAplicacaoController);
		somaBinarioAplicacaoController.addObserver(this);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}