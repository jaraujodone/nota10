/**
 * 
 */
package br.edu.nota10.view.binario;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;

import br.edu.nota10.controller.binario.SomaBinarioTutorialController;
import br.edu.nota10.model.Imagem;

/**
 * @author Jesse A. Done
 */
public class SomaBinarioTutorialView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblSomaBinarioTutorialTexto;
	public JLabel lblConferirEx1;
	public JLabel lblResultadoEx1;
	public JLabel lblVolta;

	public JTextField txtResposta;

	public JPanel painel;
	public JScrollPane scrollPane;

	public SomaBinarioTutorialView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Tutorial Adi��o de Bin�rio");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Criando o Painel
		painel = new JPanel();
		painel.setLayout(null);
		painel.setPreferredSize(new Dimension(800, 1950));

		imgFundo = new JLabel(imagens.fundoTutorial);
		lblSomaBinarioTutorialTexto = new JLabel(imagens.acicaoBinarioTextoTutorial);
		lblConferirEx1 = new JLabel(imagens.conferir);
		lblResultadoEx1 = new JLabel();
		lblVolta = new JLabel(imagens.voltar);
		txtResposta = new JTextField("", 10);

		imgFundo.setBounds(0, 0, 800, 3500);
		lblSomaBinarioTutorialTexto.setBounds(20, 50, 743, 1819);
		lblConferirEx1.setBounds(100, 1850, 151, 38);
		lblResultadoEx1.setBounds(180, 1750, 75, 65);
		txtResposta.setBounds(62, 1810, 100, 25);
		lblVolta.setBounds(30, 1900, 98, 45);

		// Adicionando item ao Painel
		painel.add(lblSomaBinarioTutorialTexto);
		painel.add(lblConferirEx1);
		painel.add(lblResultadoEx1);
		painel.add(lblVolta);
		painel.add(txtResposta);

		painel.add(imgFundo);

		// Criando o Scroll e passando o Painel para ele
		scrollPane = new JScrollPane(painel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setLayout(new ScrollPaneLayout());
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		// Adicionando o Scroll ao Frame
		add(scrollPane);

		final SomaBinarioTutorialController somaBinarioController = new SomaBinarioTutorialController(this);
		lblConferirEx1.addMouseListener(somaBinarioController);
		lblVolta.addMouseListener(somaBinarioController);
		somaBinarioController.addObserver(this);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }

	public static void main(String[] args) {
		new SomaBinarioTutorialView();
	}
}