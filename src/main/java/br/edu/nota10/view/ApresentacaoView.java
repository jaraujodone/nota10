package br.edu.nota10.view;

import javax.swing.JFrame;
import javax.swing.JLabel;

import br.edu.nota10.model.Imagem;

/**
 * @author Jesse A. Done
 * 
 * Classe responsável por mostrar o splash 
 * e chamar a tela de menu
 */

public class ApresentacaoView extends JFrame {
	final Imagem imagens = new Imagem();

	public JLabel imgTelaInicial;
	public JLabel imgCreditos;

	public ApresentacaoView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		imgTelaInicial = new JLabel(imagens.telaApresentacao);
		imgCreditos = new JLabel(imagens.creditos);

		imgTelaInicial.setBounds(0, 0, 800, 600);
		imgCreditos.setBounds(0, 555, 800, 20);

		add(imgCreditos);
		add(imgTelaInicial);
		add(new JLabel());

		this.setVisible(true);
	}

	public static void main(String[] args) throws InterruptedException{
		final ApresentacaoView telaApresenta = new ApresentacaoView();
		
		// espera para poder mostrar o splash
		Thread.currentThread().sleep(5000);
		
		// desativa a tela
		telaApresenta.setVisible(false);
		
		new MenuView();
	}
}