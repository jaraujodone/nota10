package br.edu.nota10.view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

import br.edu.nota10.controller.MenuController;
import br.edu.nota10.model.Imagem;

/**
 * @author Jesse A. Done
 */
public class MenuView extends JFrame implements Observer {
	final Imagem imagens = new Imagem();

	public JLabel imgFundo;

	public JLabel lblOperacaoMatriz;
	public JLabel lblOperacaoBinaria;

	public MenuView() {
		this.setSize(800, 600);
		this.setTitle("NOTA 10 - Menu");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		imgFundo = new JLabel(imagens.fundo);
		lblOperacaoMatriz = new JLabel(imagens.operacaoMatriz);
		lblOperacaoBinaria = new JLabel(imagens.operacaoBinaria);

		imgFundo.setBounds(0, 0, 800, 600);
		lblOperacaoMatriz.setBounds(50, 80, 500, 50);
		lblOperacaoBinaria.setBounds(50, 200, 412, 50);

		add(lblOperacaoMatriz);
		add(lblOperacaoBinaria);
		add(imgFundo);
		add(new JLabel());

		final MenuController menuController = new MenuController(this);
		menuController.addObserver(this);
		lblOperacaoMatriz.addMouseListener(menuController);
		lblOperacaoBinaria.addMouseListener(menuController);

		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) { }
}