package br.edu.nota10.model;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * @author Jesse A. Done
 * 
 * Classe Responsavel por carregar as 
 * imagens em um objeto ImagemIcon
 */

public class Imagem {
	//Caminho da pasta no resources
	final static String IMAGEM = "/imagem/";

	// Imagens de mais de uma tela
	public static ImageIcon fundo;
	public static ImageIcon tutorial;
	public static ImageIcon aplicacao;
	public static ImageIcon testeSeuConhecmento;

	public static ImageIcon voltar;
	public static ImageIcon voltarOn;

	public static ImageIcon linha;
	public static ImageIcon coluna;

	public static ImageIcon ok;
	public static ImageIcon okOn;

	// Apresenta��o View
	public static ImageIcon telaApresentacao;
	public static ImageIcon creditos;

	// Menu View
	public static ImageIcon operacaoMatriz;
	public static ImageIcon operacaoMatrizOn;
	public static ImageIcon operacaoBinaria;
	public static ImageIcon operacaoBinariaOn;

	// Opera��o Matricial
	public static ImageIcon entendendoMatriz;
	public static ImageIcon entendendoMatrizOn;
	public static ImageIcon soma;
	public static ImageIcon somaOn;
	public static ImageIcon subtracao;
	public static ImageIcon subtracaoOn;
	public static ImageIcon multEscalar;
	public static ImageIcon multEscalarOn;
	public static ImageIcon multMatriz;
	public static ImageIcon multMatrizOn;
	public static ImageIcon testeGeral;
	public static ImageIcon testeGeralOn;

	// Tutorial View
	public static ImageIcon fundoTutorial;
	public static ImageIcon conferir;
	public static ImageIcon conferirOn;
	public static ImageIcon certo;
	public static ImageIcon errado;

	// Entendendo Matriz Tutorial
	public static ImageIcon entendendoMatrizTextoTutorial;

	// Soma Tutorial
	public static ImageIcon adicacaoTextoTutorial;

	// Subtra��o Tutorial
	public static ImageIcon subtracaoTextoTutorial;

	// Multiplica��o Por Escalar
	public static ImageIcon multiplicacaoPorEscalarTextoTutorial;

	// Multiplica��o de Matriz
	public static ImageIcon multiplicacaoTextoTutorial;

	// Questoes View
	public static ImageIcon conferirQuestoes;
	public static ImageIcon conferirQuestoesOn;

	// Soma Teste
	public static ImageIcon adicacaoTextoQuestoes;

	// Subtra��o Teste
	public static ImageIcon subtracaoTextoQuestoes;

	// Multiplica��o por Escalar Teste
	public static ImageIcon multPorEscalarTextoQuestoes;

	// Multiplica��o de Matrizes Teste
	public static ImageIcon multDeMatrizesTextoQuestoes;

	// Soma de Binario Tutorial
	public static ImageIcon acicaoBinarioTextoTutorial;

	// Subtracao de Binario Tutorial
	public static ImageIcon subtracaoBinarioTextoTutorial;

	// Subtracao de Binario Teste
	public static ImageIcon subtracaoBinarioTextoQuestoes;

	// Soma de Binario Teste
	public static ImageIcon somaBinarioTextoQuestoes;

	// Aplica��o Binario
	public static ImageIcon sinalAdicao;
	public static ImageIcon sinalSubtracao;
	public static ImageIcon linhaAplicacao;
	public static ImageIcon somaBinario;
	public static ImageIcon subtracaoBinario;
	public static ImageIcon somarBinario;
	public static ImageIcon somarBinarioOn;
	public static ImageIcon subtrairBinario;
	public static ImageIcon subtrairBinarioOn;	

	//Bloco estatico, carrega antes mesmo do construtor
	static {
		try {
			fundo = criarImagem("fundo.jpg");
			tutorial = criarImagem("tutorial.png");
			aplicacao = criarImagem("aplicacao.png");
			testeSeuConhecmento = criarImagem("testeSeusConhecimentos.png");

			voltar = criarImagem("voltar.png");
			voltarOn = criarImagem("voltarOn.png");

			linha = criarImagem("linha.png");
			coluna = criarImagem("coluna.png");

			ok = criarImagem("ok.jpg");
			okOn = criarImagem("okOn.jpg");

			// Apresenta��o View
			telaApresentacao = criarImagem("telaApresentacao.jpg");
			creditos = criarImagem("creditos.jpg");

			// Menu View
			operacaoMatriz = criarImagem("operacaoMatriz.png");
			operacaoMatrizOn = criarImagem("operacaoMatriz_on.png");
			operacaoBinaria = criarImagem("operacoesBinarias.png");
			operacaoBinariaOn = criarImagem("operacoesBinariasOn.png");

			// Opera��o Matricial
			entendendoMatriz = criarImagem("entendendoAEstruturaMatricial.png");
			entendendoMatrizOn = criarImagem("entendendoAEstruturaMatricialOn.png");
			soma = criarImagem("soma.png");
			somaOn = criarImagem("somaOn.png");
			subtracao = criarImagem("subtracao.png");
			subtracaoOn = criarImagem("subtracaoOn.png");
			multEscalar = criarImagem("multiplicacoPorEscalar.png");
			multEscalarOn = criarImagem("multiplicacoPorEscalarOn.png");
			multMatriz = criarImagem("multiplicacoDeMatrizes.png");
			multMatrizOn = criarImagem("multiplicacoDeMatrizesOn.png");
			testeGeral = criarImagem("testeGeral.png");
			testeGeralOn = criarImagem("testeGeralOn.png");

			// Tutorial View
			fundoTutorial = criarImagem("fundoTutorial.jpg");
			conferir = criarImagem("conferir.png");
			conferirOn = criarImagem("conferirOn.png");
			certo = criarImagem("certo.png");
			errado = criarImagem("errado.png");

			// Entendendo Matriz Tutorial
			entendendoMatrizTextoTutorial = criarImagem("entendendoAEstruturaMatricialTextoTutorial.png");

			// Soma Tutorial
			adicacaoTextoTutorial = criarImagem("adicaoTextoTutorial.png");

			// Subtra��o Tutorial
			subtracaoTextoTutorial = criarImagem("subtracaoTextoTutorial.png");

			// Multiplica��o Por Escalar
			multiplicacaoPorEscalarTextoTutorial = criarImagem("multiplicacaoPorEscalarTextoTutorial.png");

			// Multiplica��o de Matriz
			multiplicacaoTextoTutorial = criarImagem("multiplicacaoTextoTutorial.png");

			// Questoes View
			conferirQuestoes = criarImagem("conferirQuestoes.png");
			conferirQuestoesOn = criarImagem("conferirQuestoesOn.png");

			// Soma Teste
			adicacaoTextoQuestoes = criarImagem("questoesDeAdicaoDeMatrizes.png");

			// Subtra��o Teste
			subtracaoTextoQuestoes = criarImagem("questoesDeSubtracaoDeMatrizes.png");

			// Multiplica��o por Escalar Teste
			multPorEscalarTextoQuestoes = criarImagem("questoesDeMultiplicacaoDeMatrizesPorEscalar.png");

			// Multiplica��o de Matrizes Teste
			multDeMatrizesTextoQuestoes = criarImagem("questoesDeMultiplicacaoDeMatrizes.png");

			// Soma de Binario Tutorial
			acicaoBinarioTextoTutorial = criarImagem("adicaoDeBinarioTextoTutorial.png");

			// Subtracao de Binario Tutorial
			subtracaoBinarioTextoTutorial = criarImagem("subtracaoDeBinarioTextoTutorial.png");

			// Subtracao de Binario Teste
			subtracaoBinarioTextoQuestoes = criarImagem("questoesDeSubtracaoDeBinarios.png");

			// Soma de Binario Teste
			somaBinarioTextoQuestoes = criarImagem("questoesDeAdicaoDeBinarios.png");

			// Aplica��o Binario
			sinalAdicao = criarImagem("sinalAdicao.png");
			sinalSubtracao = criarImagem("sinalSubtracao.png");
			linhaAplicacao = criarImagem("linhaBinario.png");
			somaBinario = criarImagem("somaBinario.png");
			subtracaoBinario = criarImagem("subtracaoBinario.png");
			somarBinario = criarImagem("botaoSomar.png");
			somarBinarioOn = criarImagem("botaoSomarOn.png");
			subtrairBinario = criarImagem("botaoSubtrair.png");
			subtrairBinarioOn = criarImagem("botaoSubtrairOn.png");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param nomeImagem
	 * @return
	 * @throws IOException
	 */
	private static ImageIcon criarImagem(final String nomeImagem) throws IOException {
		final InputStream in =  Imagem.class.getResourceAsStream(IMAGEM + nomeImagem);

		final Image image = ImageIO.read(in);
		return new ImageIcon(image);
	}
}