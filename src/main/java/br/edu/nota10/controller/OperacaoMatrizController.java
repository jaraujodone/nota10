/**
 * 
 */
package br.edu.nota10.controller;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.MenuView;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.EntendendoEstruturaMatrizTutorialView;
import br.edu.nota10.view.matriz.MultiplicacaoDeMatrizQuestoesView;
import br.edu.nota10.view.matriz.MultiplicacaoDeMatrizTutorialView;
import br.edu.nota10.view.matriz.MultiplicacaoPorEscalarMatrizQuestoesView;
import br.edu.nota10.view.matriz.MultiplicacaoPorEscalarMatrizTutorialView;
import br.edu.nota10.view.matriz.SomaMatrizQuestoesView;
import br.edu.nota10.view.matriz.SomaMatrizTutorialView;
import br.edu.nota10.view.matriz.SubtracaoMatrizQuestoesView;
import br.edu.nota10.view.matriz.SubtracaoMatrizTutorialView;

/**
 * @author Jesse A. Done
 */
public class OperacaoMatrizController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final OperacaoMatrizView operacaoMatriz;

	public OperacaoMatrizController(final OperacaoMatrizView operacaoMatriz) {
		this.operacaoMatriz = operacaoMatriz;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		// Tutorial
		if(e.getSource().equals(operacaoMatriz.lblEntendendoMatrizTutorial)){
			operacaoMatriz.setVisible(false);
			new EntendendoEstruturaMatrizTutorialView();
		}
		if(e.getSource().equals(operacaoMatriz.lblSomaTutorial)){
			operacaoMatriz.setVisible(false);
			new SomaMatrizTutorialView();
		}
		if(e.getSource().equals(operacaoMatriz.lblMultEscalarTutorial)){
			operacaoMatriz.setVisible(false);
			new MultiplicacaoPorEscalarMatrizTutorialView();
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoTutorial)){
			operacaoMatriz.setVisible(false);
			new SubtracaoMatrizTutorialView();
		}
		if(e.getSource().equals(operacaoMatriz.lblMultMatrizTutorial)){
			operacaoMatriz.setVisible(false);
			new MultiplicacaoDeMatrizTutorialView();
		}

		// Teste seus conhecimentos
		if(e.getSource().equals(operacaoMatriz.lblSomaTeste)){
			operacaoMatriz.setVisible(false);
			new SomaMatrizQuestoesView();
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoTeste)){
			operacaoMatriz.setVisible(false);
			new SubtracaoMatrizQuestoesView();
		}
		if(e.getSource().equals(operacaoMatriz.lblMultEscalarTeste)){
			operacaoMatriz.setVisible(false);
			new MultiplicacaoPorEscalarMatrizQuestoesView();
		}
		if(e.getSource().equals(operacaoMatriz.lblMultMatrizTeste)){
			operacaoMatriz.setVisible(false);
			new MultiplicacaoDeMatrizQuestoesView();
		}
		if(e.getSource().equals(operacaoMatriz.lblTesteGeralTeste)){
			//operacaoMatriz.lblTesteGeralTeste.setIcon(imagem.testeGeralOn);
		}

		// Voltar
		if(e.getSource().equals(operacaoMatriz.lblVoltar)){
			operacaoMatriz.setVisible(false);
			new MenuView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		e.getComponent().setCursor(new Cursor(Cursor.HAND_CURSOR));
		// Tutorial
		if(e.getSource().equals(operacaoMatriz.lblEntendendoMatrizTutorial)){
			operacaoMatriz.lblEntendendoMatrizTutorial.setIcon(imagem.entendendoMatrizOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblSomaTutorial)){
			operacaoMatriz.lblSomaTutorial.setIcon(imagem.somaOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoTutorial)){
			operacaoMatriz.lblSubtracaoTutorial.setIcon(imagem.subtracaoOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultEscalarTutorial)){
			operacaoMatriz.lblMultEscalarTutorial.setIcon(imagem.multEscalarOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultMatrizTutorial)){
			operacaoMatriz.lblMultMatrizTutorial.setIcon(imagem.multMatrizOn);
		}

		// Teste seus conhecimentos
		if(e.getSource().equals(operacaoMatriz.lblSomaTeste)){
			operacaoMatriz.lblSomaTeste.setIcon(imagem.somaOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoTeste)){
			operacaoMatriz.lblSubtracaoTeste.setIcon(imagem.subtracaoOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultEscalarTeste)){
			operacaoMatriz.lblMultEscalarTeste.setIcon(imagem.multEscalarOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultMatrizTeste)){
			operacaoMatriz.lblMultMatrizTeste.setIcon(imagem.multMatrizOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblTesteGeralTeste)){
			operacaoMatriz.lblTesteGeralTeste.setIcon(imagem.testeGeralOn);
		}

		// Voltar
		if(e.getSource().equals(operacaoMatriz.lblVoltar)){
			operacaoMatriz.lblVoltar.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		// Tutorial
		if(e.getSource().equals(operacaoMatriz.lblEntendendoMatrizTutorial)){
			operacaoMatriz.lblEntendendoMatrizTutorial.setIcon(imagem.entendendoMatriz);
		}
		if(e.getSource().equals(operacaoMatriz.lblSomaTutorial)){
			operacaoMatriz.lblSomaTutorial.setIcon(imagem.soma);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoTutorial)){
			operacaoMatriz.lblSubtracaoTutorial.setIcon(imagem.subtracao);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultEscalarTutorial)){
			operacaoMatriz.lblMultEscalarTutorial.setIcon(imagem.multEscalar);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultMatrizTutorial)){
			operacaoMatriz.lblMultMatrizTutorial.setIcon(imagem.multMatriz);
		}

		// Teste seus conhecimentos
		if(e.getSource().equals(operacaoMatriz.lblSomaTeste)){
			operacaoMatriz.lblSomaTeste.setIcon(imagem.soma);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoTeste)){
			operacaoMatriz.lblSubtracaoTeste.setIcon(imagem.subtracao);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultEscalarTeste)){
			operacaoMatriz.lblMultEscalarTeste.setIcon(imagem.multEscalar);
		}
		if(e.getSource().equals(operacaoMatriz.lblMultMatrizTeste)){
			operacaoMatriz.lblMultMatrizTeste.setIcon(imagem.multMatriz);
		}
		if(e.getSource().equals(operacaoMatriz.lblTesteGeralTeste)){
			operacaoMatriz.lblTesteGeralTeste.setIcon(imagem.testeGeral);
		}

		// Voltar
		if(e.getSource().equals(operacaoMatriz.lblVoltar)){
			operacaoMatriz.lblVoltar.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }
}