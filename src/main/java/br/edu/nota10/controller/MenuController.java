/**
 * 
 */
package br.edu.nota10.controller;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.MenuView;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.OperacaoMatrizView;

/**
 * @author Jesse A. Done
 */
public class MenuController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final MenuView menu;

	public MenuController(final MenuView menu) {
		this.menu = menu;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(menu.lblOperacaoMatriz)){
			menu.setVisible(false);
			new OperacaoMatrizView();
		}
		if(e.getSource().equals(menu.lblOperacaoBinaria)){
			menu.setVisible(false);
			new OperacaoBinariaView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		e.getComponent().setCursor(new Cursor(Cursor.HAND_CURSOR));

		if(e.getSource().equals(menu.lblOperacaoMatriz)){
			menu.lblOperacaoMatriz.setIcon(imagem.operacaoMatrizOn);
		}
		if(e.getSource().equals(menu.lblOperacaoBinaria)){
			menu.lblOperacaoBinaria.setIcon(imagem.operacaoBinariaOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(menu.lblOperacaoMatriz)){
			menu.lblOperacaoMatriz.setIcon(imagem.operacaoMatriz);
		}
		if(e.getSource().equals(menu.lblOperacaoBinaria)){
			menu.lblOperacaoBinaria.setIcon(imagem.operacaoBinaria);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }
}