/**
 * 
 */
package br.edu.nota10.controller;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.MenuView;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.binario.SomaBinarioAplicacaoView;
import br.edu.nota10.view.binario.SomaBinarioQuestoesView;
import br.edu.nota10.view.binario.SomaBinarioTutorialView;
import br.edu.nota10.view.binario.SubtracaoBinarioAplicacaoView;
import br.edu.nota10.view.binario.SubtracaoBinarioQuestoesView;
import br.edu.nota10.view.binario.SubtracaoBinarioTutorialView;

/**
 * @author Jesse A. Done
 */
public class OperacaoBinariaController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final OperacaoBinariaView operacaoMatriz;

	public OperacaoBinariaController(final OperacaoBinariaView operacaoMatriz) {
		this.operacaoMatriz = operacaoMatriz;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		// Tutorial
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioTutorial)) {
			operacaoMatriz.setVisible(false);
			new SomaBinarioTutorialView();
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioTutorial)) {
			operacaoMatriz.setVisible(false);
			new SubtracaoBinarioTutorialView();
		}

		// Aplica��o
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioAplicacao)) {
			operacaoMatriz.setVisible(false);
			new SomaBinarioAplicacaoView();
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioAplicacao)) {
			operacaoMatriz.setVisible(false);
			new SubtracaoBinarioAplicacaoView();
		}

		// Teste seus Conhecimentos
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioTeste)) {
			operacaoMatriz.setVisible(false);
			new SomaBinarioQuestoesView();
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioTeste)) {
			operacaoMatriz.setVisible(false);
			new SubtracaoBinarioQuestoesView();
		}
		if(e.getSource().equals(operacaoMatriz.lblTesteGeralBinarioTeste)) {
			operacaoMatriz.setVisible(false);

		}

		// Voltar
		if(e.getSource().equals(operacaoMatriz.lblVoltar)) {
			operacaoMatriz.setVisible(false);
			new MenuView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		e.getComponent().setCursor(new Cursor(Cursor.HAND_CURSOR));

		// Tutorial
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioTutorial)) {
			operacaoMatriz.lblSomaBinarioTutorial.setIcon(imagem.somaOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioTutorial)) {
			operacaoMatriz.lblSubtracaoBinarioTutorial.setIcon(imagem.subtracaoOn);
		}

		// Aplica��o
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioAplicacao)) {
			operacaoMatriz.lblSomaBinarioAplicacao.setIcon(imagem.somaOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioAplicacao)) {
			operacaoMatriz.lblSubtracaoBinarioAplicacao.setIcon(imagem.subtracaoOn);
		}

		// Teste seus Conhecimentos
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioTeste)) {
			operacaoMatriz.lblSomaBinarioTeste.setIcon(imagem.somaOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioTeste)) {
			operacaoMatriz.lblSubtracaoBinarioTeste.setIcon(imagem.subtracaoOn);
		}
		if(e.getSource().equals(operacaoMatriz.lblTesteGeralBinarioTeste)) {
			operacaoMatriz.lblTesteGeralBinarioTeste.setIcon(imagem.testeGeralOn);
		}

		// Voltar
		if(e.getSource().equals(operacaoMatriz.lblVoltar)) {
			operacaoMatriz.lblVoltar.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		// Tutorial
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioTutorial)) {
			operacaoMatriz.lblSomaBinarioTutorial.setIcon(imagem.soma);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioTutorial)) {
			operacaoMatriz.lblSubtracaoBinarioTutorial.setIcon(imagem.subtracao);
		}

		// Aplica��o
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioAplicacao)) {
			operacaoMatriz.lblSomaBinarioAplicacao.setIcon(imagem.soma);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioAplicacao)) {
			operacaoMatriz.lblSubtracaoBinarioAplicacao.setIcon(imagem.subtracao);
		}

		// Teste seus Conhecimentos
		if(e.getSource().equals(operacaoMatriz.lblSomaBinarioTeste)) {
			operacaoMatriz.lblSomaBinarioTeste.setIcon(imagem.soma);
		}
		if(e.getSource().equals(operacaoMatriz.lblSubtracaoBinarioTeste)) {
			operacaoMatriz.lblSubtracaoBinarioTeste.setIcon(imagem.subtracao);
		}
		if(e.getSource().equals(operacaoMatriz.lblTesteGeralBinarioTeste)) {
			operacaoMatriz.lblTesteGeralBinarioTeste.setIcon(imagem.testeGeral);
		}

		// Voltar
		if(e.getSource().equals(operacaoMatriz.lblVoltar)) {
			operacaoMatriz.lblVoltar.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }
}