/**
 * 
 */
package br.edu.nota10.controller.binario;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.binario.SubtracaoBinarioQuestoesView;

/**
 * @author Jesse A. Done
 */
public class SubtracaoBinarioQuestoesController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SubtracaoBinarioQuestoesView subBinarioView;
	
	public SubtracaoBinarioQuestoesController(final SubtracaoBinarioQuestoesView subBinarioView) {
		this.subBinarioView = subBinarioView;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(subBinarioView.lblConferirQuestoes)) {
			confirmaResultadoEx1();
			confirmaResultadoEx2();
			confirmaResultadoEx3();
			confirmaResultadoEx4();
			confirmaResultadoEx5();
			confirmaResultadoEx6();
			confirmaResultadoEx7();
			confirmaResultadoEx8();
			confirmaResultadoEx9();
			confirmaResultadoEx10();
		}
		if(e.getSource().equals(subBinarioView.lblVolta)) {
			subBinarioView.setVisible(false);
			new OperacaoBinariaView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(subBinarioView.lblConferirQuestoes)) {
			subBinarioView.lblConferirQuestoes.setIcon(imagem.conferirQuestoesOn);
		}
		if(e.getSource().equals(subBinarioView.lblVolta)) {
			subBinarioView.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(subBinarioView.lblConferirQuestoes)) {
			subBinarioView.lblConferirQuestoes.setIcon(imagem.conferirQuestoes);
		}
		if(e.getSource().equals(subBinarioView.lblVolta)) {
			subBinarioView.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }
	
	/**
	 * Confere o resultado 1
	 */
	private void confirmaResultadoEx1() {
		final String subtracaoResposta = "01110";
		
		if(subBinarioView.txtResposta1.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx1.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx1.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 2
	 */
	private void confirmaResultadoEx2() {
		final String subtracaoResposta = "010";
		
		if(subBinarioView.txtResposta2.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx2.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx2.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 3
	 */
	private void confirmaResultadoEx3() {
		final String subtracaoResposta = "1100";
		
		if(subBinarioView.txtResposta3.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx3.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx3.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 4
	 */
	private void confirmaResultadoEx4() {
		final String subtracaoResposta = "0111";
		
		if(subBinarioView.txtResposta4.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx4.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx4.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 5
	 */
	private void confirmaResultadoEx5() {
		final String subtracaoResposta = "0100";
		
		if(subBinarioView.txtResposta5.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx5.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx5.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 6
	 */
	private void confirmaResultadoEx6() {
		final String subtracaoResposta = "111010";
		
		if(subBinarioView.txtResposta6.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx6.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx6.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 7
	 */
	private void confirmaResultadoEx7() {
		final String subtracaoResposta = "01101";
		
		if(subBinarioView.txtResposta7.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx7.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx7.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 8
	 */
	private void confirmaResultadoEx8() {
		final String subtracaoResposta = "001";
		
		if(subBinarioView.txtResposta8.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx8.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx8.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 9
	 */
	private void confirmaResultadoEx9() {
		final String subtracaoResposta = "00011";
		
		if(subBinarioView.txtResposta9.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx9.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx9.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 10
	 */
	private void confirmaResultadoEx10() {
		final String subtracaoResposta = "110";
		
		if(subBinarioView.txtResposta10.getText().trim().equals(subtracaoResposta)) {
			subBinarioView.lblResultadoEx10.setIcon(imagem.certo);
		} else {
			subBinarioView.lblResultadoEx10.setIcon(imagem.errado);
		}
	}
}