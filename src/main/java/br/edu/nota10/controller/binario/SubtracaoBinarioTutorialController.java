/**
 * 
 */
package br.edu.nota10.controller.binario;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.binario.SubtracaoBinarioTutorialView;

/**
 * @author Jesse A. Done
 */
public class SubtracaoBinarioTutorialController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SubtracaoBinarioTutorialView subtracaoBinarioView;

	public SubtracaoBinarioTutorialController(final SubtracaoBinarioTutorialView subtracaoBinarioView) {
		this.subtracaoBinarioView = subtracaoBinarioView;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(subtracaoBinarioView.lblConferirEx1)) {
			confirmaResultadoEx1();
		}
		if(e.getSource().equals(subtracaoBinarioView.lblVolta)) {
			subtracaoBinarioView.setVisible(false);
			new OperacaoBinariaView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) { 
		if(e.getSource().equals(subtracaoBinarioView.lblConferirEx1)) {
			subtracaoBinarioView.lblConferirEx1.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(subtracaoBinarioView.lblVolta)) {
			subtracaoBinarioView.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(subtracaoBinarioView.lblConferirEx1)) {
			subtracaoBinarioView.lblConferirEx1.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(subtracaoBinarioView.lblVolta)) {
			subtracaoBinarioView.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado 1
	 */
	private void confirmaResultadoEx1() {
		final String subtracaoResposta = "0101";

		if(subtracaoResposta.equals(subtracaoBinarioView.txtResposta.getText().trim())) {
			subtracaoBinarioView.lblResultadoEx1.setIcon(imagem.certo);
		} else {
			subtracaoBinarioView.lblResultadoEx1.setIcon(imagem.errado);
		}
	}
}