/**
 * 
 */
package br.edu.nota10.controller.binario;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.binario.SomaBinarioQuestoesView;

/**
 * @author Jesse A. Done
 */
public class SomaBinarioQuestoesController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SomaBinarioQuestoesView somaBinarioView;

	public SomaBinarioQuestoesController(final SomaBinarioQuestoesView somaBinarioView) {
		this.somaBinarioView = somaBinarioView;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(somaBinarioView.lblConferirQuestoes)) {
			confirmaResultadoEx1();
			confirmaResultadoEx2();
			confirmaResultadoEx3();
			confirmaResultadoEx4();
			confirmaResultadoEx5();
			confirmaResultadoEx6();
			confirmaResultadoEx7();
			confirmaResultadoEx8();
			confirmaResultadoEx9();
			confirmaResultadoEx10();
		}
		if(e.getSource().equals(somaBinarioView.lblVolta)) {
			somaBinarioView.setVisible(false);
			new OperacaoBinariaView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(somaBinarioView.lblConferirQuestoes)) {
			somaBinarioView.lblConferirQuestoes.setIcon(imagem.conferirQuestoesOn);
		}
		if(e.getSource().equals(somaBinarioView.lblVolta)) {
			somaBinarioView.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(somaBinarioView.lblConferirQuestoes)) {
			somaBinarioView.lblConferirQuestoes.setIcon(imagem.conferirQuestoes);
		}
		if(e.getSource().equals(somaBinarioView.lblVolta)) {
			somaBinarioView.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }
	
	/**
	 * Confere o resultado 1
	 */
	private void confirmaResultadoEx1() {
		final String somaResposta = "1111101";
		
		if(somaBinarioView.txtResposta1.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx1.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx1.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 2
	 */
	private void confirmaResultadoEx2() {
		final String somaResposta = "111110";
		
		if(somaBinarioView.txtResposta2.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx2.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx2.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 3
	 */
	private void confirmaResultadoEx3() {
		final String somaResposta = "1010";
		
		if(somaBinarioView.txtResposta3.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx3.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx3.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 4
	 */
	private void confirmaResultadoEx4() {
		final String somaResposta = "1111";
		
		if(somaBinarioView.txtResposta4.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx4.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx4.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 5
	 */
	private void confirmaResultadoEx5() {
		final String somaResposta = "10111";
		
		if(somaBinarioView.txtResposta5.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx5.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx5.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 6
	 */
	private void confirmaResultadoEx6() {
		final String somaResposta = "100010";
		
		if(somaBinarioView.txtResposta6.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx6.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx6.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 7
	 */
	private void confirmaResultadoEx7() {
		final String somaResposta = "100";
		
		if(somaBinarioView.txtResposta7.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx7.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx7.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 8
	 */
	private void confirmaResultadoEx8() {
		final String somaResposta = "101011";
		
		if(somaBinarioView.txtResposta8.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx8.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx8.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 9
	 */
	private void confirmaResultadoEx9() {
		final String somaResposta = "1110";
		
		if(somaBinarioView.txtResposta9.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx9.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx9.setIcon(imagem.errado);
		}
	}
	
	/**
	 * Confere o resultado 10
	 */
	private void confirmaResultadoEx10() {
		final String somaResposta = "101010";
		
		if(somaBinarioView.txtResposta10.getText().trim().equals(somaResposta)) {
			somaBinarioView.lblResultadoEx10.setIcon(imagem.certo);
		} else {
			somaBinarioView.lblResultadoEx10.setIcon(imagem.errado);
		}
	}
}