/**
 * 
 */
package br.edu.nota10.controller.binario;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.binario.SubtracaoBinarioAplicacaoView;

/**
 * @author Jesse A. Done
 */
public class SubtracaoBinarioAplicacaoController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SubtracaoBinarioAplicacaoView subtrairBinarioView;

	public SubtracaoBinarioAplicacaoController(final SubtracaoBinarioAplicacaoView subtrairBinarioView) {
		this.subtrairBinarioView = subtrairBinarioView;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(subtrairBinarioView.lblBtnSubtrair)) {
			subtrairBinario(subtrairBinarioView.txtValor1.getText().trim(), subtrairBinarioView.txtValor2.getText().trim());
		}
		if(e.getSource().equals(subtrairBinarioView.lblVolta)) {
			subtrairBinarioView.setVisible(false);
			new OperacaoBinariaView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(subtrairBinarioView.lblBtnSubtrair)) {
			subtrairBinarioView.lblBtnSubtrair.setIcon(imagem.subtrairBinarioOn);
		}
		if(e.getSource().equals(subtrairBinarioView.lblVolta)) {
			subtrairBinarioView.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(subtrairBinarioView.lblBtnSubtrair)) {
			subtrairBinarioView.lblBtnSubtrair.setIcon(imagem.subtrairBinario);
		}
		if(e.getSource().equals(subtrairBinarioView.lblVolta)) {
			subtrairBinarioView.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }
	
	/**
	 * Subtrair Binario
	 * @param numero1
	 * @param numero2
	 */
	private void subtrairBinario(final String numero1, final String numero2) {
		int valor1 = 0, valor2 = 0;
		try {
			valor1 = Integer.parseInt(numero1, 2);
			valor2 = Integer.parseInt(numero2, 2);
		} catch (final Exception exc) {
			JOptionPane.showMessageDialog(null, "Valor Digitado n�o � binario");
		}
		
		subtrairBinarioView.lblResposta.setText(Integer.toBinaryString((valor1 - valor2)));
	}
}