/**
 * 
 */
package br.edu.nota10.controller.binario;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.binario.SomaBinarioTutorialView;

/**
 * @author Jesse A. Done
 */
public class SomaBinarioTutorialController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SomaBinarioTutorialView somaBinariaTutorial;

	public SomaBinarioTutorialController(final SomaBinarioTutorialView somaBinariaTutorial) {
		this.somaBinariaTutorial = somaBinariaTutorial;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(somaBinariaTutorial.lblConferirEx1)) {
			confirmaResultadoEx1();
		}
		if(e.getSource().equals(somaBinariaTutorial.lblVolta)) {
			somaBinariaTutorial.setVisible(false);
			new OperacaoBinariaView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(somaBinariaTutorial.lblConferirEx1)) {
			somaBinariaTutorial.lblConferirEx1.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(somaBinariaTutorial.lblVolta)) {
			somaBinariaTutorial.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(somaBinariaTutorial.lblConferirEx1)) {
			somaBinariaTutorial.lblConferirEx1.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(somaBinariaTutorial.lblVolta)) {
			somaBinariaTutorial.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado 1
	 */
	private void confirmaResultadoEx1() {
		final String somaResposta = "0100";

		if(somaResposta.equals(somaBinariaTutorial.txtResposta.getText().trim())) {
			somaBinariaTutorial.lblResultadoEx1.setIcon(imagem.certo);
		} else {
			somaBinariaTutorial.lblResultadoEx1.setIcon(imagem.errado);
		}
	}
}