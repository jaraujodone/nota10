/**
 * 
 */
package br.edu.nota10.controller.binario;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoBinariaView;
import br.edu.nota10.view.binario.SomaBinarioAplicacaoView;

/**
 * @author Jesse A. Done
 */
public class SomaBinarioAplicacaoController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SomaBinarioAplicacaoView somaBinarioView;

	public SomaBinarioAplicacaoController(final SomaBinarioAplicacaoView somaBinarioView) {
		this.somaBinarioView = somaBinarioView;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(somaBinarioView.lblBtnSomar)) {
			somarBinario(somaBinarioView.txtValor1.getText().trim(), somaBinarioView.txtValor2.getText().trim());
		}
		if(e.getSource().equals(somaBinarioView.lblVolta)) {
			somaBinarioView.setVisible(false);
			new OperacaoBinariaView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(somaBinarioView.lblBtnSomar)) {
			somaBinarioView.lblBtnSomar.setIcon(imagem.somarBinarioOn);
		}
		if(e.getSource().equals(somaBinarioView.lblVolta)) {
			somaBinarioView.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(somaBinarioView.lblBtnSomar)) {
			somaBinarioView.lblBtnSomar.setIcon(imagem.somarBinario);
		}
		if(e.getSource().equals(somaBinarioView.lblVolta)) {
			somaBinarioView.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Soma Binario
	 * @param numero1
	 * @param numero2
	 */
	private void somarBinario(final String numero1, final String numero2) {
		int valor1 = 0, valor2 = 0;
		try {
			valor1 = Integer.parseInt(numero1, 2);
			valor2 = Integer.parseInt(numero2, 2);
		} catch (final Exception exc) {
			JOptionPane.showMessageDialog(null, "Valor Digitado n�o � binario");
		}
		
		somaBinarioView.lblResposta.setText(Integer.toBinaryString((valor1 + valor2)));
	}
}