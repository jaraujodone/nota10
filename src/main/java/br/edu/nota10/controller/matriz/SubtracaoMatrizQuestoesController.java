/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.SubtracaoMatrizQuestoesView;

/**
 * @author Jesse A. Done
 */
public class SubtracaoMatrizQuestoesController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SubtracaoMatrizQuestoesView subtracaoQuestoesView;

	public SubtracaoMatrizQuestoesController(final SubtracaoMatrizQuestoesView subtracaoQuestoesView) {
		this.subtracaoQuestoesView = subtracaoQuestoesView;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(subtracaoQuestoesView.lblVolta)) {
			subtracaoQuestoesView.setVisible(false);
			new OperacaoMatrizView();
		}
		if(e.getSource().equals(subtracaoQuestoesView.lblConferirQuestoes)) {
			confirmaResultadoEx1();
			confirmaResultadoEx2();
			confirmaResultadoEx3();
			confirmaResultadoEx4();
			confirmaResultadoEx5();
			confirmaResultadoEx6();
			confirmaResultadoEx7();
			confirmaResultadoEx8();
			confirmaResultadoEx9();
			confirmaResultadoEx10();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(subtracaoQuestoesView.lblVolta)) {
			subtracaoQuestoesView.lblVolta.setIcon(imagem.voltarOn);
		}
		if(e.getSource().equals(subtracaoQuestoesView.lblConferirQuestoes)) {
			subtracaoQuestoesView.lblConferirQuestoes.setIcon(imagem.conferirQuestoesOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(subtracaoQuestoesView.lblVolta)) {
			subtracaoQuestoesView.lblVolta.setIcon(imagem.voltar);
		}
		if(e.getSource().equals(subtracaoQuestoesView.lblConferirQuestoes)) {
			subtracaoQuestoesView.lblConferirQuestoes.setIcon(imagem.conferirQuestoes);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) {	}

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado da matriz 1
	 */
	private void confirmaResultadoEx1() {
		final int[][] matrizResposta1 = new int[][] {{-4, 10, 0}, {3, 5, 1}, {3, 2, 0}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat111.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat112.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat113.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat121.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat122.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat123.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat131.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat132.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat133.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[2][2] == matrizView[2][2]) {

			subtracaoQuestoesView.lblResultadoEx1.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx1.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 2
	 */
	private void confirmaResultadoEx2() {
		final int[][] matrizResposta2 = new int[][] {{4, 3, 9}, {10, -12, 18}, {6, 30, -9}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat211.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat212.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat213.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat221.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat222.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat223.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat231.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat232.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat233.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta2[0][0] == matrizView[0][0] &&
				matrizResposta2[0][1] == matrizView[0][1] &&
				matrizResposta2[0][2] == matrizView[0][2] &&
				matrizResposta2[1][0] == matrizView[1][0] &&
				matrizResposta2[1][1] == matrizView[1][1] &&
				matrizResposta2[1][2] == matrizView[1][2] &&
				matrizResposta2[2][0] == matrizView[2][0] &&
				matrizResposta2[2][1] == matrizView[2][1] &&
				matrizResposta2[2][2] == matrizView[2][2]) {

			subtracaoQuestoesView.lblResultadoEx2.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx2.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 3
	 */
	private void confirmaResultadoEx3() {
		final int[][] matrizResposta3 = new int[][] {{8, 27, 12}, {6, -40, -5}, {7, -5, -10}, {-27, 1, 2}};
		int[][]  matrizView = new int[4][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat311.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat312.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat313.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat321.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat322.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat323.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat331.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat332.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat333.getText().trim());
			matrizView[3][0] = Integer.parseInt(subtracaoQuestoesView.txtMat341.getText().trim());
			matrizView[3][1] = Integer.parseInt(subtracaoQuestoesView.txtMat342.getText().trim());
			matrizView[3][2] = Integer.parseInt(subtracaoQuestoesView.txtMat343.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta3[0][0] == matrizView[0][0] &&
				matrizResposta3[0][1] == matrizView[0][1] &&
				matrizResposta3[0][2] == matrizView[0][2] &&
				matrizResposta3[1][0] == matrizView[1][0] &&
				matrizResposta3[1][1] == matrizView[1][1] &&
				matrizResposta3[1][2] == matrizView[1][2] &&
				matrizResposta3[2][0] == matrizView[2][0] &&
				matrizResposta3[2][1] == matrizView[2][1] &&
				matrizResposta3[2][2] == matrizView[2][2] &&
				matrizResposta3[3][0] == matrizView[3][0] &&
				matrizResposta3[3][1] == matrizView[3][1] &&
				matrizResposta3[3][2] == matrizView[3][2]) {

			subtracaoQuestoesView.lblResultadoEx3.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx3.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 4
	 */
	private void confirmaResultadoEx4() {
		final int[][] matrizResposta4 = new int[][] {{-2, 3, 12, 3}, {20, -3, 10, 6}, {20, -6, 12, 3}, {9, 0, 8, 3}};
		int[][]  matrizView = new int[4][4];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat411.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat412.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat413.getText().trim());
			matrizView[0][3] = Integer.parseInt(subtracaoQuestoesView.txtMat414.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat421.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat422.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat423.getText().trim());
			matrizView[1][3] = Integer.parseInt(subtracaoQuestoesView.txtMat424.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat431.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat432.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat433.getText().trim());
			matrizView[2][3] = Integer.parseInt(subtracaoQuestoesView.txtMat434.getText().trim());
			matrizView[3][0] = Integer.parseInt(subtracaoQuestoesView.txtMat441.getText().trim());
			matrizView[3][1] = Integer.parseInt(subtracaoQuestoesView.txtMat442.getText().trim());
			matrizView[3][2] = Integer.parseInt(subtracaoQuestoesView.txtMat443.getText().trim());
			matrizView[3][3] = Integer.parseInt(subtracaoQuestoesView.txtMat444.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta4[0][0] == matrizView[0][0] &&
				matrizResposta4[0][1] == matrizView[0][1] &&
				matrizResposta4[0][2] == matrizView[0][2] &&
				matrizResposta4[0][3] == matrizView[0][3] &&
				matrizResposta4[1][0] == matrizView[1][0] &&
				matrizResposta4[1][1] == matrizView[1][1] &&
				matrizResposta4[1][2] == matrizView[1][2] &&
				matrizResposta4[1][3] == matrizView[1][3] &&
				matrizResposta4[2][0] == matrizView[2][0] &&
				matrizResposta4[2][1] == matrizView[2][1] &&
				matrizResposta4[2][2] == matrizView[2][2] &&
				matrizResposta4[2][3] == matrizView[2][3] &&
				matrizResposta4[3][0] == matrizView[3][0] &&
				matrizResposta4[3][1] == matrizView[3][1] &&
				matrizResposta4[3][2] == matrizView[3][2] &&
				matrizResposta4[3][3] == matrizView[3][3]) {

			subtracaoQuestoesView.lblResultadoEx4.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx4.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 5
	 */
	private void confirmaResultadoEx5() {
		final int[][] matrizResposta5 = new int[][] {{2, 20, 100, -6}, {1, 8, 20, 45}, {-1, 16, 20, 20}, {-15, -22, -32, -30}, {-9, 19, 12, -40}};
		int[][]  matrizView = new int[5][4];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat511.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat512.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat513.getText().trim());
			matrizView[0][3] = Integer.parseInt(subtracaoQuestoesView.txtMat514.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat521.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat522.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat523.getText().trim());
			matrizView[1][3] = Integer.parseInt(subtracaoQuestoesView.txtMat524.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat531.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat532.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat533.getText().trim());
			matrizView[2][3] = Integer.parseInt(subtracaoQuestoesView.txtMat534.getText().trim());
			matrizView[3][0] = Integer.parseInt(subtracaoQuestoesView.txtMat541.getText().trim());
			matrizView[3][1] = Integer.parseInt(subtracaoQuestoesView.txtMat542.getText().trim());
			matrizView[3][2] = Integer.parseInt(subtracaoQuestoesView.txtMat543.getText().trim());
			matrizView[3][3] = Integer.parseInt(subtracaoQuestoesView.txtMat544.getText().trim());
			matrizView[4][0] = Integer.parseInt(subtracaoQuestoesView.txtMat551.getText().trim());
			matrizView[4][1] = Integer.parseInt(subtracaoQuestoesView.txtMat552.getText().trim());
			matrizView[4][2] = Integer.parseInt(subtracaoQuestoesView.txtMat553.getText().trim());
			matrizView[4][3] = Integer.parseInt(subtracaoQuestoesView.txtMat554.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta5[0][0] == matrizView[0][0] &&
				matrizResposta5[0][1] == matrizView[0][1] &&
				matrizResposta5[0][2] == matrizView[0][2] &&
				matrizResposta5[0][3] == matrizView[0][3] &&
				matrizResposta5[1][0] == matrizView[1][0] &&
				matrizResposta5[1][1] == matrizView[1][1] &&
				matrizResposta5[1][2] == matrizView[1][2] &&
				matrizResposta5[1][3] == matrizView[1][3] &&
				matrizResposta5[2][0] == matrizView[2][0] &&
				matrizResposta5[2][1] == matrizView[2][1] &&
				matrizResposta5[2][2] == matrizView[2][2] &&
				matrizResposta5[2][3] == matrizView[2][3] &&
				matrizResposta5[3][0] == matrizView[3][0] &&
				matrizResposta5[3][1] == matrizView[3][1] &&
				matrizResposta5[3][2] == matrizView[3][2] &&
				matrizResposta5[3][3] == matrizView[3][3] &&
				matrizResposta5[4][0] == matrizView[4][0] &&
				matrizResposta5[4][1] == matrizView[4][1] &&
				matrizResposta5[4][2] == matrizView[4][2] &&
				matrizResposta5[4][3] == matrizView[4][3]) {

			subtracaoQuestoesView.lblResultadoEx5.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx5.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 6
	 */
	private void confirmaResultadoEx6() {
		final int[][] matrizResposta6 = new int[][] {{40, 5, 10, 50}, {12, 5, 12, 11}, {20, 0, 1, 49}};
		int[][]  matrizView = new int[3][4];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat611.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat612.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat613.getText().trim());
			matrizView[0][3] = Integer.parseInt(subtracaoQuestoesView.txtMat614.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat621.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat622.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat623.getText().trim());
			matrizView[1][3] = Integer.parseInt(subtracaoQuestoesView.txtMat624.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat631.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat632.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat633.getText().trim());
			matrizView[2][3] = Integer.parseInt(subtracaoQuestoesView.txtMat634.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta6[0][0] == matrizView[0][0] &&
				matrizResposta6[0][1] == matrizView[0][1] &&
				matrizResposta6[0][2] == matrizView[0][2] &&
				matrizResposta6[0][3] == matrizView[0][3] &&
				matrizResposta6[1][0] == matrizView[1][0] &&
				matrizResposta6[1][1] == matrizView[1][1] &&
				matrizResposta6[1][2] == matrizView[1][2] &&
				matrizResposta6[1][3] == matrizView[1][3] &&
				matrizResposta6[2][0] == matrizView[2][0] &&
				matrizResposta6[2][1] == matrizView[2][1] &&
				matrizResposta6[2][2] == matrizView[2][2] &&
				matrizResposta6[2][3] == matrizView[2][3]) {

			subtracaoQuestoesView.lblResultadoEx6.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx6.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 7
	 */
	private void confirmaResultadoEx7() {
		final int[][] matrizResposta7 = new int[][] {{-36, 2, 4}, {3, 0, 10}, {15, -2, 1}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat711.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat712.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat713.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat721.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat722.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat723.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat731.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat732.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat733.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta7[0][0] == matrizView[0][0] &&
				matrizResposta7[0][1] == matrizView[0][1] &&
				matrizResposta7[0][2] == matrizView[0][2] &&
				matrizResposta7[1][0] == matrizView[1][0] &&
				matrizResposta7[1][1] == matrizView[1][1] &&
				matrizResposta7[1][2] == matrizView[1][2] &&
				matrizResposta7[2][0] == matrizView[2][0] &&
				matrizResposta7[2][1] == matrizView[2][1] &&
				matrizResposta7[2][2] == matrizView[2][2]) {

			subtracaoQuestoesView.lblResultadoEx7.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx7.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 8
	 */
	private void confirmaResultadoEx8() {
		final int[][] matrizResposta8 = new int[][] {{2, 4, 10}, {30, 30, 10}, {-10, -23, 29}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat811.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat812.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat813.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat821.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat822.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat823.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat831.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat832.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat833.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta8[0][0] == matrizView[0][0] &&
				matrizResposta8[0][1] == matrizView[0][1] &&
				matrizResposta8[0][2] == matrizView[0][2] &&
				matrizResposta8[1][0] == matrizView[1][0] &&
				matrizResposta8[1][1] == matrizView[1][1] &&
				matrizResposta8[1][2] == matrizView[1][2] &&
				matrizResposta8[2][0] == matrizView[2][0] &&
				matrizResposta8[2][1] == matrizView[2][1] &&
				matrizResposta8[2][2] == matrizView[2][2]) {

			subtracaoQuestoesView.lblResultadoEx8.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx8.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 9
	 */
	private void confirmaResultadoEx9() {
		final int[][] matrizResposta9 = new int[][] {{28, 12, -23, 46}, {50, -1, 9, -30}, {19, 1, -11, 27}, {-43, 9, 14, 11}, {34, 2, 31, 19}};
		int[][]  matrizView = new int[5][4];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat911.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat912.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat913.getText().trim());
			matrizView[0][3] = Integer.parseInt(subtracaoQuestoesView.txtMat914.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat921.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat922.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat923.getText().trim());
			matrizView[1][3] = Integer.parseInt(subtracaoQuestoesView.txtMat924.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat931.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat932.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat933.getText().trim());
			matrizView[2][3] = Integer.parseInt(subtracaoQuestoesView.txtMat934.getText().trim());
			matrizView[3][0] = Integer.parseInt(subtracaoQuestoesView.txtMat941.getText().trim());
			matrizView[3][1] = Integer.parseInt(subtracaoQuestoesView.txtMat942.getText().trim());
			matrizView[3][2] = Integer.parseInt(subtracaoQuestoesView.txtMat943.getText().trim());
			matrizView[3][3] = Integer.parseInt(subtracaoQuestoesView.txtMat944.getText().trim());
			matrizView[4][0] = Integer.parseInt(subtracaoQuestoesView.txtMat951.getText().trim());
			matrizView[4][1] = Integer.parseInt(subtracaoQuestoesView.txtMat952.getText().trim());
			matrizView[4][2] = Integer.parseInt(subtracaoQuestoesView.txtMat953.getText().trim());
			matrizView[4][3] = Integer.parseInt(subtracaoQuestoesView.txtMat954.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta9[0][0] == matrizView[0][0] &&
				matrizResposta9[0][1] == matrizView[0][1] &&
				matrizResposta9[0][2] == matrizView[0][2] &&
				matrizResposta9[0][3] == matrizView[0][3] &&
				matrizResposta9[1][0] == matrizView[1][0] &&
				matrizResposta9[1][1] == matrizView[1][1] &&
				matrizResposta9[1][2] == matrizView[1][2] &&
				matrizResposta9[1][3] == matrizView[1][3] &&
				matrizResposta9[2][0] == matrizView[2][0] &&
				matrizResposta9[2][1] == matrizView[2][1] &&
				matrizResposta9[2][2] == matrizView[2][2] &&
				matrizResposta9[2][3] == matrizView[2][3] &&
				matrizResposta9[3][0] == matrizView[3][0] &&
				matrizResposta9[3][1] == matrizView[3][1] &&
				matrizResposta9[3][2] == matrizView[3][2] &&
				matrizResposta9[3][3] == matrizView[3][3] &&
				matrizResposta9[4][0] == matrizView[4][0] &&
				matrizResposta9[4][1] == matrizView[4][1] &&
				matrizResposta9[4][2] == matrizView[4][2] &&
				matrizResposta9[4][3] == matrizView[4][3]) {

			subtracaoQuestoesView.lblResultadoEx9.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx9.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 10
	 */
	private void confirmaResultadoEx10() {
		final int[][] matrizResposta10 = new int[][] {{10, 20, 0}, {32, 5, 20}, {20, -36, 9}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoQuestoesView.txtMat1011.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoQuestoesView.txtMat1012.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoQuestoesView.txtMat1013.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoQuestoesView.txtMat1021.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoQuestoesView.txtMat1022.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoQuestoesView.txtMat1023.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoQuestoesView.txtMat1031.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoQuestoesView.txtMat1032.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoQuestoesView.txtMat1033.getText().trim());

		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta10[0][0] == matrizView[0][0] &&
				matrizResposta10[0][1] == matrizView[0][1] &&
				matrizResposta10[0][2] == matrizView[0][2] &&
				matrizResposta10[1][0] == matrizView[1][0] &&
				matrizResposta10[1][1] == matrizView[1][1] &&
				matrizResposta10[1][2] == matrizView[1][2] &&
				matrizResposta10[2][0] == matrizView[2][0] &&
				matrizResposta10[2][1] == matrizView[2][1] &&
				matrizResposta10[2][2] == matrizView[2][2]) {

			subtracaoQuestoesView.lblResultadoEx10.setIcon(imagem.certo);			
		} else {
			subtracaoQuestoesView.lblResultadoEx10.setIcon(imagem.errado);
		}
	}
}