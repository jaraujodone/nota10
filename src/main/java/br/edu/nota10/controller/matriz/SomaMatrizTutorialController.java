/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.SomaMatrizTutorialView;

/**
 * @author Jesse A. Done
 */
public class SomaMatrizTutorialController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SomaMatrizTutorialView somaTutorial;

	public SomaMatrizTutorialController(final SomaMatrizTutorialView somaTutorial) {
		this.somaTutorial = somaTutorial;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(somaTutorial.lblConferirEx1)) {
			confirmaResultadoEx1();
		}
		if(e.getSource().equals(somaTutorial.lblConferirEx2)) {
			confirmaResultadoEx2();
		}
		if(e.getSource().equals(somaTutorial.lblVolta)) {
			somaTutorial.setVisible(false);
			new OperacaoMatrizView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {		
		if(e.getSource().equals(somaTutorial.lblConferirEx1)) {
			somaTutorial.lblConferirEx1.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(somaTutorial.lblConferirEx2)) {
			somaTutorial.lblConferirEx2.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(somaTutorial.lblVolta)) {
			somaTutorial.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(somaTutorial.lblConferirEx1)) {
			somaTutorial.lblConferirEx1.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(somaTutorial.lblConferirEx2)) {
			somaTutorial.lblConferirEx2.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(somaTutorial.lblVolta)) {
			somaTutorial.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado da matriz 1
	 */
	private void confirmaResultadoEx1() {
		final int[][] matrizResposta1 = new int[][] {{1, 7, 7}, {1, 3, 7}};
		int[][] matrizView = new int[2][3];

		try {
			matrizView[0][0] = Integer.parseInt(somaTutorial.txtMat111.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaTutorial.txtMat112.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaTutorial.txtMat113.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaTutorial.txtMat121.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaTutorial.txtMat122.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaTutorial.txtMat123.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2]) {

			somaTutorial.lblResultadoEx1.setIcon(imagem.certo);			
		} else {
			somaTutorial.lblResultadoEx1.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 2
	 */
	private void confirmaResultadoEx2() {
		final int[][] matrizResposta1 = new int[][] {{2, 3, 1}, {4, 2, 4}, {5, -3, -2}, {3, 6, 7}};
		int[][] matrizView = new int[4][3];

		try {
			matrizView[0][0] = Integer.parseInt(somaTutorial.txtMat211.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaTutorial.txtMat212.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaTutorial.txtMat213.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaTutorial.txtMat221.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaTutorial.txtMat222.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaTutorial.txtMat223.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaTutorial.txtMat231.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaTutorial.txtMat232.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaTutorial.txtMat233.getText().trim());
			matrizView[3][0] = Integer.parseInt(somaTutorial.txtMat241.getText().trim());
			matrizView[3][1] = Integer.parseInt(somaTutorial.txtMat242.getText().trim());
			matrizView[3][2] = Integer.parseInt(somaTutorial.txtMat243.getText().trim());

		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[2][2] == matrizView[2][2] &&
				matrizResposta1[3][0] == matrizView[3][0] &&
				matrizResposta1[3][1] == matrizView[3][1] &&
				matrizResposta1[3][2] == matrizView[3][2]) {

			somaTutorial.lblResultadoEx2.setIcon(imagem.certo);			
		} else {
			somaTutorial.lblResultadoEx2.setIcon(imagem.errado);
		}
	}
}