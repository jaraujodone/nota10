/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.SomaMatrizQuestoesView;

/**
 * @author Jesse A. Done
 */
public class SomaMatrizQuestoesController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SomaMatrizQuestoesView somaQuestoes;

	public SomaMatrizQuestoesController(final SomaMatrizQuestoesView somaQuestoes) {
		this.somaQuestoes = somaQuestoes;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {	
		if(e.getSource().equals(somaQuestoes.lblVolta)) {
			somaQuestoes.setVisible(false);
			new OperacaoMatrizView();
		}

		if(e.getSource().equals(somaQuestoes.lblConferirQuestoes)) {
			confirmaResultadoEx1();
			confirmaResultadoEx2();
			confirmaResultadoEx3();
			confirmaResultadoEx4();
			confirmaResultadoEx5();
			confirmaResultadoEx6();
			confirmaResultadoEx7();
			confirmaResultadoEx8();
			confirmaResultadoEx9();
			confirmaResultadoEx10();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {	
		if(e.getSource().equals(somaQuestoes.lblVolta)) {
			somaQuestoes.lblVolta.setIcon(imagem.voltarOn);
		}
		if(e.getSource().equals(somaQuestoes.lblConferirQuestoes)) {
			somaQuestoes.lblConferirQuestoes.setIcon(imagem.conferirQuestoesOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(somaQuestoes.lblVolta)) {
			somaQuestoes.lblVolta.setIcon(imagem.voltar);
		}
		if(e.getSource().equals(somaQuestoes.lblConferirQuestoes)) {
			somaQuestoes.lblConferirQuestoes.setIcon(imagem.conferirQuestoes);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) {	}

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado da matriz 1
	 */
	private void confirmaResultadoEx1() {
		final int[][] matrizResposta1 = new int[][] {{15, 3, -3, 14, 10}, {11, 7, 15, -8, 9}, {9, 6, 9, 8, 0}};
		int[][]  matrizView = new int[3][4];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat111.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat112.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat113.getText().trim());
			matrizView[0][3] = Integer.parseInt(somaQuestoes.txtMat114.getText().trim());
			matrizView[0][4] = Integer.parseInt(somaQuestoes.txtMat115.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat121.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat122.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat123.getText().trim());
			matrizView[1][3] = Integer.parseInt(somaQuestoes.txtMat124.getText().trim());
			matrizView[1][4] = Integer.parseInt(somaQuestoes.txtMat125.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat131.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat132.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat133.getText().trim());
			matrizView[2][3] = Integer.parseInt(somaQuestoes.txtMat134.getText().trim());
			matrizView[2][4] = Integer.parseInt(somaQuestoes.txtMat135.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[0][3] == matrizView[0][3] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2] &&
				matrizResposta1[1][3] == matrizView[1][3] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[2][2] == matrizView[2][2] &&
				matrizResposta1[2][3] == matrizView[2][3]) {

			somaQuestoes.lblResultadoEx1.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx1.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 2
	 */
	private void confirmaResultadoEx2() {
		final int[][] matrizResposta2 = new int[][] {{3, 7, 5, 9}, {9, -6, 6, 0}, {-5, 2, 7, 7}, {8, 16, 28, 7}};
		int[][]  matrizView = new int[4][4];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat211.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat212.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat213.getText().trim());
			matrizView[0][3] = Integer.parseInt(somaQuestoes.txtMat214.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat221.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat222.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat223.getText().trim());
			matrizView[1][3] = Integer.parseInt(somaQuestoes.txtMat224.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat231.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat232.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat233.getText().trim());
			matrizView[3][0] = Integer.parseInt(somaQuestoes.txtMat241.getText().trim());
			matrizView[3][1] = Integer.parseInt(somaQuestoes.txtMat242.getText().trim());
			matrizView[3][2] = Integer.parseInt(somaQuestoes.txtMat243.getText().trim());
			matrizView[3][3] = Integer.parseInt(somaQuestoes.txtMat244.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta2[0][0] == matrizView[0][0] &&
				matrizResposta2[0][1] == matrizView[0][1] &&
				matrizResposta2[0][2] == matrizView[0][2] &&
				matrizResposta2[0][3] == matrizView[0][3] &&
				matrizResposta2[1][0] == matrizView[1][0] &&
				matrizResposta2[1][1] == matrizView[1][1] &&
				matrizResposta2[1][2] == matrizView[1][2] &&
				matrizResposta2[1][3] == matrizView[1][3] &&
				matrizResposta2[2][0] == matrizView[2][0] &&
				matrizResposta2[2][1] == matrizView[2][1] &&
				matrizResposta2[2][2] == matrizView[2][2] &&
				matrizResposta2[2][3] == matrizView[2][3] &&
				matrizResposta2[3][0] == matrizView[3][0] &&
				matrizResposta2[3][1] == matrizView[3][1] &&
				matrizResposta2[3][2] == matrizView[3][2] &&
				matrizResposta2[3][3] == matrizView[3][3]) {

			somaQuestoes.lblResultadoEx2.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx2.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 3
	 */
	private void confirmaResultadoEx3() {
		final int[][] matrizResposta3 = new int[][] {{6, 6, 6}, {12, 0, 5}, {8, 4, 21}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat311.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat312.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat313.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat321.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat322.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat323.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat331.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat332.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat333.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta3[0][0] == matrizView[0][0] &&
				matrizResposta3[0][1] == matrizView[0][1] &&
				matrizResposta3[0][2] == matrizView[0][2] &&
				matrizResposta3[1][0] == matrizView[1][0] &&
				matrizResposta3[1][1] == matrizView[1][1] &&
				matrizResposta3[1][2] == matrizView[1][2] &&
				matrizResposta3[2][0] == matrizView[2][0] &&
				matrizResposta3[2][1] == matrizView[2][1] &&
				matrizResposta3[2][2] == matrizView[2][2]) {

			somaQuestoes.lblResultadoEx3.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx3.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 4
	 */
	private void confirmaResultadoEx4() {
		final int[][] matrizResposta4 = new int[][] {{2, 3, 3, 14, 12}, {9, -13, -12, 5, 8}, {-14, 14, 18, 10, 11},
				{-19, -3, 10, 16, 20}, {5, 0, 16, -9, 15}};
		int[][]  matrizView = new int[5][5];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat411.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat412.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat413.getText().trim());
			matrizView[0][3] = Integer.parseInt(somaQuestoes.txtMat414.getText().trim());
			matrizView[0][4] = Integer.parseInt(somaQuestoes.txtMat415.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat421.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat422.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat423.getText().trim());
			matrizView[1][3] = Integer.parseInt(somaQuestoes.txtMat424.getText().trim());
			matrizView[1][4] = Integer.parseInt(somaQuestoes.txtMat425.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat431.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat432.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat433.getText().trim());
			matrizView[2][3] = Integer.parseInt(somaQuestoes.txtMat434.getText().trim());
			matrizView[2][4] = Integer.parseInt(somaQuestoes.txtMat435.getText().trim());
			matrizView[3][0] = Integer.parseInt(somaQuestoes.txtMat441.getText().trim());
			matrizView[3][1] = Integer.parseInt(somaQuestoes.txtMat442.getText().trim());
			matrizView[3][2] = Integer.parseInt(somaQuestoes.txtMat443.getText().trim());
			matrizView[3][3] = Integer.parseInt(somaQuestoes.txtMat444.getText().trim());
			matrizView[3][4] = Integer.parseInt(somaQuestoes.txtMat445.getText().trim());
			matrizView[4][0] = Integer.parseInt(somaQuestoes.txtMat451.getText().trim());
			matrizView[4][1] = Integer.parseInt(somaQuestoes.txtMat452.getText().trim());
			matrizView[4][2] = Integer.parseInt(somaQuestoes.txtMat453.getText().trim());
			matrizView[4][3] = Integer.parseInt(somaQuestoes.txtMat454.getText().trim());
			matrizView[4][4] = Integer.parseInt(somaQuestoes.txtMat455.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta4[0][0] == matrizView[0][0] &&
				matrizResposta4[0][1] == matrizView[0][1] &&
				matrizResposta4[0][2] == matrizView[0][2] &&
				matrizResposta4[0][3] == matrizView[0][3] &&
				matrizResposta4[0][4] == matrizView[0][4] &&
				matrizResposta4[1][0] == matrizView[1][0] &&
				matrizResposta4[1][1] == matrizView[1][1] &&
				matrizResposta4[1][2] == matrizView[1][2] &&
				matrizResposta4[1][3] == matrizView[1][3] &&
				matrizResposta4[1][4] == matrizView[1][4] &&
				matrizResposta4[2][0] == matrizView[2][0] &&
				matrizResposta4[2][1] == matrizView[2][1] &&
				matrizResposta4[2][2] == matrizView[2][2] &&
				matrizResposta4[2][3] == matrizView[2][3] &&
				matrizResposta4[2][4] == matrizView[2][4] &&
				matrizResposta4[3][0] == matrizView[3][0] &&
				matrizResposta4[3][1] == matrizView[3][1] &&
				matrizResposta4[3][2] == matrizView[3][2] &&
				matrizResposta4[3][3] == matrizView[3][3] &&
				matrizResposta4[3][4] == matrizView[3][4] &&
				matrizResposta4[4][0] == matrizView[4][0] &&
				matrizResposta4[4][1] == matrizView[4][1] &&
				matrizResposta4[4][2] == matrizView[4][2] &&
				matrizResposta4[4][3] == matrizView[4][3] &&
				matrizResposta4[4][4] == matrizView[4][4]) {

			somaQuestoes.lblResultadoEx4.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx4.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 5
	 */
	private void confirmaResultadoEx5() {
		final int[][] matrizResposta5 = new int[][] {{6, 8, 4, 3}, {6, 10, 12, 25}, {22, 10, 24, 11}};
		int[][]  matrizView = new int[3][4];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat511.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat512.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat513.getText().trim());
			matrizView[0][3] = Integer.parseInt(somaQuestoes.txtMat514.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat521.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat522.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat523.getText().trim());
			matrizView[1][3] = Integer.parseInt(somaQuestoes.txtMat524.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat531.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat532.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat533.getText().trim());
			matrizView[2][3] = Integer.parseInt(somaQuestoes.txtMat534.getText().trim());
			matrizView[2][4] = Integer.parseInt(somaQuestoes.txtMat534.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta5[0][0] == matrizView[0][0] &&
				matrizResposta5[0][1] == matrizView[0][1] &&
				matrizResposta5[0][2] == matrizView[0][2] &&
				matrizResposta5[0][3] == matrizView[0][3] &&
				matrizResposta5[1][0] == matrizView[1][0] &&
				matrizResposta5[1][1] == matrizView[1][1] &&
				matrizResposta5[1][2] == matrizView[1][2] &&
				matrizResposta5[1][3] == matrizView[1][3] &&
				matrizResposta5[2][0] == matrizView[2][0] &&
				matrizResposta5[2][1] == matrizView[2][1] &&
				matrizResposta5[2][2] == matrizView[2][2] &&
				matrizResposta5[2][3] == matrizView[2][3]) {

			somaQuestoes.lblResultadoEx5.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx5.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 6
	 */
	private void confirmaResultadoEx6() {
		final int[][] matrizResposta6 = new int[][] {{-5, 6, 10}, {10, 8, 20}, {-11, -10, 12}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat611.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat612.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat613.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat621.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat622.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat623.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat631.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat632.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat633.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta6[0][0] == matrizView[0][0] &&
				matrizResposta6[0][1] == matrizView[0][1] &&
				matrizResposta6[0][2] == matrizView[0][2] &&
				matrizResposta6[1][0] == matrizView[1][0] &&
				matrizResposta6[1][1] == matrizView[1][1] &&
				matrizResposta6[1][2] == matrizView[1][2] &&
				matrizResposta6[2][0] == matrizView[2][0] &&
				matrizResposta6[2][1] == matrizView[2][1] &&
				matrizResposta6[2][2] == matrizView[2][2]) {

			somaQuestoes.lblResultadoEx6.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx6.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 7
	 */
	private void confirmaResultadoEx7() {
		final int[][] matrizResposta7 = new int[][] {{-26, 13, 6, 20}, {10, -6, -1, 29}, {23, 90, 10, -8}, {20, 57, 26, 40}};
		int[][]  matrizView = new int[4][4];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat711.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat712.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat713.getText().trim());
			matrizView[0][3] = Integer.parseInt(somaQuestoes.txtMat714.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat721.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat722.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat723.getText().trim());
			matrizView[1][3] = Integer.parseInt(somaQuestoes.txtMat724.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat731.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat732.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat733.getText().trim());
			matrizView[2][3] = Integer.parseInt(somaQuestoes.txtMat734.getText().trim());
			matrizView[3][0] = Integer.parseInt(somaQuestoes.txtMat741.getText().trim());
			matrizView[3][1] = Integer.parseInt(somaQuestoes.txtMat742.getText().trim());
			matrizView[3][2] = Integer.parseInt(somaQuestoes.txtMat743.getText().trim());
			matrizView[3][3] = Integer.parseInt(somaQuestoes.txtMat744.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta7[0][0] == matrizView[0][0] &&
				matrizResposta7[0][1] == matrizView[0][1] &&
				matrizResposta7[0][2] == matrizView[0][2] &&
				matrizResposta7[0][3] == matrizView[0][3] &&
				matrizResposta7[1][0] == matrizView[1][0] &&
				matrizResposta7[1][1] == matrizView[1][1] &&
				matrizResposta7[1][2] == matrizView[1][2] &&
				matrizResposta7[1][3] == matrizView[1][3] &&
				matrizResposta7[2][0] == matrizView[2][0] &&
				matrizResposta7[2][1] == matrizView[2][1] &&
				matrizResposta7[2][2] == matrizView[2][2] &&
				matrizResposta7[2][3] == matrizView[2][3] &&
				matrizResposta7[3][0] == matrizView[3][0] &&
				matrizResposta7[3][1] == matrizView[3][1] &&
				matrizResposta7[3][2] == matrizView[3][2] &&
				matrizResposta7[3][3] == matrizView[3][3]) {

			somaQuestoes.lblResultadoEx7.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx7.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 8
	 */
	private void confirmaResultadoEx8() {
		final int[][] matrizResposta8 = new int[][] {{3, 2, 6, 17}, {4, 7, -7, 21}, {20, 9, 9, 0}};
		int[][]  matrizView = new int[3][4];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat811.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat812.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat813.getText().trim());
			matrizView[0][3] = Integer.parseInt(somaQuestoes.txtMat814.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat821.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat822.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat823.getText().trim());
			matrizView[1][3] = Integer.parseInt(somaQuestoes.txtMat824.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat831.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat832.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat833.getText().trim());
			matrizView[2][3] = Integer.parseInt(somaQuestoes.txtMat834.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta8[0][0] == matrizView[0][0] &&
				matrizResposta8[0][1] == matrizView[0][1] &&
				matrizResposta8[0][2] == matrizView[0][2] &&
				matrizResposta8[0][3] == matrizView[0][3] &&
				matrizResposta8[1][0] == matrizView[1][0] &&
				matrizResposta8[1][1] == matrizView[1][1] &&
				matrizResposta8[1][2] == matrizView[1][2] &&
				matrizResposta8[1][3] == matrizView[1][3] &&
				matrizResposta8[2][0] == matrizView[2][0] &&
				matrizResposta8[2][1] == matrizView[2][1] &&
				matrizResposta8[2][2] == matrizView[2][2] &&
				matrizResposta8[2][3] == matrizView[2][3]) {

			somaQuestoes.lblResultadoEx8.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx8.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 9
	 */
	private void confirmaResultadoEx9() {
		final int[][] matrizResposta9 = new int[][] {{23, 29, 10, 20}, {20, 1, 4, 13}, {3, 10, -27, 27}};
		int[][]  matrizView = new int[3][4];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat911.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat912.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat913.getText().trim());
			matrizView[0][3] = Integer.parseInt(somaQuestoes.txtMat914.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat921.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat922.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat923.getText().trim());
			matrizView[1][3] = Integer.parseInt(somaQuestoes.txtMat924.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat931.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat932.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat933.getText().trim());
			matrizView[2][3] = Integer.parseInt(somaQuestoes.txtMat934.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta9[0][0] == matrizView[0][0] &&
				matrizResposta9[0][1] == matrizView[0][1] &&
				matrizResposta9[0][2] == matrizView[0][2] &&
				matrizResposta9[0][3] == matrizView[0][3] &&
				matrizResposta9[1][0] == matrizView[1][0] &&
				matrizResposta9[1][1] == matrizView[1][1] &&
				matrizResposta9[1][2] == matrizView[1][2] &&
				matrizResposta9[1][3] == matrizView[1][3] &&
				matrizResposta9[2][0] == matrizView[2][0] &&
				matrizResposta9[2][1] == matrizView[2][1] &&
				matrizResposta9[2][2] == matrizView[2][2] &&
				matrizResposta9[2][3] == matrizView[2][3]) {

			somaQuestoes.lblResultadoEx9.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx9.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 10
	 */
	private void confirmaResultadoEx10() {
		final int[][] matrizResposta10 = new int[][] {{37, 6, 9}, {-3, -8, -14}, {4, 7, 3}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(somaQuestoes.txtMat1011.getText().trim());
			matrizView[0][1] = Integer.parseInt(somaQuestoes.txtMat1012.getText().trim());
			matrizView[0][2] = Integer.parseInt(somaQuestoes.txtMat1013.getText().trim());
			matrizView[1][0] = Integer.parseInt(somaQuestoes.txtMat1021.getText().trim());
			matrizView[1][1] = Integer.parseInt(somaQuestoes.txtMat1022.getText().trim());
			matrizView[1][2] = Integer.parseInt(somaQuestoes.txtMat1023.getText().trim());
			matrizView[2][0] = Integer.parseInt(somaQuestoes.txtMat1031.getText().trim());
			matrizView[2][1] = Integer.parseInt(somaQuestoes.txtMat1032.getText().trim());
			matrizView[2][2] = Integer.parseInt(somaQuestoes.txtMat1033.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta10[0][0] == matrizView[0][0] &&
				matrizResposta10[0][1] == matrizView[0][1] &&
				matrizResposta10[0][2] == matrizView[0][2] &&
				matrizResposta10[1][0] == matrizView[1][0] &&
				matrizResposta10[1][1] == matrizView[1][1] &&
				matrizResposta10[1][2] == matrizView[1][2] &&
				matrizResposta10[2][0] == matrizView[2][0] &&
				matrizResposta10[2][1] == matrizView[2][1] &&
				matrizResposta10[2][2] == matrizView[2][2]) {

			somaQuestoes.lblResultadoEx10.setIcon(imagem.certo);			
		} else {
			somaQuestoes.lblResultadoEx10.setIcon(imagem.errado);
		}
	}
}