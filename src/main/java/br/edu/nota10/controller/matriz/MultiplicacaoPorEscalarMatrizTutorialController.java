/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.MultiplicacaoPorEscalarMatrizTutorialView;

/**
 * @author Jesse A. Done
 */
public class MultiplicacaoPorEscalarMatrizTutorialController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final MultiplicacaoPorEscalarMatrizTutorialView multPorEscalarTutorial;

	public MultiplicacaoPorEscalarMatrizTutorialController(final MultiplicacaoPorEscalarMatrizTutorialView multPorEscalarTutorial) {
		this.multPorEscalarTutorial = multPorEscalarTutorial;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(multPorEscalarTutorial.lblConferirEx1)) {
			confirmaResultadoEx1();
		}
		if(e.getSource().equals(multPorEscalarTutorial.lblConferirEx2)) {
			confirmaResultadoEx2();
		}
		if(e.getSource().equals(multPorEscalarTutorial.lblVolta)) {
			multPorEscalarTutorial.setVisible(false);
			new OperacaoMatrizView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {		
		if(e.getSource().equals(multPorEscalarTutorial.lblConferirEx1)) {
			multPorEscalarTutorial.lblConferirEx1.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(multPorEscalarTutorial.lblConferirEx2)) {
			multPorEscalarTutorial.lblConferirEx2.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(multPorEscalarTutorial.lblVolta)) {
			multPorEscalarTutorial.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) { 
		if(e.getSource().equals(multPorEscalarTutorial.lblConferirEx1)) {
			multPorEscalarTutorial.lblConferirEx1.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(multPorEscalarTutorial.lblConferirEx2)) {
			multPorEscalarTutorial.lblConferirEx2.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(multPorEscalarTutorial.lblVolta)) {
			multPorEscalarTutorial.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado da matriz 1
	 */
	private void confirmaResultadoEx1() {
		final int[][] matrizResposta1 = new int[][] {{-2, 0, -4}, {-8, 10, -6}, {-2, -12, 14}};
		int[][] matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(multPorEscalarTutorial.txtMat111.getText().trim());
			matrizView[0][1] = Integer.parseInt(multPorEscalarTutorial.txtMat112.getText().trim());
			matrizView[0][2] = Integer.parseInt(multPorEscalarTutorial.txtMat113.getText().trim());
			matrizView[1][0] = Integer.parseInt(multPorEscalarTutorial.txtMat121.getText().trim());
			matrizView[1][1] = Integer.parseInt(multPorEscalarTutorial.txtMat122.getText().trim());
			matrizView[1][2] = Integer.parseInt(multPorEscalarTutorial.txtMat123.getText().trim());
			matrizView[2][0] = Integer.parseInt(multPorEscalarTutorial.txtMat131.getText().trim());
			matrizView[2][1] = Integer.parseInt(multPorEscalarTutorial.txtMat132.getText().trim());
			matrizView[2][2] = Integer.parseInt(multPorEscalarTutorial.txtMat133.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[2][2] == matrizView[2][2]) {

			multPorEscalarTutorial.lblResultadoEx1.setIcon(imagem.certo);			
		} else {
			multPorEscalarTutorial.lblResultadoEx1.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 2
	 */
	private void confirmaResultadoEx2() {
		final int[][] matrizResposta1 = new int[][] {{10, -5}, {15, -10}, {0, 20}, {25, -25}};
		int[][] matrizView = new int[4][2];

		try {
			matrizView[0][0] = Integer.parseInt(multPorEscalarTutorial.txtMat211.getText().trim());
			matrizView[0][1] = Integer.parseInt(multPorEscalarTutorial.txtMat212.getText().trim());
			matrizView[1][0] = Integer.parseInt(multPorEscalarTutorial.txtMat221.getText().trim());
			matrizView[1][1] = Integer.parseInt(multPorEscalarTutorial.txtMat222.getText().trim());
			matrizView[2][0] = Integer.parseInt(multPorEscalarTutorial.txtMat231.getText().trim());
			matrizView[2][1] = Integer.parseInt(multPorEscalarTutorial.txtMat232.getText().trim());
			matrizView[3][0] = Integer.parseInt(multPorEscalarTutorial.txtMat241.getText().trim());
			matrizView[3][1] = Integer.parseInt(multPorEscalarTutorial.txtMat242.getText().trim());

		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[3][0] == matrizView[3][0] &&
				matrizResposta1[3][1] == matrizView[3][1]) {

			multPorEscalarTutorial.lblResultadoEx2.setIcon(imagem.certo);			
		} else {
			multPorEscalarTutorial.lblResultadoEx2.setIcon(imagem.errado);
		}
	}
}