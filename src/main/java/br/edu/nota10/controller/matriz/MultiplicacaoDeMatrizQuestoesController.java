/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.MultiplicacaoDeMatrizQuestoesView;

/**
 * @author Jesse A. Done
 */
public class MultiplicacaoDeMatrizQuestoesController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final MultiplicacaoDeMatrizQuestoesView multQuestoesView;

	public MultiplicacaoDeMatrizQuestoesController(final MultiplicacaoDeMatrizQuestoesView multQuestoesView) {
		this.multQuestoesView = multQuestoesView;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(multQuestoesView.lblVolta)) {
			multQuestoesView.setVisible(false);
			new OperacaoMatrizView();
		}
		if(e.getSource().equals(multQuestoesView.lblConferirQuestoes)) {
			confirmaResultadoEx1();
			confirmaResultadoEx2();
			confirmaResultadoEx3();
			confirmaResultadoEx4();
			confirmaResultadoEx5();
			confirmaResultadoEx6();
			confirmaResultadoEx7();
			confirmaResultadoEx8();
			confirmaResultadoEx9();
			confirmaResultadoEx10();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(multQuestoesView.lblVolta)) {
			multQuestoesView.lblVolta.setIcon(imagem.voltarOn);
		}
		if(e.getSource().equals(multQuestoesView.lblConferirQuestoes)) {
			multQuestoesView.lblConferirQuestoes.setIcon(imagem.conferirQuestoesOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(multQuestoesView.lblVolta)) {
			multQuestoesView.lblVolta.setIcon(imagem.voltar);
		}
		if(e.getSource().equals(multQuestoesView.lblConferirQuestoes)) {
			multQuestoesView.lblConferirQuestoes.setIcon(imagem.conferirQuestoes);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado da matriz 1
	 */
	private void confirmaResultadoEx1() {
		final int[][] matrizResposta1 = new int[][] {{20, 48, 71}, {62, 41, 43}, {67, 108, 157}};
		int[][]  matrizView = new int[3][3]; 

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat111.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat112.getText().trim());
			matrizView[0][2] = Integer.parseInt(multQuestoesView.txtMat113.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat121.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat122.getText().trim());
			matrizView[1][2] = Integer.parseInt(multQuestoesView.txtMat123.getText().trim());
			matrizView[2][0] = Integer.parseInt(multQuestoesView.txtMat131.getText().trim());
			matrizView[2][1] = Integer.parseInt(multQuestoesView.txtMat132.getText().trim());
			matrizView[2][2] = Integer.parseInt(multQuestoesView.txtMat133.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[2][2] == matrizView[2][2]) {

			multQuestoesView.lblResultadoEx1.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx1.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 2
	 */
	private void confirmaResultadoEx2() {
		final int[][] matrizResposta2 = new int[][] {{48, 60}, {22, 68}};
		int[][]  matrizView = new int[2][2];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat211.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat212.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat221.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat222.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta2[0][0] == matrizView[0][0] &&
				matrizResposta2[0][1] == matrizView[0][1] &&
				matrizResposta2[1][0] == matrizView[1][0] &&
				matrizResposta2[1][1] == matrizView[1][1]) {

			multQuestoesView.lblResultadoEx2.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx2.setIcon(imagem.errado);
		}

	}

	/**
	 * Confere o resultado da matriz 3
	 */
	private void confirmaResultadoEx3() {
		final int[][] matrizResposta3 = new int[][] {{46, 45, 29}, {62, 61, 29}};
		int[][]  matrizView = new int[2][3];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat311.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat312.getText().trim());
			matrizView[0][2] = Integer.parseInt(multQuestoesView.txtMat313.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat321.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat322.getText().trim());
			matrizView[1][2] = Integer.parseInt(multQuestoesView.txtMat323.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta3[0][0] == matrizView[0][0] &&
				matrizResposta3[0][1] == matrizView[0][1] &&
				matrizResposta3[0][2] == matrizView[0][2] &&
				matrizResposta3[1][0] == matrizView[1][0] &&
				matrizResposta3[1][1] == matrizView[1][1] &&
				matrizResposta3[1][2] == matrizView[1][2]) {

			multQuestoesView.lblResultadoEx3.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx3.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 4
	 */
	private void confirmaResultadoEx4() {
		final int[][] matrizResposta4 = new int[][] {{56, 90}, {92, 139}};
		int[][]  matrizView = new int[2][2];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat411.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat412.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat421.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat422.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta4[0][0] == matrizView[0][0] &&
				matrizResposta4[0][1] == matrizView[0][1] &&
				matrizResposta4[1][0] == matrizView[1][0] &&
				matrizResposta4[1][1] == matrizView[1][1]) {

			multQuestoesView.lblResultadoEx4.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx4.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 5
	 */
	private void confirmaResultadoEx5() {
		final int[][] matrizResposta5 = new int[][] {{190, 215, 275, 213, 227}, {420, 513, 629, 513, 239}, {671, 839, 975, 1021, 414}, 
				{1776, 2097, 2719, 2204, 1724}, {1081, 1283, 1686, 1386, 1009}};
		int[][]  matrizView = new int[5][5];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat511.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat512.getText().trim());
			matrizView[0][2] = Integer.parseInt(multQuestoesView.txtMat513.getText().trim());
			matrizView[0][3] = Integer.parseInt(multQuestoesView.txtMat514.getText().trim());
			matrizView[0][4] = Integer.parseInt(multQuestoesView.txtMat515.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat521.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat522.getText().trim());
			matrizView[1][2] = Integer.parseInt(multQuestoesView.txtMat523.getText().trim());
			matrizView[1][3] = Integer.parseInt(multQuestoesView.txtMat524.getText().trim());
			matrizView[1][4] = Integer.parseInt(multQuestoesView.txtMat525.getText().trim());
			matrizView[2][0] = Integer.parseInt(multQuestoesView.txtMat531.getText().trim());
			matrizView[2][1] = Integer.parseInt(multQuestoesView.txtMat532.getText().trim());
			matrizView[2][2] = Integer.parseInt(multQuestoesView.txtMat533.getText().trim());
			matrizView[2][3] = Integer.parseInt(multQuestoesView.txtMat534.getText().trim());
			matrizView[2][4] = Integer.parseInt(multQuestoesView.txtMat535.getText().trim());
			matrizView[3][0] = Integer.parseInt(multQuestoesView.txtMat541.getText().trim());
			matrizView[3][1] = Integer.parseInt(multQuestoesView.txtMat542.getText().trim());
			matrizView[3][2] = Integer.parseInt(multQuestoesView.txtMat543.getText().trim());
			matrizView[3][3] = Integer.parseInt(multQuestoesView.txtMat544.getText().trim());
			matrizView[3][4] = Integer.parseInt(multQuestoesView.txtMat545.getText().trim());
			matrizView[4][0] = Integer.parseInt(multQuestoesView.txtMat551.getText().trim());
			matrizView[4][1] = Integer.parseInt(multQuestoesView.txtMat552.getText().trim());
			matrizView[4][2] = Integer.parseInt(multQuestoesView.txtMat553.getText().trim());
			matrizView[4][3] = Integer.parseInt(multQuestoesView.txtMat554.getText().trim());
			matrizView[4][4] = Integer.parseInt(multQuestoesView.txtMat555.getText().trim());

		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta5[0][0] == matrizView[0][0] &&
				matrizResposta5[0][1] == matrizView[0][1] &&
				matrizResposta5[0][2] == matrizView[0][2] &&
				matrizResposta5[0][3] == matrizView[0][3] &&
				matrizResposta5[0][4] == matrizView[0][4] &&
				matrizResposta5[1][0] == matrizView[1][0] &&
				matrizResposta5[1][1] == matrizView[1][1] &&
				matrizResposta5[1][2] == matrizView[1][2] &&
				matrizResposta5[1][3] == matrizView[1][3] &&
				matrizResposta5[1][4] == matrizView[1][4] &&
				matrizResposta5[2][0] == matrizView[2][0] &&
				matrizResposta5[2][1] == matrizView[2][1] &&
				matrizResposta5[2][2] == matrizView[2][2] &&
				matrizResposta5[2][3] == matrizView[2][3] &&
				matrizResposta5[2][4] == matrizView[2][4] &&
				matrizResposta5[3][0] == matrizView[3][0] &&
				matrizResposta5[3][1] == matrizView[3][1] &&
				matrizResposta5[3][2] == matrizView[3][2] &&
				matrizResposta5[3][3] == matrizView[3][3] &&
				matrizResposta5[3][4] == matrizView[3][4] &&
				matrizResposta5[4][0] == matrizView[4][0] &&
				matrizResposta5[4][1] == matrizView[4][1] &&
				matrizResposta5[4][2] == matrizView[4][2] &&
				matrizResposta5[4][3] == matrizView[4][3] &&
				matrizResposta5[4][4] == matrizView[4][4]) {

			multQuestoesView.lblResultadoEx5.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx5.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 6
	 */
	private void confirmaResultadoEx6() {
		final int[][] matrizResposta6 = new int[][] {{54, 80, 46}, {75, 111, 61}, {186, 274, 118}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat611.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat612.getText().trim());
			matrizView[0][2] = Integer.parseInt(multQuestoesView.txtMat613.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat621.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat622.getText().trim());
			matrizView[1][2] = Integer.parseInt(multQuestoesView.txtMat623.getText().trim());
			matrizView[2][0] = Integer.parseInt(multQuestoesView.txtMat631.getText().trim());
			matrizView[2][1] = Integer.parseInt(multQuestoesView.txtMat632.getText().trim());
			matrizView[2][2] = Integer.parseInt(multQuestoesView.txtMat633.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}
		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta6[0][0] == matrizView[0][0] &&
				matrizResposta6[0][1] == matrizView[0][1] &&
				matrizResposta6[0][2] == matrizView[0][2] &&
				matrizResposta6[1][0] == matrizView[1][0] &&
				matrizResposta6[1][1] == matrizView[1][1] &&
				matrizResposta6[1][2] == matrizView[1][2] &&
				matrizResposta6[2][0] == matrizView[2][0] &&
				matrizResposta6[2][1] == matrizView[2][1] &&
				matrizResposta6[2][2] == matrizView[2][2]) {

			multQuestoesView.lblResultadoEx6.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx6.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 7
	 */
	private void confirmaResultadoEx7() {
		final int[][] matrizResposta7 = new int[][] {{77, 117}, {183, 283} ,{187, 322}};
		int[][]  matrizView = new int[3][2];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat711.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat712.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat721.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat722.getText().trim());
			matrizView[2][0] = Integer.parseInt(multQuestoesView.txtMat731.getText().trim());
			matrizView[2][1] = Integer.parseInt(multQuestoesView.txtMat732.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}
		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta7[0][0] == matrizView[0][0] &&
				matrizResposta7[0][1] == matrizView[0][1] &&
				matrizResposta7[1][0] == matrizView[1][0] &&
				matrizResposta7[1][1] == matrizView[1][1] &&
				matrizResposta7[2][0] == matrizView[2][0] &&
				matrizResposta7[2][1] == matrizView[2][1]) {

			multQuestoesView.lblResultadoEx7.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx7.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 8
	 */
	private void confirmaResultadoEx8() {
		final int[][] matrizResposta8 = new int[][] {{641, 430, 267}, {427, 45, 226}, {984, 1039, 1201}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat811.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat812.getText().trim());
			matrizView[0][2] = Integer.parseInt(multQuestoesView.txtMat813.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat821.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat822.getText().trim());
			matrizView[1][2] = Integer.parseInt(multQuestoesView.txtMat823.getText().trim());
			matrizView[2][0] = Integer.parseInt(multQuestoesView.txtMat831.getText().trim());
			matrizView[2][1] = Integer.parseInt(multQuestoesView.txtMat832.getText().trim());
			matrizView[2][2] = Integer.parseInt(multQuestoesView.txtMat833.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}
		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta8[0][0] == matrizView[0][0] &&
				matrizResposta8[0][1] == matrizView[0][1] &&
				matrizResposta8[0][2] == matrizView[0][2] &&
				matrizResposta8[1][0] == matrizView[1][0] &&
				matrizResposta8[1][1] == matrizView[1][1] &&
				matrizResposta8[1][2] == matrizView[1][2] &&
				matrizResposta8[2][0] == matrizView[2][0] &&
				matrizResposta8[2][1] == matrizView[2][1] &&
				matrizResposta8[2][2] == matrizView[2][2]) {

			multQuestoesView.lblResultadoEx8.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx8.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 9
	 */
	private void confirmaResultadoEx9() {
		final int[][] matrizResposta9 = new int[][] {{130, 230, 660, 1980}, {182, 319, 873, 2814}};
		int[][]  matrizView = new int[2][4];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat911.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat912.getText().trim());
			matrizView[0][2] = Integer.parseInt(multQuestoesView.txtMat913.getText().trim());
			matrizView[0][3] = Integer.parseInt(multQuestoesView.txtMat914.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat921.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat922.getText().trim());
			matrizView[1][2] = Integer.parseInt(multQuestoesView.txtMat923.getText().trim());
			matrizView[1][3] = Integer.parseInt(multQuestoesView.txtMat924.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta9[0][0] == matrizView[0][0] &&
				matrizResposta9[0][1] == matrizView[0][1] &&
				matrizResposta9[0][2] == matrizView[0][2] &&
				matrizResposta9[0][3] == matrizView[0][3] &&
				matrizResposta9[1][0] == matrizView[1][0] &&
				matrizResposta9[1][1] == matrizView[1][1] &&
				matrizResposta9[1][2] == matrizView[1][2] &&
				matrizResposta9[1][3] == matrizView[1][3]) {

			multQuestoesView.lblResultadoEx9.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx9.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 10
	 */
	private void confirmaResultadoEx10() {
		final int[][] matrizResposta10 = new int[][] {{350, 291, 198}, {167, 178, 280}, {404, 351, 430}};
		int[][]  matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(multQuestoesView.txtMat1011.getText().trim());
			matrizView[0][1] = Integer.parseInt(multQuestoesView.txtMat1012.getText().trim());
			matrizView[0][2] = Integer.parseInt(multQuestoesView.txtMat1013.getText().trim());
			matrizView[1][0] = Integer.parseInt(multQuestoesView.txtMat1021.getText().trim());
			matrizView[1][1] = Integer.parseInt(multQuestoesView.txtMat1022.getText().trim());
			matrizView[1][2] = Integer.parseInt(multQuestoesView.txtMat1023.getText().trim());
			matrizView[2][0] = Integer.parseInt(multQuestoesView.txtMat1031.getText().trim());
			matrizView[2][1] = Integer.parseInt(multQuestoesView.txtMat1032.getText().trim());
			matrizView[2][2] = Integer.parseInt(multQuestoesView.txtMat1033.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta10[0][0] == matrizView[0][0] &&
				matrizResposta10[0][1] == matrizView[0][1] &&
				matrizResposta10[0][2] == matrizView[0][2] &&
				matrizResposta10[1][0] == matrizView[1][0] &&
				matrizResposta10[1][1] == matrizView[1][1] &&
				matrizResposta10[1][2] == matrizView[1][2] &&
				matrizResposta10[2][0] == matrizView[2][0] &&
				matrizResposta10[2][1] == matrizView[2][1] &&
				matrizResposta10[2][2] == matrizView[2][2]) {

			multQuestoesView.lblResultadoEx10.setIcon(imagem.certo);			
		} else {
			multQuestoesView.lblResultadoEx10.setIcon(imagem.errado);
		}
	}
}