/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.SubtracaoMatrizTutorialView;

/**
 * @author Jesse A. Done
 */
public class SubtracaoMatrizTutorialController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final SubtracaoMatrizTutorialView subtracaoTutorial;

	public SubtracaoMatrizTutorialController(final SubtracaoMatrizTutorialView subtracaoTutorial) {
		this.subtracaoTutorial = subtracaoTutorial;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(subtracaoTutorial.lblConferirEx1)) {
			confirmaResultadoEx1();
		}
		if(e.getSource().equals(subtracaoTutorial.lblConferirEx2)) {
			confirmaResultadoEx2();
		}
		if(e.getSource().equals(subtracaoTutorial.lblVolta)) {
			subtracaoTutorial.setVisible(false);
			new OperacaoMatrizView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		e.getComponent().setCursor(new Cursor(Cursor.HAND_CURSOR));

		if(e.getSource().equals(subtracaoTutorial.lblConferirEx1)) {
			subtracaoTutorial.lblConferirEx1.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(subtracaoTutorial.lblConferirEx2)) {
			subtracaoTutorial.lblConferirEx2.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(subtracaoTutorial.lblVolta)) {
			subtracaoTutorial.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(subtracaoTutorial.lblConferirEx1)) {
			subtracaoTutorial.lblConferirEx1.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(subtracaoTutorial.lblConferirEx2)) {
			subtracaoTutorial.lblConferirEx2.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(subtracaoTutorial.lblVolta)) {
			subtracaoTutorial.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado da matriz 1
	 */
	private void confirmaResultadoEx1() {
		final int[][] matrizResposta1 = new int[][] {{3, -4, -3}, {3, -3, 5}};
		int[][] matrizView = new int[2][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoTutorial.txtMat111.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoTutorial.txtMat112.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoTutorial.txtMat113.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoTutorial.txtMat121.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoTutorial.txtMat122.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoTutorial.txtMat123.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2]) {

			subtracaoTutorial.lblResultadoEx1.setIcon(imagem.certo);			
		} else {
			subtracaoTutorial.lblResultadoEx1.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 2
	 */
	private void confirmaResultadoEx2() {
		final int[][] matrizResposta1 = new int[][] {{6, 0, -4}, {6, 2, 4}, {0, 1, -1}, {-6, 5, 3}};
		int[][] matrizView = new int[4][3];

		try {
			matrizView[0][0] = Integer.parseInt(subtracaoTutorial.txtMat211.getText().trim());
			matrizView[0][1] = Integer.parseInt(subtracaoTutorial.txtMat212.getText().trim());
			matrizView[0][2] = Integer.parseInt(subtracaoTutorial.txtMat213.getText().trim());
			matrizView[1][0] = Integer.parseInt(subtracaoTutorial.txtMat221.getText().trim());
			matrizView[1][1] = Integer.parseInt(subtracaoTutorial.txtMat222.getText().trim());
			matrizView[1][2] = Integer.parseInt(subtracaoTutorial.txtMat223.getText().trim());
			matrizView[2][0] = Integer.parseInt(subtracaoTutorial.txtMat231.getText().trim());
			matrizView[2][1] = Integer.parseInt(subtracaoTutorial.txtMat232.getText().trim());
			matrizView[2][2] = Integer.parseInt(subtracaoTutorial.txtMat233.getText().trim());
			matrizView[3][0] = Integer.parseInt(subtracaoTutorial.txtMat241.getText().trim());
			matrizView[3][1] = Integer.parseInt(subtracaoTutorial.txtMat242.getText().trim());
			matrizView[3][2] = Integer.parseInt(subtracaoTutorial.txtMat243.getText().trim());

		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[2][2] == matrizView[2][2] &&
				matrizResposta1[3][0] == matrizView[3][0] &&
				matrizResposta1[3][1] == matrizView[3][1] &&
				matrizResposta1[3][2] == matrizView[3][2]) {

			subtracaoTutorial.lblResultadoEx2.setIcon(imagem.certo);			
		} else {
			subtracaoTutorial.lblResultadoEx2.setIcon(imagem.errado);
		}
	}
}