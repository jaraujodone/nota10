/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.EntendendoEstruturaMatrizTutorialView;

/**
 * @author Jesse A. Done
 */
public class EntendendoEstruturaMatrizTutorialController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final EntendendoEstruturaMatrizTutorialView entendendoMatrizTutorial;

	public EntendendoEstruturaMatrizTutorialController(final EntendendoEstruturaMatrizTutorialView entendendoMatrizTutorial) {
		this.entendendoMatrizTutorial = entendendoMatrizTutorial;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(entendendoMatrizTutorial.lblVolta)) {
			entendendoMatrizTutorial.setVisible(false);
			new OperacaoMatrizView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(entendendoMatrizTutorial.lblVolta)) {
			entendendoMatrizTutorial.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) { 
		if(e.getSource().equals(entendendoMatrizTutorial.lblVolta)) {
			entendendoMatrizTutorial.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }
}