/**
 * 
 */
package br.edu.nota10.controller.matriz;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;

import br.edu.nota10.model.Imagem;
import br.edu.nota10.view.OperacaoMatrizView;
import br.edu.nota10.view.matriz.MultiplicacaoDeMatrizTutorialView;

/**
 * @author Jesse A. Done
 */
public class MultiplicacaoDeMatrizTutorialController extends Observable implements MouseListener {
	final Imagem imagem = new Imagem();
	final MultiplicacaoDeMatrizTutorialView multMatrizTutorial;

	public MultiplicacaoDeMatrizTutorialController(final MultiplicacaoDeMatrizTutorialView multMatrizTutorial) {
		this.multMatrizTutorial = multMatrizTutorial;
	}

	@Override
	public void mouseClicked(final MouseEvent e) {
		if(e.getSource().equals(multMatrizTutorial.lblConferirEx1)) {
			confirmaResultadoEx1();
		}
		if(e.getSource().equals(multMatrizTutorial.lblConferirEx2)) {
			confirmaResultadoEx2();
		}
		if(e.getSource().equals(multMatrizTutorial.lblVolta)) {
			multMatrizTutorial.setVisible(false);
			new OperacaoMatrizView();
		}
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		if(e.getSource().equals(multMatrizTutorial.lblConferirEx1)) {
			multMatrizTutorial.lblConferirEx1.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(multMatrizTutorial.lblConferirEx2)) {
			multMatrizTutorial.lblConferirEx2.setIcon(imagem.conferirOn);
		}
		if(e.getSource().equals(multMatrizTutorial.lblVolta)) {
			multMatrizTutorial.lblVolta.setIcon(imagem.voltarOn);
		}
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		if(e.getSource().equals(multMatrizTutorial.lblConferirEx1)) {
			multMatrizTutorial.lblConferirEx1.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(multMatrizTutorial.lblConferirEx2)) {
			multMatrizTutorial.lblConferirEx2.setIcon(imagem.conferir);
		}
		if(e.getSource().equals(multMatrizTutorial.lblVolta)) {
			multMatrizTutorial.lblVolta.setIcon(imagem.voltar);
		}
	}

	@Override
	public void mousePressed(final MouseEvent e) { }

	@Override
	public void mouseReleased(final MouseEvent e) { }

	/**
	 * Confere o resultado da matriz 1
	 */
	private void confirmaResultadoEx1() {
		final int[][] matrizResposta1 = new int[][] {{12, 14}, {3, 1}, {22, 24}};
		int[][] matrizView = new int[3][2];

		try {
			matrizView[0][0] = Integer.parseInt(multMatrizTutorial.txtMat111.getText().trim());
			matrizView[0][1] = Integer.parseInt(multMatrizTutorial.txtMat112.getText().trim());
			matrizView[1][0] = Integer.parseInt(multMatrizTutorial.txtMat121.getText().trim());
			matrizView[1][1] = Integer.parseInt(multMatrizTutorial.txtMat122.getText().trim());
			matrizView[2][0] = Integer.parseInt(multMatrizTutorial.txtMat131.getText().trim());
			matrizView[2][1] = Integer.parseInt(multMatrizTutorial.txtMat132.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1]) {

			multMatrizTutorial.lblResultadoEx1.setIcon(imagem.certo);			
		} else {
			multMatrizTutorial.lblResultadoEx1.setIcon(imagem.errado);
		}
	}

	/**
	 * Confere o resultado da matriz 2
	 */
	private void confirmaResultadoEx2() {
		final int[][] matrizResposta1 = new int[][] {{47, 52, 57}, {64, 71, 78}, {81, 90, 99}};
		int[][] matrizView = new int[3][3];

		try {
			matrizView[0][0] = Integer.parseInt(multMatrizTutorial.txtMat211.getText().trim());
			matrizView[0][1] = Integer.parseInt(multMatrizTutorial.txtMat212.getText().trim());
			matrizView[0][2] = Integer.parseInt(multMatrizTutorial.txtMat213.getText().trim());
			matrizView[1][0] = Integer.parseInt(multMatrizTutorial.txtMat221.getText().trim());
			matrizView[1][1] = Integer.parseInt(multMatrizTutorial.txtMat222.getText().trim());
			matrizView[1][2] = Integer.parseInt(multMatrizTutorial.txtMat223.getText().trim());
			matrizView[2][0] = Integer.parseInt(multMatrizTutorial.txtMat231.getText().trim());
			matrizView[2][1] = Integer.parseInt(multMatrizTutorial.txtMat232.getText().trim());
			matrizView[2][2] = Integer.parseInt(multMatrizTutorial.txtMat233.getText().trim());
		} catch(final Exception exception){
			JOptionPane.showMessageDialog(null, "Voce � burro!!! Digite Numeros Idiota");
		}

		// Verificando se Matriz do Usuario � igual a resposta
		if(matrizResposta1[0][0] == matrizView[0][0] &&
				matrizResposta1[0][1] == matrizView[0][1] &&
				matrizResposta1[0][2] == matrizView[0][2] &&
				matrizResposta1[1][0] == matrizView[1][0] &&
				matrizResposta1[1][1] == matrizView[1][1] &&
				matrizResposta1[1][2] == matrizView[1][2] &&
				matrizResposta1[2][0] == matrizView[2][0] &&
				matrizResposta1[2][1] == matrizView[2][1] &&
				matrizResposta1[2][2] == matrizView[2][2]) {

			multMatrizTutorial.lblResultadoEx2.setIcon(imagem.certo);			
		} else {
			multMatrizTutorial.lblResultadoEx2.setIcon(imagem.errado);
		}
	}
}